<?php
session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}
?>


<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Simon School of Business Photo Roster">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Photo Roster</title>

	<link rel="stylesheet" href="/style.css">
	<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
	<link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">

    <script src="/static/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="/static/jquery.fullscreen-popup.min.js" type="text/javascript"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script>
		window.onload = function() {
		  document.getElementById("first_name").focus();
	  	};
	</script>
</head>
<body style="overflow: visible">
    <div class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <img src="/static/simon_logo.png">
            </div>
            <div class="col-xs-9">
                <a href="/search_selection.php"><h1>Photo Roster</h1></a>
            </div>
        </div>
    </div>
    </div>


    <div class="main-body">
    <div class="container">
	<h4><a href="/search_selection.php" style="margin-left:25px">Home</a> / <a href="/student_search.php">Student Search</a> / Search by Individual</h4>

        <h1><b><u><center>Search Simon Students</center></u></b></h1>
            <p><h3><center>You can search by persons name, or University ID.</center></h3></p>




			<table width="100%">
				<tr>
					<th>

			            <p><h3><b>Search By Name:</b></h3></p>
						<p>You can search by first and last name or just first or last name.</p><br />
			            <form action="indiv_search.php?usr=student" method="post">
			                <div class="form-group">
								<label for="first_name">First Name:</label>
								<div class="row">
									<div class="col-md-3">
										<input id="first_name" type="text" class="form-control" style="width:300px" name="first_name">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="last_name">Last Name:</label>
								<div class="row">
									<div class="col-md-3">
										<input type="text" class="form-control" style="width:300px" name="last_name">
									</div>
								</div>
							</div>
							<input type="submit" value="Submit" name = "subname" class="btn btn-primary">
				            </form>
					</th>
					<th>
						<h2>- OR -</h2>
 					</th>
					<th >
						<div style="margin-left:100px;">
			            <p><h3><b>Search By University ID:</b></h3></p>
						<p>You can search by University ID.</p><br />
			            <form action ="indiv_search.php?usr=student" method="post">
			                <div class="form-group">
								<label for="UID">University ID Number:</label>
								<br />
								<div class="row">
									<div class="col-md-3">
										<input type="text" class="form-control" style="width:300px" name="UID">
										<br /><br />
									</div>
								</div>
							</div>
							<input type="submit" value="Submit" name="subid" class="btn btn-primary">
			            </form>
					</div>
					</th>
				</tr>
			</table>
		</div>
		</div>

		<footer class="footer container-fluid">
			<?php include("base.html");?>
		</footer>
</body>
</html>
