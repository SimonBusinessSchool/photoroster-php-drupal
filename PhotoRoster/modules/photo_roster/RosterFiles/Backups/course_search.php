<?php

	session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}

    //Connect to the database
    $connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

    $sel_term = $_GET['term'];

if( $link ) {

    //Empty arrays to hold data for later access
     $alpha_course_stack = array();  //Used for comparing the alpha index to the alpha of the individual courses (repeating values)
     $course_stack = array();        //Used for the individual courses
     $section_stack = array();       //Used for holding all the sections
	 $course_stack_compare = array();
     
    
	//Queries for the distinct courses under the specified alpha
    $ind_course_list_query = "select distinct course_number, section_name,
		LEFT(course_number,1) AS alpha --,
        from SIS_Class_Roster
		order by alpha, section_name";
     
    //Queries the course section information
    $course_information_query = "SELECT distinct section_name, course_number 
        from SIS_Class_Roster
        ORDER BY course_number";


    //Queries the course section information
    $section_information_query = "select distinct course_number, term_code,
		LEFT(course_number,6) AS alpha
		from SIS_Class_Roster
        WHERE term_code = '".$sel_term."'
		order by alpha";


    $ind_query_stmt = sqlsrv_query( $link, $ind_course_list_query );
    $name_stmt = sqlsrv_query( $link, $course_information_query );
    $section_stmt = sqlsrv_query( $link, $section_information_query);

   
	while( $row = sqlsrv_fetch_array( $ind_query_stmt, SQLSRV_FETCH_ASSOC ) ) {
		
      array_push($alpha_course_stack, $row['alpha']);
      array_push($course_stack, $row['course_number']);
    }
    
	//Pull the course section numbers
    while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
      array_push($section_stack, $row['section_name']);
    }

	//Pull the course section numbers
    while( $row = sqlsrv_fetch_array( $section_stmt, SQLSRV_FETCH_ASSOC ) ) {
	  array_push($course_stack_compare, $row['course_number']);
    }

    if( $name_stmt === false ) {
      die( print_r( sqlsrv_errors(), true));
    }

} else{
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true ) );
}
    
?>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title></title>
        <link rel="stylesheet" href="layout.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">
        <script src="/static/jquery-2.1.4.min.js" type="text/javascript"></script>
        <!-- #region js -->
        <script>
            function autocomplete(inp, arr) {
                /*the autocomplete function takes two arguments,
                the text field element and an array of possible autocompleted values:*/
                var currentFocus;
                /*execute a function when someone writes in the text field:*/
                inp.addEventListener("input", function(e) {
                    var a, b, i, val = this.value;
                    /*close any already open lists of autocompleted values*/
                    closeAllLists();
                    if (!val) { return false;}
                    currentFocus = -1;
                    /*create a DIV element that will contain the items (values):*/
                    a = document.createElement("DIV");
                    a.setAttribute("id", this.id + "autocomplete-list");
                    a.setAttribute("class", "autocomplete-items");
                    /*append the DIV element as a child of the autocomplete container:*/
                    this.parentNode.appendChild(a);
                    /*for each item in the array...*/
                    for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                            b.addEventListener("click", function(e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                    }
                });
                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function(e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                    } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                    } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                    }
                });
                function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
                }
                function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
                }
                function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
            } 
           

            function getUrlParameter(name) {
                name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
                var results = regex.exec(location.search);
                return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
            };

            var input_array = 
             var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];


            window.onload = function() {
              document.getElementById("course_list").focus();
            };
        </script>
        <!-- #endregion -->

    
    <body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/static/simon_logo.png">
                </div>
                <div class="col-xs-9">
                    <a href="/roster_welcome.php"><h1>Photo Roster</h1></a>
                </div>
            </div>
            <h4><a href="/roster_welcome.php" style="margin-left:25px">Home</a> / <a href="/conn_course.php">Term Selection</a></h4>
        </div>
    </div>
     <div class="main-body">
        <div class="container">
            <br /><br />
            <h2><p>Either start by typing a course name</p><p>or click in the bar for a dropdown:</p></h2>
            <!--Make sure the form has the autocomplete function switched off:-->
            <table style="width: 100%">
                <tr>
                    <form method="get" action="/section_search.php">
                        <div class="autocomplete">
                            <th>
                                <input type="hidden" id="hiddenInput" type="text" name="term"  list="course_list" value="<?php echo $sel_term ?>" />
                                <input id="myInput" type="text" name="course" list="course_list" placeholder="Course Number" autofocus/>
                                <datalist id="course_list" name="myCountry">
                                <?php 
                                for($i = 0; $i < sizeof($course_stack_compare); $i++){
                                    echo "<option value=".$course_stack_compare[$i].">".$course_stack_compare[$i]."</option>"; 
                                }
                                
                                
                                ?>
                                </datalist>
                            </th>
                            <th>
                                <input type="submit" align="right">
                            </th>
                        </div>
                    </form>
                </tr>
            </table>






        </div>
    </div>
   
    
    
	<script>
            var acc = document.getElementsByClassName("accordion");
			var i;
			for (i = 0; i < acc.length; i++) {
			  acc[i].addEventListener("click", function(e) {
				/* Toggle between adding and removing the "active" class,
				to highlight the button that controls the panel */
				this.classList.toggle("active");
				console.log(e.target);
				/* Toggle between hiding and showing the active panel */
				var panel = this.nextElementSibling;
				if (panel.style.display === "block") {
				  panel.style.display = "none";
				} else {
				  panel.style.display = "block";
				}
			  });
			}
        </script>
         <script>
            autocomplete(document.getElementById("myInput"), countries);
        </script> 
		<footer class="footer-absolute container-fluid">
			<?php include("base.html");?>
		</footer>
    </body>
</html>