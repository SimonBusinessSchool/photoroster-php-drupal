
<?php
    
    $connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );
   
    if($link){


        $hints = array();
        $sections = array();
        $course_stack = array();

        $sel_term = $_GET['term'];
        $hint_query = 'select distinct course_number, term_code,
	        LEFT(course_number, 6) as omega
            from SIS_Class_Roster
            where term_code ='.$sel_term.'
	        order by omega';

        $course_information_query = "SELECT distinct section_name, course_number 
            from SIS_Class_Roster
            ORDER BY course_number";

        $course_stack_query ='select distinct course_number, section_name,
	        LEFT(course_number,6) AS alpha --,
            from SIS_Class_Roster
	        order by alpha, section_name';

        $stmt = sqlsrv_query( $link, $hint_query );
        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
            array_push($hints, $row['course_number']);
        }

        $section_stmt = sqlsrv_query( $link, $course_information_query );
        while( $row = sqlsrv_fetch_array( $section_stmt, SQLSRV_FETCH_ASSOC ) ) {
            array_push($sections, $row['section_name']);
        }

        $ind_query_stmt = sqlsrv_query( $link, $course_stack_query );
        while( $row = sqlsrv_fetch_array( $ind_query_stmt, SQLSRV_FETCH_ASSOC ) ) {
            array_push($course_stack, $row['course_number']);
        }

        // get the q parameter from URL
        $q = $_REQUEST["q"];
        $hint = "";

        // lookup all hints from array if $q is different from ""
        if ($q !== "") {
            $q = strtolower($q);
            $len=strlen($q);
            foreach($hints as $name) {
                if (stristr($q, substr($name, 0, $len))) {
                    if ($hint === "") {
                        $hint = $name;
                    } else {
                        $hint .= ", ".$name;
                    }
                    //echo $name.', ';
                }
               
            }
        }
        session_start();
        //echo $hint;
        $string_array = explode(',', $hint);


        $_SESSION['str_len'] = sizeof($string_array);
        $_SESSION['str_arr'] = $string_array;
        
        for($i = 0; $i < sizeof($string_array); $i++){
            //echo "<a href=\"/course_search.php?term=".$sel_term.">".$string_array[$i]."<br />";
            echo "<a href=\"/course_search.php?term=".$sel_term."\">".$string_array[$i]."<br />";
            
            /*

            echo "<button class=\"accordion\">".$string_array[$i]."</button><br />
                <div class=\"panel\" style=\"display: none;\"><div>";
                    for($j = 0; $j < sizeof($sections); $j++){
                        echo "<div class=\"panel\" style=\"display: none;\">";
                        echo "<p>".$j."</p></div>";
                        //echo "<p><a href=\"/roster.php?term=".$sel_term."&course=".$string_array[$i]."&section=".$sections[$j].">".$string_array[$i]."</a><br /></p>";
					}
            echo "</div></div>";
            */
            //echo "<a href=\"/roster.php?term=".$sel_term."&course=".$string_array[$i]."&section=*\">".$string_array[$i]."</a><br />";
            //----------------------------------------------------------------------------

            //echo $string_array[$i];


            //-----------------------------------------------------------------------------
            //echo $string_array[$i]."<br />";
        }
        //header("Refresh:0");
        //header('Location: course_search.php?term='.$sel_term);

        // Output "no suggestion" if no hint was found or output correct values
        //echo $hint === "" ? "no suggestion" : $hint;
    }//end if($link)
?> 

<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title></title>
        <link rel="stylesheet" href="layout.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">
    <body>
    
    </body>
</html>