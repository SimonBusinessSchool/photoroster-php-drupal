

<?php

	session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}



    $response = "";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $response .= "I have made it to this point";
    }

    //Connect to the database
    $connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

	if( $link ) {

		//Empty arrays to hold data for later access
		$term_stack = array();  					//Used for comparing the alpha index to the alpha of the individual courses (repeating values)
		$course_stack = array();        	//Used for the individual courses
		$section_stack = array();       	//Used for holding all the sections
		$course_stack_compare = array();	//Only holds one copy of each course, used for section comparisons later
		//print_r(($_POST['val']));
		//Initial run

		$sel_term = trim($_POST['term']);
		$sel_course = trim($_POST['course']);
		//print_r($sel_term);
		//print_r($sel_course);
		$universal_query2 = "SELECT distinct section_name, course_number, term_code
			from SIS_Class_Roster
			where term_code = '".trim($sel_term)."' and course_number = '".trim($sel_course)."'
			ORDER BY course_number";

	    //Returns each individual section for each course (one to many repeat courses)
	    $course_information_query = "SELECT distinct section_name, course_number
	        from SIS_Class_Roster
	        ORDER BY course_number";


	    //Gets each individual course available. This will feed solely into $course_stack_compare
			//so that we can check against each course once to populate every section availabe to that course

	    $section_information_query = "SELECT distinct course_number, term_code,
			LEFT(course_number,6) AS alpha
			from SIS_Class_Roster
			WHERE term_code = '".$sel_term."'
			order by alpha";



		//$ind_query_stmt = sqlsrv_query( $link, $ind_course_list_query );
		//$name_stmt = sqlsrv_query( $link, $course_information_query );
		$section_stmt = sqlsrv_query( $link, $section_information_query);

		$universal_query_stmt2 = sqlsrv_query( $link, $universal_query2 );
		while( $row  = sqlsrv_fetch_array( $universal_query_stmt2, SQLSRV_FETCH_ASSOC) ) {
			array_push($section_stack, $row['section_name']);
		}


		//Pull the course section numbers
	    while( $row = sqlsrv_fetch_array( $section_stmt, SQLSRV_FETCH_ASSOC ) ) {
		  array_push($course_stack_compare, $row['course_number']);
	    }
		//print_r($course_stack);
	    //if( $universal_query_stmt === false ) {
	    //  die( print_r( sqlsrv_errors(), true));
	    //}


		//Course
		if($_POST['val'] == 1){//isset($_POST['term']) && isset($_POST['course'])){
			//$sel_term = $_POST['term'];
			// Here, you can also perform some database query operations with above values.
			//$_POST['courses'] = $course_stack_compare;
			//print_r($section_stack);
			$_POST['course'] = $sel_course;
			echo json_encode($section_stack);
		}

	} else{
	     echo "Connection could not be established.<br />";
	     die( print_r( sqlsrv_errors(), true ) );
	}

?>
