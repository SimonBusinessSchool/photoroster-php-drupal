<?php



	session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}

    
//Connect to the database
$connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

if( $link ) {

    //Empty arrays to hold data for later access
     $term_stack = array();
     $name_stack = array();

     //Returns a list of current terms
    $term_list_query = "select distinct term, term_code from SIS_Class_Roster";

    $term_stmt = sqlsrv_query( $link, $term_list_query );
    while( $row = sqlsrv_fetch_array( $term_stmt, SQLSRV_FETCH_ASSOC ) ) {
        array_push($name_stack, $row['term']);
        array_push($term_stack, $row['term_code']);
    }

    if( $term_stmt === false ) {
        die( print_r( sqlsrv_errors(), true));
    }

//If connection fails
} else{
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true ) );
}


?>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title></title>
    </head>
    <body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/static/simon_logo.png">
                </div>
                <div class="col-xs-9">
                    <a href="/roster_welcome.php"><h1>Photo Roster</h1></a>
                </div>
            </div>
            <h4><a href="/roster_welcome.php" style="margin-left:25px">Home</a></h4>
        </div>
    </div>
    <div class="main-body">
        <div class="container">
        
        <br /><br />
            <ul>
            <!-- Iterate through term_stack and display current terms by name -->
            <?php for($i = 0; $i < sizeof($name_stack); $i++): ?> 
                <li>
                    <h3><a href="course_search.php?term=<?php echo $term_stack[$i] ?>"><?php echo str_replace("Quarter", "Term", $name_stack[$i]) ?></a></h3>
                </li>
                <br>
                <?php   ?>
             <?php endfor; ?>
            </ul>
        </div>
    </div>
   <footer class="footer-absolute container-fluid">
    <?php include("base.html");?>
    </footer>
    </body>
</html>