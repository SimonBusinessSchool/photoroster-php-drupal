<?php


	session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}


    $expressions = array();
    $num_cols = 6;

    //Connect to the database
    //$connectionInfo = array( "UID" => "crystal_rpt", "PWD" => "d14m0ndd4wg", "Database" => "Photo_Roster" );      TESTING
    //$link = sqlsrv_connect( "its-simnsis-wt1.ur.rochester.edu", $connectionInfo );            TESTING
    $connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

    //Pulling in from course search
    if(isset($_GET['course'])){
        array_push($expressions, $_GET['term'], $_GET['course'], $_GET['section']);

        $bread = "<a href=/course_search.php>Search by Course</a> / ";
		$bread2 = null;
        $echo_val = null;
		$return_var = "/course_search.php";

        if( $link ) {

            //Empty arrays to hold data for later access
            $first_name_stack = array();       //Student first name
            $preferred_name_stack = array();   //Student preferred name
            $last_name_stack = array();        //Student last name
            $country_stack = array();         //Country
            $program_stack = array();         //Program of Study
            $photo_image = array();            //Photo image location
            $UID = array();                   //University ID

            if($expressions[2] != '*'){
                //Queries the unique alpha index identifier
            $roster_query = "select p.*, r.course_number, r.term, r.section_name
                from SIS_Class_Roster r
                inner join photo_roster_student_view p ON p.university_id = r.university_id
                where term_code = '".$expressions[0]."' and course_number = '".$expressions[1]."'
                and section_name = '".$expressions[2]."'
                order by p.last_name";
            }else{
            $roster_query = "select p.*, r.course_number, r.term, r.section_name
                from SIS_Class_Roster r
                inner join photo_roster_student_view p ON p.university_id = r.university_id
                where term_code = '".$expressions[0]."' and course_number = '".$expressions[1]."'
                order by p.last_name";
            }

            $stmt = sqlsrv_query( $link, $roster_query );


            //Pull information for the roster based on input data
            while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
                array_push($preferred_name_stack, $row['preferred_name']);
                array_push($first_name_stack, $row['first_name']);
                array_push($last_name_stack, $row['last_name']);
                array_push($country_stack, $row['country']);
                array_push($program_stack, $row['program']);
                array_push($photo_image, $row['photo_image']);
				array_push($UID, $row['university_id']);
            }

            if( $stmt === false ) {
                die( print_r( sqlsrv_errors(), true));
            }

			$_SESSION["preferred_name"] = $preferred_name_stack;
			$_SESSION["first_name"] = $first_name_stack;
			$_SESSION["last_name"] = $last_name_stack;
			$_SESSION["country"] = $country_stack;
			$_SESSION["program"] = $program_stack;
			$_SESSION["photo_image"] = $photo_image;
			$_SESSION["uid"] = $UID;
        }
        else{
            echo "Connection could not be established.<br />";
            die( print_r( sqlsrv_errors(), true ) );
        }

    }

    //Pulling in from program search
    if(isset($_GET['program'])){
        array_push($expressions, $_GET['program'], $_GET['year']);

        $tmp_prog = str_replace(" ", "%20", $_GET['program']);
        $bread = "<a href=/student_search.php>Student Search</a> / ";
		$bread2 = " / <a href=prog_search.php?program=".$tmp_prog.">Year</a> / ";
        $return_var = "/conn_prog.php";
        $echo_val = " Search by Program ";
        if( $link ) {

            //Empty arrays to hold data for later access
            $preferred_name_stack = array();   //Student preferred name
            $first_name_stack = array();       //Student first name
            $last_name_stack = array();        //Student last name
            $country_stack = array();         //Current Employer
            $program_stack = array();         //Program of Study
            $photo_image = array();            //Photo image location
            $UID = array();                   //University ID




            //Queries the unique alpha index identifier
            $roster_query = "SELECT p.preferred_name, p.first_name, p.last_name, p.country, p.photo_image, p.program, p.university_id from SIS_student_roster r
                inner join photo_roster_student_view p ON p.university_id = r.university_id
                WHERE p.program = '".$expressions[0]."' and p.grad_year = '".$expressions[1]."'
                ORDER BY p.last_name";

            $stmt = sqlsrv_query( $link, $roster_query );

            //Pull information for the roster based on input data
            while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
                array_push($preferred_name_stack, $row['preferred_name']);
                array_push($first_name_stack, $row['first_name']);
                array_push($last_name_stack, $row['last_name']);
                array_push($country_stack, $row['country']);
                array_push($program_stack, $row['program']);
                array_push($photo_image, $row['photo_image']);
                array_push($UID, $row['university_id']);
            }
			//Removes any single quotes (') from names so it won't crash the query
			for($i = 0; $i < sizeof($last_name_stack); $i++){
				if(strpos($last_name_stack[$i], "'")) {
					$last_name_stack[$i] = str_replace("'", "", $last_name_stack[$i]);
				}
			}

            if( $stmt === false ) {
                die( print_r( sqlsrv_errors(), true));
            }

			$_SESSION["preferred_name"] = $preferred_name_stack;
			$_SESSION["first_name"] = $first_name_stack;
			$_SESSION["last_name"] = $last_name_stack;
			$_SESSION["country"] = $country_stack;
			$_SESSION["program"] = $program_stack;
			$_SESSION["photo_image"] = $photo_image;
			$_SESSION["uid"] = $UID;

        }
        else{
            echo "Connection could not be established.<br />";
            die( print_r( sqlsrv_errors(), true ) );
        }
    }

	//pulling in data from staff
	if(isset($_GET['value']) && $_GET['value'] == "staff"){
		$bread = "<a href=/student_search.php>Student Search</a> / ";
		$bread2 = "<a href=prog_search.php?program=>Year</a> / ";
        $return_var = "/conn_prog.php";
        $echo_val = " Search by Program ";

		$first_name_stack = array();       //Student first name
		$last_name_stack = array();        //Student last name
		$photo_image = array();            //Photo image location
		$UID = array();                   //University ID

		//Queries the unique alpha index identifier
		$staff_view_query = "SELECT * FROM photo_roster_staff_view
			order by last_name";

		$stmt = sqlsrv_query( $link, $staff_view_query );

		//Pull information for the roster based on input data
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
			array_push($first_name_stack, $row['first_name']);
			array_push($last_name_stack, $row['last_name']);
			array_push($photo_image, $row['photo_image']);
			array_push($UID, $row['university_id']);
		}
		//Removes any single quotes (') from names so it won't crash the query
		for($i = 0; $i < sizeof($last_name_stack); $i++){
			if(strpos($last_name_stack[$i], "'")) {
				$last_name_stack[$i] = str_replace("'", "", $last_name_stack[$i]);
			}
		}

		$_SESSION["preferred_name"] = null;
		$_SESSION["first_name"] = $first_name_stack;
		$_SESSION["last_name"] = $last_name_stack;
		$_SESSION["country"] = null;
		$_SESSION["program"] = null;
		$_SESSION["photo_image"] = $photo_image;
		$_SESSION["uid"] = $UID;

	}
	//pulling in data from faculty
	if(isset($_GET['value']) && $_GET['value'] == "faculty"){
		$bread = "<a href=/student_search.php>Student Search</a> / ";
		$bread2 = "<a href=prog_search.php?program=>Year</a> / ";
        $return_var = "/conn_prog.php";
        $echo_val = " Search by Program ";

		$first_name_stack = array();       //Student first name
		$last_name_stack = array();        //Student last name
		$photo_image = array();            //Photo image location
		$UID = array();                   //University ID

		//Queries the unique alpha index identifier
		$staff_view_query = "SELECT * FROM photo_roster_faculty_view";

		$stmt = sqlsrv_query( $link, $staff_view_query );

		//Pull information for the roster based on input data
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
			array_push($first_name_stack, $row['first_name']);
			array_push($last_name_stack, $row['last_name']);
			array_push($photo_image, $row['photo_image']);
			array_push($UID, $row['university_id']);
		}
		//Removes any single quotes (') from names so it won't crash the query
		for($i = 0; $i < sizeof($last_name_stack); $i++){
			if(strpos($last_name_stack[$i], "'")) {
				$last_name_stack[$i] = str_replace("'", "", $last_name_stack[$i]);
			}
		}

		$_SESSION["preferred_name"] = null;
		$_SESSION["first_name"] = $first_name_stack;
		$_SESSION["last_name"] = $last_name_stack;
		$_SESSION["country"] = null;
		$_SESSION["program"] = null;
		$_SESSION["photo_image"] = $photo_image;
		$_SESSION["uid"] = $UID;

	}
	//pulling in data from all
	if(isset($_GET['value']) && $_GET['value'] == "all"){
		$bread = "";// "<a href=/student_search.php>Student Search</a> / ";
		$bread2 = "";// "<a href=prog_search.php?program=>Year</a> / ";
        $return_var = "";//"/conn_prog.php";
        $echo_val = "";//" Search by Program ";

		$first_name_stack = array();       //Student first name
		$last_name_stack = array();        //Student last name
		$program_stack = array();			//Program for students
		$job_title_stack = array();			//Job title for employees (staff/faculty)
		$photo_image = array();            //Photo image location
		$UID = array();                   //University ID

		//Queries the unique alpha index identifier
		$staff_view_query = "SELECT * FROM photo_roster_all_view
			order by sort_order, last_name";

		$stmt = sqlsrv_query( $link, $staff_view_query );

		//Pull information for the roster based on input data
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
			array_push($first_name_stack, $row['first_name']);
			array_push($last_name_stack, $row['last_name']);
			array_push($program_stack, $row['program']);
			array_push($job_title_stack, $row['job_title']);
			array_push($photo_image, $row['photo_image']);
			array_push($UID, $row['university_id']);
		}
		//Removes any single quotes (') from names so it won't crash the query
		for($i = 0; $i < sizeof($last_name_stack); $i++){
			if(strpos($last_name_stack[$i], "'")) {
				$last_name_stack[$i] = str_replace("'", "", $last_name_stack[$i]);
			}
		}

		$_SESSION["preferred_name"] = null;
		$_SESSION["first_name"] = $first_name_stack;
		$_SESSION["last_name"] = $last_name_stack;
		$_SESSION["country"] = null;
		$_SESSION["program"] = null;
		$_SESSION["photo_image"] = $photo_image;
		$_SESSION["uid"] = $UID;

	}


    function hex2str($hex) {
        $str = '';
        for($i=0;$i<strlen($hex);$i+=2)
            $str .= chr(hexdec(substr($hex,$i,2)));

        return $str;
    }


    $last_page = basename($_SERVER['HTTP_REFERER']);


class Roster{
    function __construct(){
        echo 'I am made';
    }

    function input_array(){//$inp_arr){
        echo 'I have been called';
    }
}

?>




<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title></title>
		<link rel="stylesheet" href="/style.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">
        <script src="/static/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="/static/jquery.fullscreen-popup.min.js" type="text/javascript"></script>
        <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script>
			$().ready(function() {
				$('.selection').click(function() {
					$("#program").val($(this).attr('data-program'));
					$("#year").val($(this).attr('data-year'));
					$("#jank_form").submit();
				});
			});
		</script>


    </head>



<body>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <img src="/static/simon_logo.png">
            </div>
            <div class="col-xs-9">
                <a href="/search_selection.php"><h1>Photo Roster</h1></a>
            </div>
        </div>
    </div>
</div>
<div class="main-body">
<div class="container">
<h4><a href="/search_selection.php" style="margin-left:25px">Home</a><?php echo " / ".$bread ?><a href=" <?php echo $return_var ?>"><?php echo $echo_val ?></a>  <?php echo $bread2 ?> Results</h4>
<br /><br />
    <div>
        <button type="button" class="btn btn-primary" onclick="window.location.replace('/<?php echo $last_page ?>');">Back to search</button>
        <a href="/pdf.php"><button class="btn btn-primary">Download as PDF</button></a>
        <br /><br />
    </div>
    <!-- Table for display of thmbnails -->

    <?php
	if(sizeof($last_name_stack) > 0){


		$i = 0;
			for ($row=0; $row < sizeof($last_name_stack)/$num_cols; $row++){
			?>

			<?php
				for ($col=0; $col<$num_cols; $col++){
					while($i < sizeof($last_name_stack)){
					  ?><center>
						<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
						<div class="profile">
							<?php
							if(!isset($_GET['value'])){

								if($photo_image[$i] != NULL){
									//echo sizeof($UID);
									?><a href="" data-student="<?php echo $UID[$i];?>" data-popup="#popup"><img border="3" alt="" src="data:image/jpg;base64,<?= base64_encode( $photo_image[$i]) ?>" height="200"></a><?php
									//echo 'photo hex input '.dechex(base64_decode($photo_image[$i]));

								}else{
									?><a href="" data-student="<?php echo $UID[$i];?>" data-popup="#popup"><img border="3" alt="" src="static/rochester.png" height="200"></a><?php
								}?>

								<p><?php
									if(!$preferred_name_stack[$i] == NULL){
										echo '<strong>'.$preferred_name_stack[$i].' '.$last_name_stack[$i].'</strong>';
										echo "<br />";
									}else{
										echo '<strong>'.$first_name_stack[$i].' '.$last_name_stack[$i].'</strong>';
										echo "<br />";
									}

									if(!$country_stack[$i] == NULL){
										echo $country_stack[$i];
										echo "<br />";
									}else{
										echo "<br />";
									}

									echo $program_stack[$i];
								}
								if(isset($_GET['value']) && $_GET['value'] != "all"){
									if($photo_image[$i] != NULL){
										?><a href="" data-student="<?php echo $UID[$i];?>" data-popup="#popup"><img border="3" alt="" src="data:image/jpg;base64,<?= base64_encode( $photo_image[$i]) ?>" height="200"></a><?php
									}else{
										?><a href="" data-student="<?php $UID[$i];?>" data-popup="#popup"><img border="3" alt="" src="static/rochester.png" height="200"></a><?php
									}?>

									<p><?php

									echo '<strong>'.$first_name_stack[$i].' '.$last_name_stack[$i].'</strong>';
									echo "<br />";
								}
								if(isset($_GET['value']) && $_GET['value'] == "all"){
									if($photo_image[$i] != NULL){
										?><a href="" data-student="<?php echo $UID[$i];?>" data-popup="#popup"><img border="3" alt="" src="data:image/jpg;base64,<?= base64_encode( $photo_image[$i]) ?>" height="200"></a><?php
									}else{
										?><a href="" data-student="<?php $UID[$i];?>" data-popup="#popup"><img border="3" alt="" src="static/rochester.png" height="200"></a><?php
									}?>

									<p><?php

									echo '<strong>'.$first_name_stack[$i].' '.$last_name_stack[$i].'</strong>';
									echo "<br />";
									//Print out student program information
									if($program_stack[$i] != null){
										echo '<strong>'.$program_stack[$i].'</strong>';
										echo '<br />';
									//print out faculty and staff job title information
									}else if($job_title_stack != null){
										echo '<strong>'.$job_title_stack[$i].'</strong>';
										echo '<br />';
									}else{
										//I love PHP <3 !!!
									}


								}



								?>
								</p>

						</div>
						</div>
						</center>
				<?php $i++;
					}
				}?>
				<?php
			}
		} else{
			echo '<h3>There are no results to show at this time.</h3>';
		}?>
    </div>
</div>




<div>
            <button type="button" class="btn btn-primary" onclick="window.location.replace('/student_search.php');">Back to search</button>
            <a href="/pdf.php"><button class="btn btn-primary">Download as PDF</button></a>
        </div>
        <br />
    </div>

    <div id="popup">
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-2 col-xs-1"></div>
            <div class="col-lg-8 col-md-9 col-sm-10 col-xs-11">
                <h1 id="loading" style="display:none">Loading image...</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-2 col-xs-1"></div>
            <div class="col-lg-8 col-md-9 col-sm-10 col-xs-11">
                <h1 id="no-image-error" style="display:none">Error: No photo found.</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <img download="photo.jpg" id="large-image"></img>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-2 col-xs-1"></div>
            <div class="col-xs-8">
                <a class="btn btn-primary" id="download">Download</a>
                <a class="btn btn-primary" id="close">Close</a>
            </div>
        </div>
    </div>





        <script type="text/javascript">
        // Assign callbacks after page loads.
        $(function() {

            $('.profile a').fullScreenPopup({
                bgColor: 'rgba(255,255,255,0.8)'
            }).click(function() {
                loadImage($(this).attr('data-student'))
				//console.log($(this).attr('data-student'));
            });

            $('#download').click(downloadImage);

            $('#close').click(closeImage);
        });

        // Callback functions defined here.
        var loadImage = function(id) {
			//console.log($('#large-image'));
            $('#no-image-error').hide();
            $('#loading').show();
            $('#large-image').hide();
            //setTimeout(function() {

			$.post("large_image.php", {uid:id}, function(response, status){
				//console.log(response);
				$('#loading').hide();
                if (response == '') {
                    $('#no-image-error').show();
					console.log("No image");
                } else {
                    $('#large-image').attr('src', "data:image/jpg;base64," + response);
                    $('#large-image').show();
					console.log(response);
					console.log("We have an image");
                }


			});

        }

        var downloadImage = function() {
            // Grab the image's uri
            var img_uri = $('#large-image')[0].src;
            //var img_data = img.src.remove(
            //var downloadLink = $("<a download='photo.jpg' href='" + img.src + "'></a>");

            // Fetch the image data, store it in a blob, then make a download link to it.
            var result = fetch(img_uri)
                .then(function(response) {
                    return response.blob();
                }).then(function(blob) {
                    console.log(blob);
                    var downloadLink = $("<a></a>").attr({
                        'value': 'Download',
                        'href': URL.createObjectURL(blob, {
                            type: 'application/octet-stream'
                        }),
                        'download': 'photo.jpg'
                    });

                    // We need to add the link to the page's body so FireFox can trigger the click event.
                    downloadLink.appendTo('body');
                    downloadLink[0].click();

                    // Remove it afterwards to keep the page clean.
                    downloadLink.remove();
                });
        }

        var closeImage = function() {
            /*console.log("clicked!");*/
            $('.fsp-close').click();
        }
    </script>


		<footer class="footer container-fluid">
			<?php include("base.html");?>
		</footer>
    </body>
</html>
