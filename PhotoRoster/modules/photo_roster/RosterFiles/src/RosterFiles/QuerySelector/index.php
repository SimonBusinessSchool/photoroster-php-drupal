<?php


	/* Create a class for your webservice structure, in this case: Contact */
	class Contact {
		public function __construct($id)
		{
			$this->id = $id;
		}
	}
	$connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
	$link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

	$link2 = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

	if( $link ) {
		//Empty arrays to hold data for later access
		$UID_stack = array();
		$people_id_stack = array();
		//$photo_image_stack = array();


		//Query for information
		$query = "SELECT cur.people_id, p.university_id
			FROM people p
			       INNER JOIN
			       (
			              SELECT people_id from people_faculty WHERE is_current = 1
			              UNION
			              SELECT people_id from people_students WHERE is_current = 1
			              UNION
			              SELECT people_id from people_staff WHERE is_current = 1
			       ) cur
			              ON cur.people_id = p.people_id
			left outer join (SELECT people_id, photo_id, primary_photo, photo_image, photo_type_id from people_photos where photo_type_id = 3) p1
			       ON P1.people_id = p.people_id
			where p1.people_id is null";

		$stmt = sqlsrv_query( $link, $query );
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
			array_push($UID_stack, $row['university_id']);
			array_push($people_id_stack, $row['people_id']);
		}

		/*for($i = 0; $i < 5; $i++){
			echo $UID_stack[$i]."<br /><br />";
		}*/
		date_default_timezone_set("America/New_York");
		/*echo sizeof($UID_stack);*/

		/* Initialize webservice with your WSDL */

		//$person = new Contact($UID_stack[0]);
		for($i = 0; $i < sizeof($UID_stack); $i++){
			$client = new SoapClient("https://photoweb.ur.rochester.edu/getImage.asmx?WSDL");
			$params = array(
			  "urid" => $UID_stack[$i],
			  "id" => "",
			  "pwd" => ""
			);

			/* Invoke webservice method with your parameters, in this case: Function1 */
			$response = $client->__soapCall("pullPhoto", array($params));
			if(isset($response->pullPhotoResult)){
				echo "<br /><br />".$UID_stack[$i]." Entered into database!<br />";
				$response_string = $response->pullPhotoResult;

				$injection_value = bin2hex($response_string);
				//echo $injection_value;
				$date = date('Y-m-d h:i:s');
				$parameters = array(
					$people_id_stack[$i],
					"1",
					$date,
					$UID_stack[$i].$i.".jpg",
					"0x".$injection_value,
					"3");
				echo $injection_value;
				//$unpacked = unpack('H*hex', $people_id_stack[$i]);
				$insert_query = "INSERT INTO people_photos (
					photo_id,
					people_id,
					primary_photo,
					photo_date,
					photo_filename,
					photo_image,
					photo_type_id)
	                VALUES (
	                    NEWID(),
	                    ?,
	                    ?,
	                    ?,
	                    ?,
	                    CAST (? AS varbinary(max)),
	                    ?)";
				$ins_stmt = sqlsrv_query( $link2, $insert_query, $parameters );
				$error = sqlsrv_errors(SQLSRV_ERR_ALL);
				if($ins_stmt === False){
					echo $error[0][0]."<br />".$error[0][1]."<br />".$error[0][2]."<br /><br />";
				}
			}else{
				echo "<br /><br />".$UID_stack[$i]." <br />Failed:<br />";
				echo "Bad response value";
			}

		}//end for




		if( $stmt === false ) {
			die( print_r( sqlsrv_errors(), true));
		}

	}else{
		//Filed Connection
		die( print_r( sqlsrv_errors(), true));
	}


?>
