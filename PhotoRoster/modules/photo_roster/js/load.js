function loading(){
	//document.getElementById("noloader").style.visibility = "hidden";
	document.getElementById("loader").style.display = "block";
}

function stopLoading(){
	document.getElementById("noloader").style.visibility = "visible";
	document.getElementById("loader").style.display = "none";
}

function logout() {
	window.location = '/photoroster/logout';
}

function login() {
	window.location = '/photoroster/login';
}
