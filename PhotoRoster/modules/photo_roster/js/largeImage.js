
//=============================Download Large Photo======================================
function downloadLargeImage(){
		console.log('Hello ' + document.getElementById('img01').getAttribute('class'));
		// Grab the image's uri
		var img_uri = $('#img01')[0].src;

		// Fetch the image data, store it in a blob, then make a download link to it.
		var result = fetch(img_uri)
			.then(function(response) {
				return response.blob();
			}).then(function(blob) {
				console.log(blob);
				var downloadLink = $("<a></a>").attr({
					'value': 'Download',
					'href': URL.createObjectURL(blob, {
						type: 'application/octet-stream'
					}),
					'download': 'photo.jpg'
				});

				// We need to add the link to the page's body so FireFox can trigger the click event.
				downloadLink.appendTo('body');
				downloadLink[0].click();

				// Remove it afterwards to keep the page clean.
				downloadLink.remove();
			});
}

//================Modal Image for Large Image=====================================
function selectPhoto(urid, lcv){
	// Get the modal
	console.log("I am here in downloadLargeImage function");

	var modal = document.getElementById('myModal');
	var modalImg = document.getElementById("img01");
	var captionText = document.getElementById("caption");
	var id = 'myImg'+lcv;

	$.post("/sites/default/files/large_image.php", //Required URL of the page on server
		{ // Data Sending With Request To Server
		uid:urid
		},
		function(response,status){ // Required Callback Function
			var img = "data:image/jpg;base64,"+response;
			modalImg.src = img;
		}
	);

	setTimeout(function(){modal.style.display = "block";}, 600);
	
	document.getElementById("closeButton").onclick = function() {
		modal.style.display = "none";
	}
}
