var datalist = document.getElementById('datalist');
var input = datalist.querySelector('input');
var list = datalist.querySelector('ul');
var submit = document.getElementById('submit');
var options = [];
var inputString = "";

function show(val){
  val.style.display = 'block';
}

function hide(val){
  if(!val){
	options.forEach(function(option){
	  option[1].style.display = 'none';
	});
  } else {
	val.style.display = 'none';
  }
}
function resetInput(){
  inputString = "";
  input.value = "";
  hide();
}

function getOptions(){
  [].slice.call(list.children).forEach(function(option){
	option.addEventListener('click', function(e){
	  inputString = e.target.innerHTML;
	  input.value = inputString;
	});
	options.push([option.innerHTML, option]);
  });
}
getOptions();

function checkOptions(){
  show(list);
  for(var j = 0; j < options.length; j++){
	var eachOption = "";
	if(options[j][0].toLowerCase().indexOf(inputString.toLowerCase()) !== -1){
	  show(options[j][1]);
	} else {
	  hide(options[j][1]);
	}
  }
}

input.addEventListener('input', function(e){
  var key = e.data;
  inputString = input.value;

	if(input.value === ""){
	  resetInput();
	  return;
	}
	checkOptions();

});

input.addEventListener('click', function(e){
  checkOptions();
  show(list);
});
// input.addEventListener('focus', function(e) {
//   checkOptions();
//   show(list);
// });

submit.addEventListener('click', function(e){
  resetInput();
});

document.addEventListener('click', function(e){
  if(!e.target.isEqualNode(input)){
	hide(list);
  }
});
