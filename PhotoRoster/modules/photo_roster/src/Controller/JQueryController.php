<?php



	namespace Drupal\photo_roster\Controller;

	use Drupal\Core\Controller\ControllerBase;
	use Drupal\Core\Database\Database;
	use Drupal\Core\Database\Driver\sqlsrv;


	class JQueryController extends ControllerBase{


		function init(){

			$return_array = array(
				'#theme' => 'jQuery',
				'#color' => 'Red',
				'#size' => '32px',
			);



			return $return_array;


		}

	}


?>
