<?php

	namespace Drupal\photo_roster\Controller;
	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\HttpFoundation\RedirectResponse;

	/* this form allows users to select based on
	* courses
	* This class usurps the course_search.php file
	*/

	$base_url;
	$breadcrumbURL;
	$breadcrumb_path;

	/**
	 * This class takes into accout all student, staff, and faculty search results and displays
	 * those results 
	 */
	class IndividualSearchResultsController extends ControllerBase{

		/**
		 * Main function to return student, staff, or faculty results based
		 * on an individual search criteria either first name last name, or 
		 * by URID. 
		 * TODO: break this function into smaller functions for more
		 * OOP standardization 
		 */
		public function ret_student_results(){
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}


			if(!isset($_GET['UID']) && ($_GET['first_name'] != NULL xor $_GET['last_name'] != NULL)){
				$first_name = str_replace("'", "", $_GET['first_name']);
				$last_name = str_replace("'", "", $_GET['last_name']);
			}
			if(!isset($_GET['UID']) && ($_GET['first_name'] != NULL && $_GET['last_name'] != NULL)){
				$first_name = str_replace("'", "", $_GET['first_name']);
				$last_name = str_replace("'", "", $_GET['last_name']);
			}
			if(isset($_GET['UID'])){

				$UID = str_replace("'", "", $_GET['UID']);

			}

			$value = '';
			//Connect to the database
			$connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
		    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

			if( $link ) {
			//==========================================================STUDENT=========================================================================
				if($_GET['usr'] == "student"){
				    //Empty arrays to hold data for later access
				    $preferred_name_stack = array();
				    $first_name_stack = array();
				    $last_name_stack = array();
				    $country_stack = array();
				    $program_stack = array();
				    $UID_stack = array();
					$value = 'student';
				    $placeholder_UID_stack = array();
				    $photo_image = array();
					$encoded_photos = array();

					global $base_url;
					global $breadcrumbURL;
					global $breadcrumb_path;

					if(!isset($_SESSION['photo_roster']['bc']['last_page'])){
						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/indiv_conn'];
						$breadcrumb_path = [0 => 'Home', 1 => 'Individual Student Search', 2 => 'Search Results'];
						$_SESSION['photo_roster']['bc']['last_page'] = \Drupal::request()->getRequestUri();
					}else{
						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/student_search', 2 => $base_url.'/photoroster/indiv_conn'];
						$breadcrumb_path = [0 => 'Home', 1 => 'Search Students', 2 => 'Individual Student Search', 3 => 'Search Results'];
					}

				    //Search by First Name OR Last Name
				    if(!isset($_GET['UID']) && ($_GET['first_name'] != NULL xor $_GET['last_name'] != NULL)){

				        //Query for information
				        $name_query = "SELECT distinct p.first_name, p.last_name, p.university_id, p.country, p.preferred_name, p.program
				            from SIS_Class_Roster r
				            inner join photo_roster_student_view p ON p.university_id = r.university_id
				            where p.first_name = '".$first_name."' or p.last_name = '".$last_name."'";

				        $name_stmt = sqlsrv_query( $link, $name_query );
				        while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
				            array_push($preferred_name_stack, $row['preferred_name']);
				            array_push($first_name_stack, $row['first_name']);
				            array_push($last_name_stack, $row['last_name']);
				            array_push($placeholder_UID_stack, $row['university_id']);
				            array_push($country_stack, $row['country']);
				            array_push($program_stack, $row['program']);
				        }
				        for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
				            //Query from another table for image based on UID
				            $photo_image_query = "SELECT p.photo_image from SIS_student_roster r
				                inner join photo_roster_student_view p ON p.university_id = r.university_id
				                WHERE p.university_id = '".$placeholder_UID_stack[$i]."'";

				            //populate photo_image with photo from temp UID
				            $stmt = sqlsrv_query( $link, $photo_image_query );
				            $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ;
				                array_push($photo_image, $row['photo_image']);

						}
						
						
						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}

				        if( $name_stmt === false ) {
				            die( print_r( sqlsrv_errors(), true));
				        }
					}

					//Search by First Name AND Last Name
					if(!isset($_GET['UID']) && ($_GET['first_name'] != NULL && $_GET['last_name'] != NULL)){

				        //Query for information
				        $name_query = "SELECT distinct p.first_name, p.last_name, p.university_id, p.country, p.preferred_name, p.program
				            from SIS_Class_Roster r
				            inner join photo_roster_student_view p ON p.university_id = r.university_id
				            where p.first_name = '".$first_name."' and p.last_name = '".$last_name."'";

				        $name_stmt = sqlsrv_query( $link, $name_query );
				        while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
				            array_push($preferred_name_stack, $row['preferred_name']);
				            array_push($first_name_stack, $row['first_name']);
				            array_push($last_name_stack, $row['last_name']);
				            array_push($placeholder_UID_stack, $row['university_id']);
				            array_push($country_stack, $row['country']);
				            array_push($program_stack, $row['program']);
				        }
				        for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
				            //Query from another table for image based on UID
				            $photo_image_query = "SELECT p.photo_image from SIS_student_roster r
				                inner join photo_roster_student_view p ON p.university_id = r.university_id
				                WHERE p.university_id = '".$placeholder_UID_stack[$i]."'";

				            //populate photo_image with photo from temp UID
				            $stmt = sqlsrv_query( $link, $photo_image_query );
				            $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ;
				                array_push($photo_image, $row['photo_image']);

				        }

						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}

				        if( $name_stmt === false ) {
				            die( print_r( sqlsrv_errors(), true));
				        }
					}

					//search by UID
					if(isset($_GET['UID'])){

				        $UID_query = "SELECT distinct p.first_name, p.last_name, p.university_id, p.preferred_name, p.country, p.program
				            from SIS_Class_Roster r
				            inner join photo_roster_student_view p ON p.university_id = r.university_id
				            where p.university_id = '".$UID."'";

				        $UID_stmt = sqlsrv_query( $link, $UID_query );
				        while( $row = sqlsrv_fetch_array( $UID_stmt, SQLSRV_FETCH_ASSOC ) ) {
				            array_push($preferred_name_stack, $row['preferred_name']);
				            array_push($first_name_stack, $row['first_name']);
				            array_push($last_name_stack, $row['last_name']);
				            array_push($UID_stack, $row['university_id']);
				            array_push($placeholder_UID_stack, $row['university_id']);
				            array_push($country_stack, $row['country']);
				            array_push($program_stack, $row['program']);
				        }

				        for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
				            //Query from another table for image based on UID
				            $photo_image_query2 = "SELECT p.photo_image from SIS_student_roster r
				                inner join photo_roster_student_view p ON p.university_id = r.university_id
				                WHERE p.university_id = '".$UID."'";

				            //populate photo_image with photo from temp UID
				            $uid_stmt = sqlsrv_query( $link, $photo_image_query2 );
				            $row = sqlsrv_fetch_array( $uid_stmt, SQLSRV_FETCH_ASSOC ) ;
				                array_push($photo_image, $row['photo_image']);
						}

						
						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}

				        if( $UID_stmt === false ) {
				            die( print_r( sqlsrv_errors(), true));
				        }
				    }
				}
				//==========================================================STAFF===========================================================================
				else if($_GET['usr'] == "staff"){

					//Empty arrays to hold data for later access
					$encoded_photos = array();
					$preferred_name_stack = array();
					$first_name_stack = array();
					$last_name_stack = array();
					$country_stack = array();
					$program_stack = array();
					$UID_stack = array();
					$placeholder_UID_stack = array();
					$photo_image = array();
					$value = 'staff';
					$department_stack = array();

					global $base_url;
					global $breadcrumbURL;
					global $breadcrumb_path;

					if(!isset($_SESSION['photo_roster']['bc']['last_page'])){
						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/staff_indiv_conn'];
						$breadcrumb_path = [0 => 'Home', 1 => 'Individual Staff Search', 2 => 'Search Results'];
						$_SESSION['photo_roster']['bc']['last_page'] = \Drupal::request()->getRequestUri();
					}else{
						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/staff_search', 2 => $base_url.'/photoroster/staff_indiv_conn'];
						$breadcrumb_path = [0 => 'Home', 1 => 'Search Staff', 2 => 'Individual Staff Search', 3 => 'Search Results'];
					}
					
					//Search by First Name OR Last Name
					if(!isset($_GET['UID']) && ($_GET['first_name'] != NULL xor $_GET['last_name'] != NULL)){

						//Query for information
						$name_query = "SELECT * FROM photo_roster_staff_view
							where first_name = '".$first_name."' or last_name = '".$last_name."'";
						

						$name_stmt = sqlsrv_query( $link, $name_query );
						
						while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
							//array_push($photo_image, $row['photo_image']);
							array_push($first_name_stack, $row['first_name']);
							array_push($last_name_stack, $row['last_name']);
							array_push($placeholder_UID_stack, $row['university_id']);
						}

						for($i = 0; $i < sizeof($last_name_stack); $i++){

							$department_query = "SELECT department,
								REVERSE(LEFT(REVERSE(department), ABS(PATINDEX('% %', REVERSE(department))-1))) as department_name
								from photo_roster_staff_view where university_id = '".$placeholder_UID_stack[$i]."'";

							$dpt = sqlsrv_query($link, $department_query);
				
							array_push($department_stack, sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC )['department_name']);
				

						}
						
						for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
							//Query from another table for image based on UID
							$photo_image_query = "SELECT photo_image from photo_roster_staff_view
								WHERE university_id = '".$placeholder_UID_stack[$i]."'";

							//populate photo_image with photo from temp UID
							$stmt = sqlsrv_query( $link, $photo_image_query );
							$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ;
								array_push($photo_image, $row['photo_image']);
						}
						
						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}

						if( $name_stmt === false ) {
							die( print_r( sqlsrv_errors(), true));
						}
					}

					
					///Search by First Name AND Last Name
					if(!isset($_GET['UID']) && ($_GET['first_name'] != NULL && $_GET['last_name'] != NULL)){

						//Query for information
						$name_query = "SELECT distinct p.first_name, p.last_name, p.university_id, p.country, p.preferred_name, p.program
							from SIS_Class_Roster r
							inner join photo_roster_staff_view p ON p.university_id = r.university_id
							where p.first_name = '".$first_name."' and p.last_name = '".$last_name."'";
						$department_query = "SELECT department,
							REVERSE(LEFT(REVERSE(department), ABS(PATINDEX('% %', REVERSE(department))-1))) as department_name
							from photo_roster_staff_view";

						$name_stmt = sqlsrv_query( $link, $name_query );
						$dpt = sqlsrv_query($link, $department_query);

						while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
							array_push($preferred_name_stack, $row['preferred_name']);
							array_push($first_name_stack, $row['first_name']);
							array_push($last_name_stack, $row['last_name']);
							array_push($placeholder_UID_stack, $row['university_id']);
							//array_push($country_stack, $row['country']);
							//array_push($program_stack, $row['program']);
						}

						while( $row = sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC ) ) {
							array_push($department_stack, $row['department_name']);
						}
						
						for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
							//Query from another table for image based on UID
							$photo_image_query = "SELECT photo_image from photo_roster_staff_view
								WHERE university_id = '".$placeholder_UID_stack[$i]."'";

							//populate photo_image with photo from temp UID
							$stmt = sqlsrv_query( $link, $photo_image_query );
							$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ;
								array_push($photo_image, $row['photo_image']);
						}

						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}

						if( $name_stmt === false ) {
							die( print_r( sqlsrv_errors(), true));
						}
					}
					
					///Search by UID
					if(isset($_GET['UID'])){
						$UID_query = "SELECT distinct first_name, last_name, university_id
							from photo_roster_staff_view
							where university_id = '".$UID."'";
						$department_query = "SELECT department,
							REVERSE(LEFT(REVERSE(department), ABS(PATINDEX('% %', REVERSE(department))-1))) as department_name
							from photo_roster_staff_view";

						$UID_stmt = sqlsrv_query( $link, $UID_query );
						$dpt = sqlsrv_query($link, $department_query);

						while( $row = sqlsrv_fetch_array( $UID_stmt, SQLSRV_FETCH_ASSOC ) ) {
							array_push($first_name_stack, $row['first_name']);
							array_push($last_name_stack, $row['last_name']);
							array_push($UID_stack, $row['university_id']);
							array_push($placeholder_UID_stack, $row['university_id']);
						}

						while( $row = sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC ) ) {
							array_push($department_stack, $row['department_name']);
						}

						for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
							//Query from another table for image based on UID
							$photo_image_query2 = "SELECT photo_image from photo_roster_staff_view
								WHERE university_id = '".$UID."'";

							//populate photo_image with photo from temp UID
							$uid_stmt = sqlsrv_query( $link, $photo_image_query2 );
							$row = sqlsrv_fetch_array( $uid_stmt, SQLSRV_FETCH_ASSOC ) ;
								array_push($photo_image, $row['photo_image']);
						}

						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}

						if( $UID_stmt === false ) {
							die( print_r( sqlsrv_errors(), true));
						}
					}
				}
			//==========================================================FACULTY=========================================================================
				else if($_GET['usr'] == "faculty"){

					//Empty arrays to hold data for later access
					$encoded_photos = array();
				    $preferred_name_stack = array();
				    $first_name_stack = array();
				    $last_name_stack = array();
				    $country_stack = array();
				    $program_stack = array();
				    $UID_stack = array();
				    $placeholder_UID_stack = array();
				    $photo_image = array();
					$value = 'faculty';
					$department_stack = array();

					global $base_url;
					global $breadcrumbURL;
					global $breadcrumb_path;

					if(!isset($_SESSION['photo_roster']['bc']['last_page'])){
						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/faculty_indiv_conn'];
						$breadcrumb_path = [0 => 'Home', 1 => 'Individual Faculty Search', 2 => 'Search Results'];
					}else{
						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/faculty_search', 2 => $base_url.'/photoroster/faculty_indiv_conn'];
						$breadcrumb_path = [0 => 'Home', 1 => 'Search Faculty', 2 => 'Individual Faculty Search', 3 => 'Search Results'];
					}

				    //Search by First OR Last Name
				    if(!isset($_GET['UID']) && ($_GET['first_name'] != NULL xor $_GET['last_name'] != NULL)){

				        //Query for information
				        $name_query = "SELECT * FROM photo_roster_faculty_view
							where first_name = '".$first_name."' or last_name = '".$last_name."'";
						$department_query = "SELECT department,
							REVERSE(LEFT(REVERSE(department), ABS(PATINDEX('% %', REVERSE(department))-1))) as department_name
							from photo_roster_faculty_view";

						$name_stmt = sqlsrv_query( $link, $name_query );
						$dpt = sqlsrv_query($link, $department_query);

				        while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
				            array_push($first_name_stack, $row['first_name']);
				            array_push($last_name_stack, $row['last_name']);
				            array_push($placeholder_UID_stack, $row['university_id']);
						}
						
						while( $row = sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC ) ) {
							array_push($department_stack, $row['department_name']);
						}
						
				        for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
				            //Query from another table for image based on UID
				            $photo_image_query = "SELECT photo_image from photo_roster_faculty_view
				                WHERE university_id = '".$placeholder_UID_stack[$i]."'";

				            //populate photo_image with photo from temp UID
				            $stmt = sqlsrv_query( $link, $photo_image_query );
				            $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ;
				                array_push($photo_image, $row['photo_image']);

						}
						
						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}

				        if( $name_stmt === false ) {
				            die( print_r( sqlsrv_errors(), true));
				        }
					}


				    ///Search by First AND Last Name
					if(!isset($_GET['UID']) && ($_GET['first_name'] != NULL && $_GET['last_name'] != NULL)){

				        //Query for information
				        $name_query = "SELECT distinct first_name, last_name, university_id
				            from photo_roster_faculty_view
							where first_name = '".$first_name."' and last_name = '".$last_name."'";
						$department_query = "SELECT department,
							REVERSE(LEFT(REVERSE(department), ABS(PATINDEX('% %', REVERSE(department))-1))) as department_name
							from photo_roster_faculty_view";	

						$name_stmt = sqlsrv_query( $link, $name_query );
						$dpt = sqlsrv_query($link, $department_query);

				        while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
				            array_push($first_name_stack, $row['first_name']);
				            array_push($last_name_stack, $row['last_name']);
				            array_push($placeholder_UID_stack, $row['university_id']);
						}
						
						while( $row = sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC ) ) {
							array_push($department_stack, $row['department_name']);
						}

				        for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
				            //Query from another table for image based on UID
				            $photo_image_query = "SELECT photo_image from photo_roster_faculty_view
				                WHERE university_id = '".$placeholder_UID_stack[$i]."'";

				            //populate photo_image with photo from temp UID
				            $stmt = sqlsrv_query( $link, $photo_image_query );
				            $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ;
				                array_push($photo_image, $row['photo_image']);
						}

						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}
						
				        if( $name_stmt === false ) {
				            die( print_r( sqlsrv_errors(), true));
				        }
					}

					///Search by UID
					if(isset($_GET['UID'])){
						
				        $UID_query = "SELECT distinct first_name, last_name, university_id
				            from photo_roster_faculty_view
							where university_id = '".$UID."'";
						$department_query = "SELECT department,
							REVERSE(LEFT(REVERSE(department), ABS(PATINDEX('% %', REVERSE(department))-1))) as department_name
							from photo_roster_faculty_view";

						$UID_stmt = sqlsrv_query( $link, $UID_query );
						$dpt = sqlsrv_query($link, $department_query);

				        while( $row = sqlsrv_fetch_array( $UID_stmt, SQLSRV_FETCH_ASSOC ) ) {
				            array_push($first_name_stack, $row['first_name']);
				            array_push($last_name_stack, $row['last_name']);
				            array_push($UID_stack, $row['university_id']);
				            array_push($placeholder_UID_stack, $row['university_id']);
						}
						
						while( $row = sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC ) ) {
							array_push($department_stack, $row['department_name']);
						}

				        for($i = 0; $i < sizeof($placeholder_UID_stack); $i++){
				            //Query from another table for image based on UID
				            $photo_image_query2 = "SELECT photo_image from photo_roster_faculty_view
				                WHERE university_id = '".$UID."'";

				            //populate photo_image with photo from temp UID
				            $uid_stmt = sqlsrv_query( $link, $photo_image_query2 );
				            $row = sqlsrv_fetch_array( $uid_stmt, SQLSRV_FETCH_ASSOC ) ;
				                array_push($photo_image, $row['photo_image']);
						}
						
						foreach ($photo_image as $photo){
							array_push($encoded_photos, base64_encode($photo));
						}


				        if( $UID_stmt === false ) {
				            die( print_r( sqlsrv_errors(), true));
				        }
					}
				}//END FACULTY
				

				\Drupal::service('page_cache_kill_switch')->trigger();
				if($first_name_stack != NULL){
					$student_form = array(
						'#theme' => 'stu_indiv_res',
						'#first_name' => $first_name_stack,
						'#preferred_name' => $preferred_name_stack,
						'#last_name' => $last_name_stack,
						'#photo_image' => $encoded_photos,
						'#value' => $value,
						'#breadcrumb' => $breadcrumb_path,
						'#urlVal' => $breadcrumbURL,
						'#uid' => $placeholder_UID_stack,
						'#dept' => $department_stack,
					);

					$student_form['blank_space'] = array(
						'#markup' => '<br /><br /><br /><br /><br /><br />',
					);
				}
				else{
					$student_form = array(
						'#theme' => 'form-theme',
						'#pageTitle' => 'Sorry no results returned',
						'#value' => $value,
						'#breadcrumb' => $breadcrumb_path,
						'#urlVal' => $breadcrumbURL,
					);

					$student_form['blank_space'] = array(
						'#markup' => '<center><h3>Sorry no results returned</h3></center><br /><br /><br /><br /><br /><br />',
					);
				}
					
				return $student_form;
			}
		}
	}

?>
