<?php

	namespace Drupal\photo_roster\Controller;

	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\HttpFoundation\RedirectResponse;

	//Global arrays for breadcrumbing
	$breadcrumbURL = array();
	$breadcrumb_path = array();


	class RosterResultsController extends ControllerBase{

		/**
		* {@inheritdoc}
		*/
		public function build_roster(){

			session_start();
			
			\Drupal::service('page_cache_kill_switch')->trigger();
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			$tempstore = \Drupal::service('user.private_tempstore')->get('photo_roster');
			$last_page_visited = $tempstore->get('uri_path');
			$return_array = array();
			$expressions = array();
			$encoded_photos = array();
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$link = sqlsrv_connect( $serverName, $connectionInfo);
			if(isset($_GET['course'])){
		        array_push($expressions, str_replace("_", " ", $_GET['term']), $_GET['course'], $_GET['section']);

		        if( $link ) {

		            //Empty arrays to hold data for later access
		            $first_name_stack = array();       //Student first name
		            $preferred_name_stack = array();   //Student preferred name
		            $last_name_stack = array();        //Student last name
		            $country_stack = array();         //Country
		            $program_stack = array();         //Program of Study
		            $photo_image = array();            //Photo image location
		            $UID = array();                   //University ID

		            if($expressions[2] != '*'){
		                //Queries the unique alpha index identifier
		            $roster_query = "SELECT p.*, r.course_number, r.term, r.section_name
		                from SIS_Class_Roster r
		                inner join photo_roster_student_view p ON p.university_id = r.university_id
		                where term = '".$expressions[0]."' and course_number = '".$expressions[1]."'
		                and section_name = '".$expressions[2]."'
		                order by p.last_name";
		            }else{
		            $roster_query = "SELECT p.*, r.course_number, r.term, r.section_name
		                from SIS_Class_Roster r
		                inner join photo_roster_student_view p ON p.university_id = r.university_id
		                where term = '".$expressions[0]."' and course_number = '".$expressions[1]."'
		                order by p.last_name";
		            }

		            $stmt = sqlsrv_query( $link, $roster_query );


		            //Pull information for the roster based on input data
		            while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
		                array_push($preferred_name_stack, $row['preferred_name']);
		                array_push($first_name_stack, $row['first_name']);
		                array_push($last_name_stack, $row['last_name']);
		                array_push($country_stack, $row['country']);
		                array_push($program_stack, $row['program']);
		                array_push($photo_image, $row['photo_image']);
						array_push($UID, $row['university_id']);
		            }

					foreach ($photo_image as $photo){
						array_push($encoded_photos, base64_encode($photo));
					}
					$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
					$_SESSION['photo_roster']['first_name'] = $first_name_stack;
					$_SESSION['photo_roster']['last_name'] = $last_name_stack;
					$_SESSION['photo_roster']['country'] = $country_stack;
					$_SESSION['photo_roster']['program'] = $program_stack;
					$_SESSION['photo_roster']['photo_image'] = $photo_image;
					$_SESSION['photo_roster']['value'] = 'course';
					$_SESSION['photo_roster']['selprog'] = $expressions[0];
					$_SESSION['photo_roster']['year'] = $expressions[1];
					unset($_SESSION['photo_roster']['section']);
					$_SESSION['photo_roster']['section'] = $expressions[2];
					unset($_SESSION['photo_roster']['dept']);

					global $base_url;
					global $breadcrumbURL;
					global $breadcrumb_path;
					if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){

						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/course_search'];
						$breadcrumb_path = [0 => 'Home', 1 => 'Course Selection', 2 => 'Results'];
					}else{
						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/student_search',
						2 => $base_url.'/photoroster/course_search'];
						$breadcrumb_path = [0 => 'Home', 1 => 'Student Search', 2 => 'Course Selection', 3 => 'Results'];
					}

					$return_array = array(
						'#theme' => 'photos',
						'#first_name' => $first_name_stack,
						'#last_name' => $last_name_stack,
						'#preferred_name' => $preferred_name_stack,
						'#country' => $country_stack,
						'#program' => $program_stack,
						'#photo_image' => $encoded_photos,
						'#uid' => $UID,
						'#last_page' => $last_page_visited,
						'#selected_term' => $expressions[0],
						'#selected_course' => $expressions[1],
						'#selected_section' => $expressions[2],
						'#value' => 'course',
						'#breadcrumb' => $breadcrumb_path,
						'#urlVal' => $breadcrumbURL,
					);


		            if( $stmt === false ) {
		                die( print_r( sqlsrv_errors(), true));
		            }

		        }
		        else{
		            echo "Connection could not be established.<br />";
		            die( print_r( sqlsrv_errors(), true ) );
		        }

		    }

		    //Pulling in from program search
		    if(isset($_GET['program'])){
				//echo('I am in the program<br />');
		        array_push($expressions, $_GET['program'], $_GET['year']);
		        if( $link ) {

		            //Empty arrays to hold data for later access
		            $preferred_name_stack = array();   //Student preferred name
		            $first_name_stack = array();       //Student first name
		            $last_name_stack = array();        //Student last name
		            $country_stack = array();         //Current Employer
		            $program_stack = array();         //Program of Study
		            $photo_image = array();            //Photo image location
		            $UID = array();                   //University ID

		            //Queries the unique alpha index identifier
		            $roster_query = "SELECT r.preferred_name, r.first_name, r.last_name, p.country, p.photo_image, p.program, p.university_id from SIS_student_roster r
		                inner join photo_roster_student_view p ON p.university_id = r.university_id
		                WHERE p.program = '".$expressions[0]."' and p.grad_year = '".$expressions[1]."'
		                ORDER BY p.last_name";


		            $stmt = sqlsrv_query( $link, $roster_query );

		            //Pull information for the roster based on input data
		            while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
		                array_push($preferred_name_stack, $row['preferred_name']);
		                array_push($first_name_stack, $row['first_name']);
		                array_push($last_name_stack, $row['last_name']);
		                array_push($country_stack, $row['country']);
		                array_push($program_stack, $row['program']);
		                array_push($photo_image, $row['photo_image']);
		                array_push($UID, $row['university_id']);
		            }

					//Removes any single quotes (') from names so it won't crash the query
					for($i = 0; $i < sizeof($last_name_stack); $i++){
						if(strpos($last_name_stack[$i], "'")) {
							$last_name_stack[$i] = str_replace("'", "", $last_name_stack[$i]);
						}
					}

		            if( $stmt === false) {
		                die( print_r( sqlsrv_errors(), true));
		            }
					foreach ($photo_image as $photo){
						array_push($encoded_photos, base64_encode($photo));
					}

					$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
					$_SESSION['photo_roster']['first_name'] = $first_name_stack;
					$_SESSION['photo_roster']['last_name'] = $last_name_stack;
					$_SESSION['photo_roster']['country'] = $country_stack;
					$_SESSION['photo_roster']['program'] = $program_stack.array('staff' => false);
					$_SESSION['photo_roster']['photo_image'] = $photo_image;
					$_SESSION['photo_roster']['selprog'] = $expressions[0].' Class of ';
					$_SESSION['photo_roster']['year'] = $expressions[1];
					$_SESSION['photo_roster']['value'] = 'program';
					unset($_SESSION['photo_roster']['dept']);

					global $base_url;
					global $breadcrumbURL;
					global $breadcrumb_path;
					if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){

						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/conn_prog', 2 => $base_url.$_SESSION['photo_roster']['bc']['last']];
						$breadcrumb_path = [0 => 'Home', 1 => 'Program Selection', 2 => 'Graduating Year', 3 => 'Results'];
					}else{
						$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/student_search',
						2 => $base_url.'/photoroster/conn_prog', 3 => $base_url.$_SESSION['photo_roster']['bc']['last']];
						$breadcrumb_path = [0 => 'Home', 1 => 'Student Search', 2 => 'Program Selection', 3 => 'Graduating Year', 4 => 'Results'];
					}

					$return_array = array(
						'#theme' => 'photos',
						'#selected_program' => $expressions[0],
						'#graduating_year' => $expressions[1],
						'#first_name' => $first_name_stack,
						'#last_name' => $last_name_stack,
						'#preferred_name' => $preferred_name_stack,
						'#country' => $country_stack,
						'#program' => $program_stack,
						'#photo_image' => $encoded_photos,
						'#uid' => $UID,
						'#value' => 'program',
						'#breadcrumb' => $breadcrumb_path,
						'#urlVal' => $breadcrumbURL,
					);
		        }
		        else{
		            echo "Connection could not be established.<br />";
		            die( print_r( sqlsrv_errors(), true ) );
		        }
		    }

			//pulling in data from staff
			if(isset($_GET['value']) && $_GET['value'] == "staff"){
				return $this->allStaff();
			}
			//pulling in data from faculty
			if(isset($_GET['value']) && $_GET['value'] == "faculty"){
				return $this->allFaculty();
			}
			//pulling in data from all
			if(isset($_GET['value']) && $_GET['value'] == "all"){
				return $this->allSimon();
			}
			return $return_array;
		}

		public function allStaff(){
			session_start();
			\Drupal::service('page_cache_kill_switch')->trigger();
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}

			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;
			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Individual Staff Search'];
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/staff_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Staff Search', 2 => 'Individual Staff Search'];
			}

			$tempstore = \Drupal::service('user.private_tempstore')->get('photo_roster');
			$last_page_visited = $tempstore->get('uri_path');
			$return_array = array();
			$encoded_photos = array();

			$first_name_stack = array();       //Student first name
			$last_name_stack = array();        //Student last name
			$photo_image = array();            //Photo image location
			$department_stack = array();

			$UID = array();                   //University ID
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$link = sqlsrv_connect( $serverName, $connectionInfo);
			//Queries the unique alpha index identifier
			$staff_view_query = "SELECT * FROM photo_roster_staff_view
				order by last_name";
			

			$stmt = sqlsrv_query( $link, $staff_view_query );
			
			//Pull information for the roster based on input data
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
				array_push($first_name_stack, $row['first_name']);
				array_push($last_name_stack, $row['last_name']);
				array_push($photo_image, $row['photo_image']);
				array_push($UID, $row['university_id']);
			}

			for($i = 0; $i < sizeof($last_name_stack); $i++){

				$department_query = "SELECT department,
					REVERSE(LEFT(REVERSE(department), ABS(PATINDEX('% %', REVERSE(department))-1))) as department_name
					from photo_roster_staff_view where university_id = '".$UID[$i]."'";

				$dpt = sqlsrv_query($link, $department_query);
				
				array_push($department_stack, sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC )['department_name']);
				

			}

			//Removes any single quotes (') from names so it won't crash the query
			for($i = 0; $i < sizeof($last_name_stack); $i++){
				if(strpos($last_name_stack[$i], "'")) {
					$last_name_stack[$i] = str_replace("'", "", $last_name_stack[$i]);
				}
			}


			foreach ($photo_image as $photo){
				array_push($encoded_photos, base64_encode($photo));
			}

			//$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
			$_SESSION['photo_roster']['first_name'] = $first_name_stack;
			$_SESSION['photo_roster']['last_name'] = $last_name_stack;
			unset($_SESSION['photo_roster']['country']);
			unset($_SESSION['photo_roster']['program']);
			$_SESSION['photo_roster']['photo_image'] = $photo_image;
			$_SESSION['photo_roster']['value'] = 'staff';
			$_SESSION['photo_roster']['dept'] = $department_stack;

			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;
			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'All Staff Results'];
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/staff_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Staff Search', 2 => 'All Staff Results'];
			}

			$return_array = array(
				'#theme' => 'photos',
				'#first_name' => $first_name_stack,
				'#last_name' => $last_name_stack,
				//'#preferred_name' => $preferred_name_stack,
				'#photo_image' => $encoded_photos,
				'#last_page' => $last_page_visited,
				'#uid' => $UID,
				'#breadcrumb' => $breadcrumb_path,
				'#urlVal' => $breadcrumbURL,
				'#value' => 'staff',
				'#dept' => $department_stack,
			);
			return $return_array;
		}

		public function allFaculty(){

			session_start();
			
			\Drupal::service('page_cache_kill_switch')->trigger();
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}

			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;


			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Individual Faculty Search'];
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/faculty_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Faculty Search', 2 => 'Individual Faculty Search'];
			}


			$tempstore = \Drupal::service('user.private_tempstore')->get('photo_roster');
			$last_page_visited = $tempstore->get('uri_path');
			$return_array = array();
			$encoded_photos = array();
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$link = sqlsrv_connect( $serverName, $connectionInfo);

			$first_name_stack = array();       //Student first name
			$last_name_stack = array();        //Student last name
			$photo_image = array();            //Photo image location
			$UID = array();                   //University ID
			$department_stack = array();

			//Queries the unique alpha index identifier
			$staff_view_query = "SELECT * FROM photo_roster_faculty_view
				order by last_name";
			$department_query = "SELECT department FROM photo_roster_faculty_view";

			$stmt = sqlsrv_query( $link, $staff_view_query );
			$dpt = sqlsrv_query($link, $department_query);

			//Pull information for the roster based on input data
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
				array_push($first_name_stack, $row['first_name']);
				array_push($last_name_stack, $row['last_name']);
				array_push($photo_image, $row['photo_image']);
				array_push($UID, $row['university_id']);
			}
			while( $row = sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC ) ) {
				array_push($department_stack, $row['department']);
			}

			//Removes any single quotes (') from names so it won't crash the query
			for($i = 0; $i < sizeof($last_name_stack); $i++){
				if(strpos($last_name_stack[$i], "'")) {
					$last_name_stack[$i] = str_replace("'", "", $last_name_stack[$i]);
				}
			}

			foreach ($photo_image as $photo){
				array_push($encoded_photos, base64_encode($photo));
			}

			//$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
			$_SESSION['photo_roster']['first_name'] = $first_name_stack;
			$_SESSION['photo_roster']['last_name'] = $last_name_stack;
			unset($_SESSION['photo_roster']['country']);
			unset($_SESSION['photo_roster']['program']);
			$_SESSION['photo_roster']['photo_image'] = $photo_image;
			$_SESSION['photo_roster']['value'] = 'faculty';
			$_SESSION['photo_roster']['dept'] = $department_stack;

			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;
			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){

				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'All Staff Results'];
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/faculty_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Faculty Search', 2 => 'All Faculty Results'];
			}

			$return_array = array(
				'#theme' => 'photos',
				'#first_name' => $first_name_stack,
				'#last_name' => $last_name_stack,
				//'#preferred_name' => $preferred_name_stack,
				'#photo_image' => $encoded_photos,
				'#last_page' => $last_page_visited,
				'#uid' => $UID,
				'#breadcrumb' => $breadcrumb_path,
				'#urlVal' => $breadcrumbURL,
				'#value' => 'faculty',
				'#dept' => $department_stack,
			);

			return $return_array;
		}

		public function generatePDFs(){
			session_start();
		    \Drupal::service('page_cache_kill_switch')->trigger();

			if (!$_SESSION['photo_roster']['user_id'] == '646176652e77726f626c6577736b69') {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}

			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$link = sqlsrv_connect( $serverName, $connectionInfo);

			$encoded_photos = array();
			$first_name_stack = array();       //Student first name
			$last_name_stack = array();        //Student last name
			$preferred_name_stack = array();	//Preferred name for all of Simon students/faculty/staff
			$photo_image = array();            //Photo image location
			if($link){

//========================= 1 TO 1000 ======================================================================
				$staff_view_query = ";WITH Selection AS(
						SELECT ROW_NUMBER() OVER (order by sort_order, last_name) AS 'RowNumber', *
						FROM photo_roster_all_view
					)
					SELECT * FROM selection
					WHERE RowNumber BETWEEN 1 AND 1000
					order by sort_order, last_name";
				$stmt = sqlsrv_query( $link, $staff_view_query );
				$lcv = 0;
				//Pull information for the roster based on input data
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
					array_push($first_name_stack, $row['first_name']);
					array_push($last_name_stack, $row['last_name']);
					array_push($preferred_name_stack, $row['preferred_name']);
					array_push($photo_image, $row['photo_image']);

					if(strpos($last_name_stack[$lcv], "'")) {
						$last_name_stack[$lcv] = str_replace("'", "", $last_name_stack[$lcv]);
					}
					array_push($encoded_photos, base64_encode($photo_image[$lcv]));
					$lcv++;
				}
				$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
				$_SESSION['photo_roster']['first_name'] = $first_name_stack;
				$_SESSION['photo_roster']['last_name'] = $last_name_stack;
				$_SESSION['photo_roster']['photo_image'] = $photo_image;
				//$pdf = PDFViewController::init();
				//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/sites/default/files/1to1000.pdf', $pdf->array());
				unset($first_name_stack);
				unset($last_name_stack);
				unset($preferred_name_stack);
				unset($photo_image);

//======================== 1001 TO 2000 ==================================================================
				$staff_view_query = ";WITH Selection AS(
						SELECT ROW_NUMBER() OVER (order by sort_order, last_name) AS 'RowNumber', *
						FROM photo_roster_all_view
					)
					SELECT * FROM selection
					WHERE RowNumber BETWEEN 1001 AND 2000
					order by sort_order, last_name";
				$stmt = sqlsrv_query( $link, $staff_view_query );
				$lcv = 0;
				//Pull information for the roster based on input data
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
					array_push($first_name_stack, $row['first_name']);
					array_push($last_name_stack, $row['last_name']);
					array_push($preferred_name_stack, $row['preferred_name']);
					array_push($photo_image, $row['photo_image']);

					if(strpos($last_name_stack[$lcv], "'")) {
						$last_name_stack[$lcv] = str_replace("'", "", $last_name_stack[$lcv]);
					}
					array_push($encoded_photos, base64_encode($photo_image[$lcv]));
					$lcv++;
				}
				$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
				$_SESSION['photo_roster']['first_name'] = $first_name_stack;
				$_SESSION['photo_roster']['last_name'] = $last_name_stack;
				$_SESSION['photo_roster']['photo_image'] = $photo_image;
				$pdf = PDFViewController::init();
				file_put_contents($_SERVER['DOCUMENT_ROOT'].'/sites/default/files/1001to2000.pdf', $pdf);
				unset($first_name_stack);
				unset($last_name_stack);
				unset($preferred_name_stack);
				unset($photo_image);

//========================= 2001 TO 3000 ==================================================================
				$staff_view_query = ";WITH Selection AS(
						SELECT ROW_NUMBER() OVER (order by sort_order, last_name) AS 'RowNumber', *
						FROM photo_roster_all_view
					)
					SELECT * FROM selection
					WHERE RowNumber BETWEEN 2001 AND 3000
					order by sort_order, last_name";
				$stmt = sqlsrv_query( $link, $staff_view_query );
				$lcv = 0;
				//Pull information for the roster based on input data
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
					array_push($first_name_stack, $row['first_name']);
					array_push($last_name_stack, $row['last_name']);
					array_push($preferred_name_stack, $row['preferred_name']);
					array_push($photo_image, $row['photo_image']);

					if(strpos($last_name_stack[$lcv], "'")) {
						$last_name_stack[$lcv] = str_replace("'", "", $last_name_stack[$lcv]);
					}
					array_push($encoded_photos, base64_encode($photo_image[$lcv]));
					$lcv++;
				}
				$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
				$_SESSION['photo_roster']['first_name'] = $first_name_stack;
				$_SESSION['photo_roster']['last_name'] = $last_name_stack;
				$_SESSION['photo_roster']['photo_image'] = $photo_image;
				$pdf = PDFViewController::init();
				file_put_contents($_SERVER['DOCUMENT_ROOT'].'/sites/default/files/2001to3000.pdf', $pdf);
				unset($first_name_stack);
				unset($last_name_stack);
				unset($preferred_name_stack);
				unset($photo_image);

//======================== 3001 TO 4000 =================================================================
				$staff_view_query = ";WITH Selection AS(
						SELECT ROW_NUMBER() OVER (order by sort_order, last_name) AS 'RowNumber', *
						FROM photo_roster_all_view
					)
					SELECT * FROM selection
					WHERE RowNumber BETWEEN 3001 AND 4000
					order by sort_order, last_name";
				$stmt = sqlsrv_query( $link, $staff_view_query );
				$lcv = 0;
				//Pull information for the roster based on input data
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
					array_push($first_name_stack, $row['first_name']);
					array_push($last_name_stack, $row['last_name']);
					array_push($preferred_name_stack, $row['preferred_name']);
					array_push($photo_image, $row['photo_image']);

					if(strpos($last_name_stack[$lcv], "'")) {
						$last_name_stack[$lcv] = str_replace("'", "", $last_name_stack[$lcv]);
					}
					array_push($encoded_photos, base64_encode($photo_image[$lcv]));
					$lcv++;
				}
				$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
				$_SESSION['photo_roster']['first_name'] = $first_name_stack;
				$_SESSION['photo_roster']['last_name'] = $last_name_stack;
				$_SESSION['photo_roster']['photo_image'] = $photo_image;
				$pdf = PDFViewController::init();
				file_put_contents($_SERVER['DOCUMENT_ROOT'].'/sites/default/files/3001to4000.pdf', $pdf);
				unset($first_name_stack);
				unset($last_name_stack);
				unset($preferred_name_stack);
				unset($photo_image);

//======================== 4001 TO 4510 ==============================================================================
				$staff_view_query = ";WITH Selection AS(
						SELECT ROW_NUMBER() OVER (order by sort_order, last_name) AS 'RowNumber', *
						FROM photo_roster_all_view
					)
					SELECT * FROM selection
					WHERE RowNumber BETWEEN 4001 AND 4510
					order by sort_order, last_name";
				$stmt = sqlsrv_query( $link, $staff_view_query );
				$lcv = 0;
				//Pull information for the roster based on input data
				while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
					array_push($first_name_stack, $row['first_name']);
					array_push($last_name_stack, $row['last_name']);
					array_push($preferred_name_stack, $row['preferred_name']);
					array_push($photo_image, $row['photo_image']);

					if(strpos($last_name_stack[$lcv], "'")) {
						$last_name_stack[$lcv] = str_replace("'", "", $last_name_stack[$lcv]);
					}
					array_push($encoded_photos, base64_encode($photo_image[$lcv]));
					$lcv++;
				}
				$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
				$_SESSION['photo_roster']['first_name'] = $first_name_stack;
				$_SESSION['photo_roster']['last_name'] = $last_name_stack;
				$_SESSION['photo_roster']['photo_image'] = $photo_image;
				$pdf = PDFViewController::init();
				file_put_contents($_SERVER['DOCUMENT_ROOT'].'/sites/default/files/4001to4510.pdf', $pdf);
				unset($first_name_stack);
				unset($last_name_stack);
				unset($preferred_name_stack);
				unset($photo_image);
			}

			exit();
		}

		function create_File($file){
			//$file = $_SERVER['DOCUMENT_ROOT'].'/sites/default/files/'.$file_name;
			$handle = fopen($file, "w");
			$content = fread($handle, filesize($file));

			// save PDF buffer
			file_put_contents($file, $content);

			// ensure we don't have any previous output
			if(headers_sent()){
				exit("PDF stream will be corrupted - there is already output from previous code.");
			}

			// force download dialog
			header('Content-Type: application/force-download');
			header('Content-Type: application/octet-stream', false);
			header('Content-Type: application/download', false);

			// use the Content-Disposition header to supply a recommended filename
			header('Content-Disposition: attachment; filename="'.basename($file).'";');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: '.filesize($file));
			header('Content-Type: application/pdf', false);

			// send binary stream directly into buffer rather than into memory
			readfile($file);

			return;

		}


		function displayPDF(){
			$file = $_SERVER['DOCUMENT_ROOT'].'/sites/default/files/FullRoster.pdf';
			$handle = fopen($file, "r");
			$content = fread($handle, filesize($file));

			// save PDF buffer
			file_put_contents($file, $content);

			// ensure we don't have any previous output
			if(headers_sent()){
				exit("PDF stream will be corrupted - there is already output from previous code.");
			}

			//Display in window
			header('Content-Type: application/pdf', true);
			header('Content-Disposition: inline; filename="FullRoster.pdf"', true);

			// send binary stream directly into buffer rather than into memory
			readfile($file);

			// make sure stream ended
			exit();
		}


		public function allSimon(){
			session_start();
		 	\Drupal::service('page_cache_kill_switch')->trigger();
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			$return_array = array();
			$encoded_photos = array();
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$link = sqlsrv_connect( $serverName, $connectionInfo);

			$first_name_stack = array();       //Student first name
			$last_name_stack = array();        //Student last name
			$preferred_name_stack = array();	//Preferred name for all of Simon students/faculty/staff
			$program_stack = array();			//Program for students
			//$job_title_stack = array();			//Job title for employees (staff/faculty)
			$photo_image = array();            //Photo image location
			$UID = array();                   //University ID
			$department_stack = array();
			$country_stack = array();

			//Queries the unique alpha index identifier
			$staff_view_query = "SELECT * FROM photo_roster_all_view
				order by sort_order, last_name";
			$department_query = "SELECT department,
				REVERSE(LEFT(REVERSE(department), ABS(PATINDEX('% %', REVERSE(department))-1))) as department_name
				from photo_roster_staff_view";

			$stmt = sqlsrv_query( $link, $staff_view_query );
			$dpt = sqlsrv_query($link, $department_query);

			$lcv = 0;

			//Pull information for the roster based on input data
			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
				array_push($first_name_stack, $row['first_name']);
				array_push($last_name_stack, $row['last_name']);
				array_push($preferred_name_stack, $row['preferred_name']);
				array_push($photo_image, $row['photo_image']);
				array_push($UID, $row['university_id']);
				//Uncomment if needed
				array_push($program_stack, $row['program']);
				array_push($country_stack, $row['country']);
				//array_push($UID, $row['university_id']);

				if(strpos($last_name_stack[$lcv], "'")) {
					$last_name_stack[$lcv] = str_replace("'", "", $last_name_stack[$lcv]);
				}
				
				array_push($encoded_photos, base64_encode($photo_image[$lcv]));
				$lcv++;
			}
			while( $row = sqlsrv_fetch_array( $dpt, SQLSRV_FETCH_ASSOC ) ) {
				array_push($department_stack, $row['department_name']);
			}

			unset($_SESSION['photo_roster']['country']);
			unset($_SESSION['photo_roster']['program']);
			unset($_SESSION['photo_roster']['dept']);
			$_SESSION['photo_roster']['preferred_name'] = $preferred_name_stack;
			$_SESSION['photo_roster']['first_name'] = $first_name_stack;
			$_SESSION['photo_roster']['last_name'] = $last_name_stack;
			$_SESSION['photo_roster']['photo_image'] = $photo_image;
			$_SESSION['photo_roster']['country'] = $country_stack;
			$_SESSION['photo_roster']['program'] = $program_stack;
			$_SESSION['photo_roster']['value'] = 'all';
			$_SESSION['photo_roster']['dept'] = $department_stack;

 			global $base_url;
			$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
			$return_array = array(
				'#theme' => 'photos',
				'#first_name' => $first_name_stack,
				'#last_name' => $last_name_stack,
				'#preferred_name' => $preferred_name_stack,
				'#program' => $program_stack,
				'#photo_image' => $encoded_photos,
				'#country' => $country_stack,
				'#dept' => $department_stack,
				'#uid' => $UID,
				'#breadcrumb' => [0 => 'Home', 1 => 'Search all of Simon'],
				'#urlVal' => $breadcrumbURL,
				'#value' => 'all',
			);
			return $return_array;
		}
	}
