<?php

	namespace Drupal\photo_roster\Controller;

	use Drupal\Core\Url;
	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Drupal\Component\Utility\Tags;
	use Drupal\Component\Utility\Unicode;
	//use Drupal\Core\Form\FormStateInterface;
	use Drupal\Core\Database\Database;
	use Drupal\Core\Database\Driver\sqlsrv;

	use Symfony\Component\HttpFoundation\RedirectResponse;
	/* this form allows users to select based on
	* courses
	* This class usurps the course_search.php file
	*/
	$lcv = 3;
	//Global arrays for breadcrumbing
	$breadcrumbURL = array();
	$breadcrumb_path = array();

	class StudentCourseSelectionController extends ControllerBase{



		/**
		* {@inheritdoc}
		*/
		//public function getFormId(){
		//	return 'module_photoroster_course_search_enter_controller';
		//}

		/**
		* {@inheritdoc}
		*/
		public function init(){

			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;

			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				unset($_SESSION['photo_roster']['bc']['last_page']);
				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Course Selection'];
				$_SESSION['photo_roster']['bc']['last_page'] = \Drupal::request()->getRequestUri();
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/student_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Student Search', 2 => 'Course Selection'];
			}

			$semester_stack = array();
			$term_code_stack = array();
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$conn = sqlsrv_connect( $serverName, $connectionInfo);
			if( $conn ) {

				//Gets an individual copy of terms sorted by term code
				$query = "SELECT distinct term, term_code
				from SIS_Class_Roster
				order by term_code";

				$result = sqlsrv_query($conn, $query);

				while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
					//echo $row['program']."<br />";
					$semester = str_replace(" ", "_", $row['term']);
					array_push($semester_stack, $semester);

				}

			}else{
				 echo "Connection could not be established.<br />";
				 die( print_r( sqlsrv_errors(), true));
			}

			 \Drupal::service('page_cache_kill_switch')->trigger();


			if(!strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile')){
				if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')){
					//Firefox
					//echo 'Firefox'; die();
					$return_array = array(
						'#theme' => 'course_search',
						'#semester' => $semester_stack,
						'#breadcrumb' => $breadcrumb_path,
						'#urlVal' => $breadcrumbURL,
						'#base_path' => $_SERVER['DOCUMENT_ROOT'],
						'#browser' => $_SERVER['HTTP_USER_AGENT'],
					);
				}else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') && !strpos($_SERVER['HTTP_USER_AGENT'], 'OPR') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Edge')){
					//Chrome
					//echo 'Chrome'; die();
					$return_array = array(
						//'#theme' => 'course_search_all_browsers',
						'#theme' => 'course_search_all_browsers',
						'#semester' => $semester_stack,
						'#breadcrumb' => $breadcrumb_path,
						'#urlVal' => $breadcrumbURL,
						'#base_path' => $_SERVER['DOCUMENT_ROOT'],
						'#browser' => $_SERVER['HTTP_USER_AGENT'],
					);
				}else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Edge')){
					//Edge
					//echo 'Edge'; die();
					$return_array = array(
						'#theme' => 'course_search_NS',
					);

					/*$return_array = array(
						//'#theme' => 'course_search_all_browsers',
						'#theme' => 'course_search_all_browsers',
						'#semester' => $semester_stack,
						'#breadcrumb' => $breadcrumb_path,
						'#urlVal' => $breadcrumbURL,
						'#browser' => $_SERVER['HTTP_USER_AGENT'],
					);*/
				}else if(strpos($_SERVER['HTTP_USER_AGENT'], 'OPR')){
					//Opera
					//echo 'Opera'; die();
					/*$return_array = array(
						'#theme' => 'course_search_NS',
					);*/

					$return_array = array(
						//'#theme' => 'course_search_all_browsers',
						'#theme' => 'course_search_all_browsers',
						'#semester' => $semester_stack,
						'#breadcrumb' => $breadcrumb_path,
						'#urlVal' => $breadcrumbURL,
						'#browser' => $_SERVER['HTTP_USER_AGENT'],
					);
				}else{
					//Other
					//echo 'Some other browser'; die();
					$return_array = array(
						'#theme' => 'course_search_NS',
					);
				}
			}else{ //If the device is a mobile device
				$return_array = array(
					'#theme' => 'course_search_NS',
				);
			}

			return $return_array;
		}

		function build(){
			$prog_stack = array();
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$conn = sqlsrv_connect( $serverName, $connectionInfo);

			if( $conn ) {


				if(!isset($_POST['term'])){
 					$sel_term = '*';
 					//Gets an individual copy of terms sorted by term code
 					$query = "SELECT distinct term, term_code
 			        from SIS_Class_Roster
 			        order by term_code";
 				}
				$result = sqlsrv_query($conn, $query);

				while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
      				//echo $row['program']."<br />";
					$prog = str_replace(" ", "_", $row['term']);
					array_push($prog_stack, $prog);


				}
			}else{
				 echo "Connection could not be established.<br />";
				 die( print_r( sqlsrv_errors(), true));
			}
			return array(
				'#theme' => 'photos',
				'#attributes' => array(
					'id' => 'roster',
					'style' => 'float: left',
					'class' => array('left', 'clearfix'),
				),
				'#type' => 'responsive_image',
				'#programs' => $prog_stack,
				'#courses' => array($this->t('acc401'), $this->t('bac401'), $this->t('css423')),
				'#individual' => 'Dave Wroblewski',
				'#current_url' => \Drupal::service('path.current')->getPath(),
			);
		}


		function buildtwo(){
			return array(
				'#theme' => 'photos',
				'#attributes' => array(
					'id' => 'roster',
					'style' => 'float: left',
					'class' => array('left', 'clearfix'),
				),
				'#programs' => NULL,
				'#courses' => array($this->t('acc401'), $this->t('bac401'), $this->t('css423')),
				'#individual' => 'Sam Samuelson',
				'#cache' => ['max_age' => 0],

			);
		}

		function handleAutocomplete(Request $request) {
			$result = [];
			$prog_stack = array();
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$conn = sqlsrv_connect( $serverName, $connectionInfo);

			if( $conn ) {

				// @todo: Apply logic for generating results based on typed_string and other
				// arguments passed.
				$query = "SELECT distinct term, term_code
					from SIS_Class_Roster
					order by term_code";

				$result = sqlsrv_query($conn, $query);

				while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
					//echo $row['program']."<br />";
					$prog = str_replace(" ", "_", $row['term']);
					array_push($prog_stack, $prog);
				}
				$response = new JsonResponse($prog_stack);
				$decoded_response = json_encode($response);
			}


			return $this->init($response);
		}
	}




 ?>
