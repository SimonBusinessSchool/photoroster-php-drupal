<?php
session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}


//Connect to the database
$connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

if( $link ) {

    //Empty arrays to hold data for later access
     $prog_stack = array();
     $UID_stack = array();

    //Returns a list of current terms
    $prog_list_query = "SELECT DISTINCT sort_order, program
	    FROM photo_roster_student_view
	    ORDER BY sort_order";


    $prog_stmt = sqlsrv_query( $link, $prog_list_query );
    while( $row = sqlsrv_fetch_array( $prog_stmt, SQLSRV_FETCH_ASSOC ) ) {
        array_push($prog_stack, $row['program']);
    }


    if( $prog_stmt === false ) {
        die( print_r( sqlsrv_errors(), true));
    }

//If connection fails
} else{
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true ) );
}


$crumbs = explode("/",$_SERVER["REQUEST_URI"]);


?>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Simon School of Business Photo Roster">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Photo Roster</title>
		<link rel="stylesheet" href="/style.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">

        <script src="/static/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="/static/jquery.fullscreen-popup.min.js" type="text/javascript"></script>
        <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

        <script>
        $().ready(function() {
            $('.selection').click(function() {
                $("#program").val($(this).attr('data-program'));
                $("#year").val($(this).attr('data-year'));
                $("#jank_form").submit();
            });
        });
        </script>
    </head>
    <body>

        <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/static/simon_logo.png">
                </div>
                <div class="col-xs-9">
                    <a href="/search_selection.php"><h1>Photo Roster</h1></a>
                </div>
            </div>
        </div>
        </div>

        <div class="main-body">

            <div class="container">

	            <h4><a href="/search_selection.php" style="margin-left:25px">Home</a> / <a href="/student_search.php">Student Search</a> / Search by Program</h4>
                <!--<a class="btn btn-primary" href="/logout">Logout</a>-->
                <h3>Welcome</h3>
                <p>Please select a program to see all students enrolled within that program.  Then select a graduating year to see all enrolled students in that class of the program, or select all to see all students currently in the program.</p>
                <br />
                <br />
                <!-- Iterate through term_stack and display current terms by name -->
                <?php for($i = 0; $i < sizeof($prog_stack); $i++):?>
					<?php if($prog_stack[$i] != null){ ?>
                <div id="<?php echo str_replace(' ', '', $prog_stack[$i]);?>" role="tablist">
                    <div class="card">
                        <div class="card-header" role="tab" id="<?php str_replace(' ', '', $prog_stack[$i]);?>"-heading">
                            <a class="program" aria-expanded="true" aria-controls="<?php str_replace(' ', '', $prog_stack[$i]);?>-type" href="prog_search.php?program=<?php echo $prog_stack[$i]; ?>"><?php echo $prog_stack[$i];?></a>
                            <br /><br /><br />
                        </div>
                    </div>
                </div>

			<?php }endfor; ?>
            </div>
        </div>
		<footer class="footer container-fluid">
			<?php include("base.html");?>
		</footer>
    </body>
</html>
