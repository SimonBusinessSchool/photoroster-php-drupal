<?php

	session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}

    //Connect to the database
    $connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

    $sel_term = $_GET['term'];
    $sel_course = $_GET['course'];

if( $link ) {

    //Empty array to hold data for later access
     $section_stack = array();       //Used for holding all the sections
     
    //Queries the course section information
    $course_information_query = "SELECT distinct section_name, course_number, term_code 
        from SIS_Class_Roster
        where term_code = '".$sel_term."' and course_number = '".$sel_course."'";



    $name_stmt = sqlsrv_query( $link, $course_information_query );

    
	//Pull the course section numbers
    while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
      array_push($section_stack, $row['section_name']);
    }

    if( $name_stmt === false ) {
      die( print_r( sqlsrv_errors(), true));
    }

} else{
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true ) );
}
?>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title></title>
        <link rel="stylesheet" href="layout.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <img src="/static/simon_logo.png">
                    </div>
                    <div class="col-xs-9">
                        <a href="/roster_welcome.php"><h1>Photo Roster</h1></a>
                    </div>
                </div>
                <h4><a href="/roster_welcome.php" style="margin-left:25px">Home</a> / <a href="/conn_course.php">Term Selection</a> / <a href="/course_search.php?term=<?php echo $sel_term ?>">Course Search</a></h4>
            </div>
        </div>
        <div class="main-body">
            <div class="container">
                <br /><br />
                <!--Make sure the form has the autocomplete function switched off:-->
                <h3><p><?php echo 'Available sections for '.$sel_course ?></p></h3>
                <div style="margin-left: 20px;">
                <ul>
                    <?php 

                        for($i = 0; $i < sizeof($section_stack); $i++){
                            echo "<li><a href=\"/roster.php?term=".$sel_term."&course=".$sel_course."&section=".$section_stack[$i]."\">Section ".$section_stack[$i]."</a></li>";
                            //echo $section_stack[$i]."<br />";
                        }

                    ?>
                </ul>
                </div>
            </div>
        </div>
		<footer class="footer-absolute container-fluid">
			<?php include("base.html");?>
		</footer>
    </body>
</html>