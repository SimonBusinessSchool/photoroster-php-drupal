<?php

session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}


    //Connect to the database
   $connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

    $selected_program = $_GET['program'];
    if( $link ) {

        //Empty arrays to hold data for later access
         $grad_year_stack = array();         //Used for storing the programs
         $UID_stack = array();             //Used to store university id's

        //Queries the course section information
        $section_information_query = "SELECT DISTINCT grad_year from SIS_student_roster
            WHERE program = '".$_GET['program']."'
            ORDER BY grad_year DESC";

        $prog_stmt = sqlsrv_query( $link, $section_information_query );

        //Pull alpha from query (unique index)
        while( $row = sqlsrv_fetch_array( $prog_stmt, SQLSRV_FETCH_ASSOC ) ) {
          array_push($grad_year_stack, $row['grad_year']);
          //array_push($UID_stack, $row['university_id']);
        }

    } else{
         echo "Connection could not be established.<br />";
         die( print_r( sqlsrv_errors(), true ) );
    }



?>

<!DOCTYPE html>
<html lang="en-US">
    <head>
		<link rel="stylesheet" href="/style.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">
        <title></title>
    </head>
    <body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/static/simon_logo.png">
                </div>
                <div class="col-xs-9">
                    <a href="/search_selection.php"><h1>Photo Roster</h1></a>
                </div>
            </div>
        </div>
    </div>
    <div class="main-body">
    <div class="container">
	<h4><a href="/search_selection.php" style="margin-left:25px">Home</a> / <a href="/student_search.php">Student Search</a> / <a href="/conn_prog.php">Search by Program</a> / Year</h4>
    <br /><br />
        <ul>
        <!-- Iterate through term_stack and display current terms by name -->
        <?php
        for($i = 0; $i < sizeof($grad_year_stack); $i++): ?>
            <li>
                <h3><a href="roster.php?program=<?php echo $selected_program ?>&year=<?php echo $grad_year_stack[$i] ?>"><?php echo $grad_year_stack[$i] ?></a></h3>
            </li>
            <br>
            <?php   ?>
            <?php endfor; ?>
        </ul>
    </div>
    </div>
   <footer class="footer container-fluid">
    <?php include("base.html");?>
	</footer>

    </body>
</html>
