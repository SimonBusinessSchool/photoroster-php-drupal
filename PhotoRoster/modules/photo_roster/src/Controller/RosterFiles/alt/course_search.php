<?php

	session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}
    $string_array = array();

    if(isset($_SESSION['str_arr']) && isset($_SESSION['str_len'])){
    $string_array = $_SESSION['str_arr'];
        for($i = 0; $i < $_SESSION['str_len']; $i++){
            //echo $string_array[$i];
        }
    } 
	
//Connect to the database
    $connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );


    $sel_term = $_GET['term'];


if( $link ) {

    //Empty arrays to hold data for later access
     $alpha_stack = array();         //Used for the alpha index (non-repeating values)
     $alpha_course_stack = array();  //Used for comparing the alpha index to the alpha of the individual courses (repeating values)
     $course_stack = array();        //Used for the individual courses
     $section_stack = array();       //Used for holding all the sections
	 $course_stack_compare = array();
     
    //Queries the unique alpha index identifier
    $course_list_query = "select distinct
    LEFT(course_number,1) AS alpha --,
    --course_number
        from SIS_Class_Roster
    where term_code =".  $_GET['term'] . "
        order by 1";
	//Queries for the distinct courses under the specified alpha
    $ind_course_list_query = "select distinct course_number, section_name,
		LEFT(course_number,1) AS alpha --,
        from SIS_Class_Roster
		order by alpha, section_name";
     
    //Queries the course section information
    $course_information_query = "SELECT distinct section_name, course_number 
        from SIS_Class_Roster
        ORDER BY course_number";


    //Queries the course section information
    $section_information_query = "select distinct course_number,
		LEFT(course_number,6) AS alpha
		from SIS_Class_Roster
		order by alpha";


    $ind_query_stmt = sqlsrv_query( $link, $ind_course_list_query );
    $course_stmt = sqlsrv_query( $link, $course_list_query );
    $name_stmt = sqlsrv_query( $link, $course_information_query );
    $section_stmt = sqlsrv_query( $link, $section_information_query);

    //Pull alpha from query (unique index)
    while( $row = sqlsrv_fetch_array( $course_stmt, SQLSRV_FETCH_ASSOC ) ) {
      array_push($alpha_stack, $row['alpha']);
    }

   
	while( $row = sqlsrv_fetch_array( $ind_query_stmt, SQLSRV_FETCH_ASSOC ) ) {
		
      array_push($alpha_course_stack, $row['alpha']);
      array_push($course_stack, $row['course_number']);
    }
    
	//Pull the course section numbers
    while( $row = sqlsrv_fetch_array( $name_stmt, SQLSRV_FETCH_ASSOC ) ) {
      array_push($section_stack, $row['section_name']);
    }

	//Pull the course section numbers
    while( $row = sqlsrv_fetch_array( $section_stmt, SQLSRV_FETCH_ASSOC ) ) {
	  array_push($course_stack_compare, $row['course_number']);
    }

    if( $course_stmt === false ) {
      die( print_r( sqlsrv_errors(), true));
    }

} else{
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true ) );
}
    $response_text = '';


     


    
?>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title></title>
        <link rel="stylesheet" href="layout.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">
        <script src="/static/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script>
            function showHint(str) {
                if (str.length == 0) {
                    document.getElementById("search").innerHTML = "";
                    return;
                } else {
                    var xmlhttp = new XMLHttpRequest();
                    var term = <?php echo $sel_term ?>;
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            document.getElementById("search").innerHTML = this.responseText;
                            var urlParams = new URLSearchParams(location.search);
                        }
                    };
                   
                    xmlhttp.open("GET", "get_info.php?term=" + term +"&q=" + str, true);
                    xmlhttp.send();
                }
                 console.log(<?php echo sizeof($string_array); ?>);
            }
           

            function getUrlParameter(name) {
                name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
                var results = regex.exec(location.search);
                return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
            };

            window.onload = function() {
              document.getElementById("input_box").focus();
            };
        </script>


    
    <body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/static/simon_logo.png">
                </div>
                <div class="col-xs-9">
                    <a href="/roster_welcome.php"><h1>Photo Roster</h1></a>
                </div>
            </div>
            <h4><a href="/roster_welcome.php" style="margin-left:25px">Home</a> / <a href="/conn_course.php">Course Search</a></h4>
        </div>
    </div>
     <div class="main-body">

        <div class="container">
        <br /><br />
            <form >
			    <label for="username">Start typing a name in the input field below:</label>
			    <input type="text" name="username" id="input_box" onkeyup="showHint(this.value)" autocomplete="off">
            </form>
            <?php
            
                if(sizeof($string_array) > 0){
                $q = 0;
                    for($i = 0; $i < sizeof($string_array); $i++){
                        //echo $string_array[$i]."<br />";
                        //echo "<a href=\"/roster.php?term=".$sel_term."&course=".$string_array[$i]."&section=*\">".$string_array[$i]."</a><br />";
                        echo 'String array at i='.$i.' Value='.$string_array[$i];
                        
                        
                        ?>
                        
                            <button class="accordion" name="username"><?php echo $string_array[$i] ?></button><br />
                            <div class="panel" style="display: block;">
                                <div><p><?php
                                    for($j = 0; $j < sizeof($course_stack); $j++){
                                        if($course_stack[$j] == $course_stack_compare[$i]){


											?> <a href ="roster.php?term=<?php echo $sel_term ?>&course=<?php 
                                                echo $course_stack[$j]?>&section=<?php echo $section_stack[$q] ?>">
                                                <?php echo 'Section '.$section_stack[$q] ?></p></a><br><?php 
                                            $q++;
										}
									}
                              ?></p></div>
                            </div>
                            <?php
                            

                            
                    }
                    
                            //unset($_SESSION['str_len']);
                }else{
                    echo 'Check your string array length '.sizeof($string_array);
                }
                    //destroy_session();
                    unset($_SESSION['str_arr']);
                    unset($_SESSION['str_len']);
            ?>

            <p>Suggestions:</p><p id="search"></p>
            <!--<p id="search"></p>-->
            <!-- Iterate through term_stack and display current terms -->
            <h3><p>Select the choice corresponding to the beginning of the course name</p>
				<p>Then select the course and then section number you wish to view:</p></h3>
            
        </div>
    </div>
   
    
    
	<script>
            var acc = document.getElementsByClassName("accordion");
			var i;
			for (i = 0; i < acc.length; i++) {
			  acc[i].addEventListener("click", function(e) {
				/* Toggle between adding and removing the "active" class,
				to highlight the button that controls the panel */
				this.classList.toggle("active");
				console.log(e.target);
				/* Toggle between hiding and showing the active panel */
				var panel = this.nextElementSibling;
				if (panel.style.display === "block") {
				  panel.style.display = "none";
				} else {
				  panel.style.display = "block";
				}
			  });
			}
        </script>
		<footer class="footer container-fluid">
			<?php include("base.html");?>
		</footer>
    </body>
</html>