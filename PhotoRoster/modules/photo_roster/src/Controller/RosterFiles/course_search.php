<?php

	session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}



    $response = "";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $response .= "I have made it to this point";
    }

    //Connect to the database
    $connectionInfo = array( "UID" => "boomi", "PWD" => "D3ll_B00m1!#((+", "Database" => "Photo_Roster" );
    $link = sqlsrv_connect( "its-simnsql-wp1.ur.rochester.edu", $connectionInfo );

if( $link ) {

		//Empty arrays to hold data for later access
		$term_stack = array();  					//Used for comparing the alpha index to the alpha of the individual courses (repeating values)
		$term_code_stack = array();			//Used to hold the term code to pass
		$course_stack = array();        	//Used for the individual courses
		$section_stack = array();       	//Used for holding all the sections
		$course_stack_compare = array();	//Only holds one copy of each course, used for section comparisons later

		$term_compare = array();
		$term_code_compare = array();
		//Initial run
		if(!isset($_POST['term'])){
			$sel_term = '*';
			//Gets an individual copy of terms sorted by term code
			$universal_query = "SELECT distinct term, term_code
	        from SIS_Class_Roster
	        order by term_code";
		}

		//After first submission and term isset
		if(isset($_POST['term'])){
			echo 'POST IS set in course_search';
    	$sel_term = $_POST['term'];
			//Query for the individual course information based upon the selected term
			$universal_query = "SELECT distinct course_number,
					LEFT(course_number,6) AS alpha
					from SIS_Class_Roster
					WHERE term_code= '".$sel_term."'
					order by alpha";
		}

		//After second submission and term and corse are both set
		if(isset($_POST['term']) && isset($_POST['course'])){
    	$sel_term = $_POST['term'];
			$sel_course = $_POST['course'];
			$universal_query = "SELECT * from SIS_Class_Roster
					WHERE term_code = '".$sel_term."' and course_number = '".$sel_course."'";
		}

    //Returns each individual section for each course (one to many)
    $course_information_query = "SELECT distinct section_name, course_number
        from SIS_Class_Roster
        ORDER BY course_number";


    //Gets each individual course available. This will feed solely into $course_stack_compare
		//so that we can check against each course once to populate every section availabe to that course
    $section_information_query = "SELECT distinct course_number, term_code,
		LEFT(course_number,6) AS alpha
		from SIS_Class_Roster
		WHERE term_code = '".$sel_term."'
		order by alpha";



	$term_information_query = "SELECT distinct term, term_code
		from SIS_Class_Roster";

	$universal_query_stmt = sqlsrv_query( $link, $universal_query );
	//$ind_query_stmt = sqlsrv_query( $link, $ind_course_list_query );
	$term_stmt = sqlsrv_query( $link, $term_information_query );
	$section_stmt = sqlsrv_query( $link, $section_information_query);


	while( $row = sqlsrv_fetch_array( $universal_query_stmt, SQLSRV_FETCH_ASSOC ) ) {
      		array_push($term_stack, $row['term']);
	  	  	array_push($term_code_stack, $row['term_code']);
			if(isset($_POST['term']))
      			array_push($course_stack, $row['course_number']);
			if(isset($_POST['term']) && isset($_POST['course']))
				array_push($course_stack, $row['section_name']);
    }

	//Pull the course section numbers
    while( $row = sqlsrv_fetch_array( $section_stmt, SQLSRV_FETCH_ASSOC ) ) {
	  array_push($course_stack_compare, $row['course_number']);
    }

	while( $row = sqlsrv_fetch_array( $term_stmt, SQLSRV_FETCH_ASSOC ) ) {
	  array_push($term_compare, $row['term']);
	  array_push($term_code_compare, $row['term_code']);
    }

    if( $universal_query_stmt === false ) {
      die( print_r( sqlsrv_errors(), true));
    }

} else{
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true ) );
}

?>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title></title>
        <link rel="stylesheet" href="/layout.css">
		<link rel="stylesheet" href="/style.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">
        <script src="/static/jquery-2.1.4.min.js" type="text/javascript"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- #region js -->
        <script>
            function autocomplete(inp, arr) {
                /*the autocomplete function takes two arguments,
                the text field element and an array of possible autocompleted values:*/
                var currentFocus;
                /*execute a function when someone writes in the text field:*/
                inp.addEventListener("input", function(e) {
                    var a, b, i, val = this.value;
                    /*close any already open lists of autocompleted values*/
                    closeAllLists();
                    if (!val) { return false;}
                    currentFocus = -1;
                    /*create a DIV element that will contain the items (values):*/
                    a = document.createElement("DIV");
                    a.setAttribute("id", this.id + "autocomplete-list");
                    a.setAttribute("class", "autocomplete-items");
                    /*append the DIV element as a child of the autocomplete container:*/
                    this.parentNode.appendChild(a);
                    /*for each item in the array...*/
                    for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                            b.addEventListener("click", function(e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                            (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                    }
                });
                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function(e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x) x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                    } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                    } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                    }
                });
                function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
                }
                function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
                }
                function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
            }


            function getUrlParameter(name) {
                name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
                var results = regex.exec(location.search);
                return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
            };
             var countries = [];
            window.onload = function() {
              document.getElementById("term_list").focus();
            };
        </script>
        <!-- #endregion -->



    <body>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/static/simon_logo.png">
                </div>
                <div class="col-xs-9">
                    <a href="/search_selection.php"><h1>Photo Roster</h1></a>
                </div>
            </div>
        </div>
    </div>
     <div class="main-body">
        <div class="container" id="untabable">
		<h4><a href="/search_selection.php" style="margin-left:25px">Home</a> / <a href="/student_search.php">Student Search</a> / Search by Course</h4>
            <br /><br />
            <h2><p>Either start by typing a term name</p><p>or click in the term box for a dropdown:</p></h2>
			<h4>Hit F5 or refresh your browser to clear form<br />
				Or press the "Refresh Page" button below</h4>
            <!--Make sure the form has the autocomplete function switched off:-->
            <table style="width: 100%" style="border-spacing: 125px; border-collapse: seperate;">
				</tr>
					<th>
						<button id="refreshButton">Refresh Page</button>
						<br /><br />
					</th>
				<tr>
                <tr>
                    <form method="post">
                        <div class="autocomplete">
                            <th>
								<br />
								<p>Start by selecting the semester of your course:</p>
								<br />
								<input id="hidden_output" type="hidden" />
                                <input id="input_term" type="text" name="term" list="term_list" placeholder="Term" autofocus="true"/>
                                <datalist id="term_list">
                                <?php
                                for($i = 0; $i < sizeof($term_stack); $i++){
                                    echo "<option id=".$term_code_stack[$i]." name=".str_replace(' ', '%20', $term_stack[$i])." class=".$term_code_stack[$i]." value=".$term_code_stack[$i].">".$term_stack[$i]."</option>";
                                }
                                ?>
                                </datalist>
								<br /><br /><br />
								<p>Then select Your course number:</p>
								<br />
                            </th>
                        </div>
                    </form>
                </tr>
                <tr>
                    <form method="post">
                        <div class="autocomplete">
                            <th>
                                <!--<input type="hidden" id="input_term" type="text" name="term"  list="course_list" value="<?php echo $sel_term ?>" />-->
                                <input id="input_course" type="text" name="course" list="course_list" placeholder="Course Number" disabled="true"/>
                                <datalist id="course_list">
                                </datalist>
								<br /><br /><br />
								<p>And then the section number:</p>
								<br />
                            </th>
                        </div>
                    </form>
                </tr>
								<br />
                <tr>
                    <form method="post">
                        <div class="autocomplete">
                            <th>
                                <input id="input_section" type="text" name="section" list="section_list" placeholder="Section Number" disabled="true"/>
                                <datalist id="section_list">
                                </datalist>
                            </th>
                        </div>
                    </form>
            </table>
        </div>
    </div>
	<script>

		//function callFunction(){
			document.getElementById("untabable").onkeydown = function(e){
				if(e.keyCode == 9){
					e.preventDefault();
				}
			};

		//}
	</script>
	<script type="text/javascript">
			document.getElementById("refreshButton").onclick = function() {location.reload();};
			document.getElementById("input_term").onselect = function() {refreshPage("input_term")};
			document.getElementById("input_course").onselect = function() {refreshPage("input_course")};
			document.getElementById("input_section").onselect = function() {refreshPage("input_section")};
			//var term_passed;
			//var course_passed;
			var section_passed;
			function refreshPage(input_type){
				if(input_type == "input_term"){
						var course_array = new Array();
						document.getElementById("input_term").onchange = function(e) {
						var sel_term = document.getElementById(e.target.value).value;
						term_passed = sel_term;
						$.post("jquery_post.php", //Required URL of the page on server
						{ // Data Sending With Request To Server
						term:sel_term,
						val:0
						},
						function(response,status){ // Required Callback Function
						  	var course = document.getElementById("course_list");
							var options = new Array();
							var indivOptions = new Array();
							options = response.split(',');
						  	for(i = 0; i < options.length; i++){
								indivOptions[i] = options[i].replace(/[[\\'"]+/g, '');
								var opt = indivOptions[i].trim();
							    var el = document.createElement("option");
								el.setAttribute("id", opt);
								if(i == options.length - 1){
									opt = options[i].replace(']', '');
									opt = opt.replaceAll('"', '');
								}
								el.textContent = opt;
							    el.value = opt;
							    course.appendChild(el);
							}
							var arr_length = <?php echo sizeof($term_code_compare) ?>;
							var inc;
							//console.log(arr_length);
							$.post("term_compare.php", {selection:sel_term}, function(response, nnStatus){
								//console.log(response);
								document.getElementById('hidden_output').type = "text";
								document.getElementById("input_term").type= "hidden";
								document.getElementById('hidden_output').value = response;
							});
						});
					}

					document.getElementById("input_course").disabled=false;
					document.getElementById("input_course").focus();
					document.getElementById("input_term").disabled=true;

				}
				if(input_type == "input_course"){
					var section_array = new Array();

					document.getElementById("input_course").onchange = function(e) {
						var sel_course = document.getElementById(e.target.value).value;
						course_passed = sel_course;
						//console.log(sel_course + "was selected");
						$.post("jquery_post_section.php",
						{
						term:term_passed,
						course:sel_course,
						val:1
						},
						function(response,status){
							var section = document.getElementById("section_list");
							var options = new Array();
							var indivOptions = new Array();
							options = response.split(',');
							for(i = 0; i < options.length; i++){
								indivOptions[i] = options[i].replace(/[[\\'"]+/g, '');
								var opt = indivOptions[i];
								if(i == options.length - 1){
									opt = opt.replace(']', '');
								}
								var el = document.createElement("option");
								el.textContent = opt;
								el.value = opt;
								section.appendChild(el);
							}
						});
					}
					document.getElementById("input_section").disabled=false;
					document.getElementById("input_section").focus();
					document.getElementById("input_course").disabled=true;
				}
				if(input_type == "input_section"){
					document.getElementById("input_section").onchange = function(e) {
						section_passed = e.target.value;
						console.log("I am going to input the search results, calculating ... " + section_passed);
					}
					window.location = "/roster.php?term="+document.getElementById("input_term").value+"&course="+document.getElementById("input_course").value+"&section="+document.getElementById("input_section").value;
				}
			}





	</script>

         <script>
            autocomplete(document.getElementById("input_term"), countries);
			autocomplete(document.getElementById("input_course"), countries);
        </script>
		<footer class="footer container-fluid">
			<?php include("base.html");?>
		</footer>
    </body>
</html>
