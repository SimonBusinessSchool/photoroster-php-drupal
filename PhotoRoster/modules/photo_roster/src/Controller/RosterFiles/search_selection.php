<?php
	session_start();
	$token = $_SESSION['user_id'];
	if (!isset( $_SESSION['user_id'])) {
		// Redirect them to the login page
		header("Location: /login.php");
	}


?>

<html>
	<head>
		<meta charset="utf-8">
        <meta name="description" content="Simon School of Business Photo Roster">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Photo Roster</title>

		<link rel="stylesheet" href="/style.css">
        <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
        <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">

        <script src="/static/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="/static/jquery.fullscreen-popup.min.js" type="text/javascript"></script>
        <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-xs-3">
					<img src="/static/simon_logo.png">
				</div>
				<div class="col-xs-9">
					<a href="/search_selection.php"><h1>Photo Roster</h1></a>
				</div>
			</div>
		</div>
		</div>

		<div class="main-body">

            <div class="container">
				<h4><p style="margin-left:25px">Home</h4>

				<h2>Please make a selection for what you are searching for:</h2>

				<h3><a href="/student_search.php">Students</a></h3>
				<br />
				<h3><a href="/conn_staff.php">Staff</a></h3>
				<br />
				<h3><a href="/conn_faculty.php">Faculty</a></h3>
				<br />
				<h3><a href="/roster.php?value=all">Simon All</a></h3>
				<br />

			</div>
		</div>

		<footer class="footer container-fluid">
			<?php include("base.html");?>
		</footer>
	</body>
</html>
