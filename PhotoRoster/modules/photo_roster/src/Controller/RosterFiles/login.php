



<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Simon School of Business Photo Roster">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Photo Roster</title>

    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/bootstrap/css/simon-bootstrap.css">
    <link rel="stylesheet" media="only screen and (max-width:576px)" href="/static/bootstrap/css/simon-bootstrap-mobile.css">

    <script src="/static/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="/static/jquery.fullscreen-popup.min.js" type="text/javascript"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body style="overflow: visible">
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <img src="/static/simon_logo.png">
                </div>
                <div class="col-xs-9">
                    <a href="/roster_welcome.html"><h1>Photo Roster</h1></a>
                </div>
            </div>
        </div>
    </div>
	<br />
	
    <div class="main-body">
        <div class="container">
			<h3><p>Login</p></h3>
			<p>Please login using your UR AD credentials (email credentials)</p>
			<br/>
            <form action="/verification.php" method="post">

				<div class="form-group">
					<label for="username">Username</label>
					<div class="row">
						<div class="col-md-3">
							<input type="text" class="form-control" name="username" placeholder="Enter your username" required>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<div class="row">
						<div class="col-md-3">
							<input type="password" class="form-control" placeholder="Enter your password" name="password" required>
						</div>
					</div>
				</div>
                <input type="submit" value="Login" class="btn btn-primary">
				<br />
				<?php
				session_start();
				if(isset($_SESSION['error_message'])){
					echo '<h4><span style="color:#FF0000;text-align:center;">'.$_SESSION['error_message'].'</span></h4>';
					unset($_SESSION['error_message']);
				}
				?>
            </form>
        </div>
    </div>
	<br />
	<br />
	<br />
	<footer class="footer container-fluid">
	<?php include("base.html");?>
	</footer>
</body>
</html>
