<?php

	namespace Drupal\photo_roster\Controller;

	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\HttpFoundation\RedirectResponse;

	//Global arrays for breadcrumbing
	$breadcrumbURL = array();
	$breadcrumb_path = array();

	class StudentProgramSelectionController extends ControllerBase{


		public function programSearch() {
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}

			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;
			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				//unset($_SESSION['photo_roster']['bc']['last_page']);
				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Program Selection'];
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/student_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Student Search', 2 => 'Program Selection'];
			}

			$prog_stack = array();
			$url_stack = array();
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$conn = sqlsrv_connect( $serverName, $connectionInfo);
			global $base_url;
			if( $conn ) {
				$query = "SELECT DISTINCT sort_order, program
			 	   FROM photo_roster_student_view
			 	   ORDER BY sort_order";

				$result = sqlsrv_query($conn, $query);
				while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
      				//echo $row['program']."<br />";
					$prog = str_replace(" ", "_", $row['program']);
					array_push($prog_stack, $row['program']);
					array_push($url_stack, $base_url.'/photoroster/prog_search?program='.$row['program']);
				}
			}else{
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}

			 \Drupal::service('page_cache_kill_switch')->trigger();
			$build =[
				'#theme' => 'program-search',
				'#title' => $prog_stack,
				'#url' => $url_stack,
				'#breadcrumb' => $breadcrumb_path,
				'#urlVal' => $breadcrumbURL,
			];
			return $build;
		}

		public function yearSearch() {
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;

			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/conn_prog'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Program Selection', 2 => 'Graduating Year'];
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/student_search', 2 => $base_url.'/photoroster/conn_prog'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Student Search', 2 => 'Program Selection', 3 => 'Graduating Year'];
			}

			$myform = \Drupal::formBuilder()->getForm('Drupal\photo_roster\Form\ProgramSearchInputForm');
			$_SESSION['photo_roster']['bc']['last'] = \Drupal::request()->getRequestUri();
 			\Drupal::service('page_cache_kill_switch')->trigger();
			$build = [
			'#theme' => 'form-theme',
			'#pageTitle' => "Select Graduating Year",
			'#form' => $myform,
			'#breadcrumb' => $breadcrumb_path,
			'#urlVal' => $breadcrumbURL,
			];

			return $build;
		}


		public function studentSearch() {
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;

			unset($_SESSION['photo_roster']['bc']['last_page']);
			$_SESSION['photo_roster']['bc']['last_page'] = '/photoroster/student_search';
			 \Drupal::service('page_cache_kill_switch')->trigger();
			$build = [
				'#theme' => 'student-search',
				'#pageTitle' => 'Search students',
				'#breadcrumb' => [0 => 'Home'],
				'#urlVal' => [0 => $base_url.'/photoroster/home'],
			];

			return $build;
		}

		public function staffSearch() {
			if (!isset($_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}

			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;

			unset($_SESSION['photo_roster']['bc']['last_page']);
			$_SESSION['photo_roster']['bc']['last_page'] = \Drupal::request()->getRequestUri();
			 \Drupal::service('page_cache_kill_switch')->trigger();
			$build = [
				'#theme' => 'staff-search',
				'#pageTitle' => 'Search Simon Staff',
				'#value' => 'staff',
				'#breadcrumb' => [0 => 'Home', 1 => 'Staff Search'],
				'#urlVal' => [0 => $base_url.'/photoroster/home'],
			];

			return $build;
		}

		public function facultySearch() {
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}

			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;

			unset($_SESSION['photo_roster']['bc']['last_page']);
			$_SESSION['photo_roster']['bc']['last_page'] = \Drupal::request()->getRequestUri();
			 \Drupal::service('page_cache_kill_switch')->trigger();
			$build = [
				'#theme' => 'staff-search',
				'#pageTitle' => 'Search Simon Faculty',
				'#value' => 'faculty',
				'#breadcrumb' => [0 => 'Home', 1 => 'Faculty Search'],
				'#urlVal' => [0 => $base_url.'/photoroster/home'],
			];

			return $build;
		}

		public function indivStuSearch() {
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;

			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				unset($_SESSION['photo_roster']['bc']['last_page']);
				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Individual Student Search'];
				//$_SESSION['photo_roster']['bc']['last_page'] = \Drupal::request()->getRequestUri();
			}else{
				//unset($_SESSION['photo_roster']['bc']['last_page']);
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/student_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Student Search', 2 => 'Individual Student Search'];
			}

			$myform = \Drupal::formBuilder()->getForm('Drupal\photo_roster\Form\StudentIndividualSearchForm');
			// If you want modify the form:
			$myform['field']['#value'] = 'From my controller';
			 \Drupal::service('page_cache_kill_switch')->trigger();
			$build = [
			'#theme' => 'student-indiv-search',
			'#pageTitle' => 'Search Individual Students',
			'#form' => $myform,
			'#breadcrumb' => $breadcrumb_path,
			'#urlVal' => $breadcrumbURL,
			];

			return $build;
		}

		public function indivStaffSearch() {
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;

			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				unset($_SESSION['photo_roster']['bc']['last_page']);
				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Individual Staff Search'];
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/staff_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Staff Search', 2 => 'Individual Staff Search'];
			}

			$myform = \Drupal::formBuilder()->getForm('Drupal\photo_roster\Form\StaffIndividualSearchForm');
			// If you want modify the form:
			$myform['field']['#value'] = 'From my controller';
			 \Drupal::service('page_cache_kill_switch')->trigger();
			$build = [
			'#theme' => 'staff-indiv-search',
			'#pageTitle' => 'Search Individual Staff',
			'#form' => $myform,
			'#breadcrumb' => $breadcrumb_path,
			'#urlVal' => $breadcrumbURL,
			'#value' => 'staff',
			];

			return $build;
		}

		public function indivFacultySearch() {

			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}

			global $base_url;
			global $breadcrumbURL;
			global $breadcrumb_path;

			if($_SESSION['photo_roster']['bc']['last_page'] == '/photoroster/home'){
				unset($_SESSION['photo_roster']['bc']['last_page']);
				$breadcrumbURL = [0 => $base_url.'/photoroster/home'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Individual Faculty Search'];
			}else{
				$breadcrumbURL = [0 => $base_url.'/photoroster/home', 1 => $base_url.'/photoroster/faculty_search'];
				$breadcrumb_path = [0 => 'Home', 1 => 'Faculty Search', 2 => 'Individual Faculty Search'];
			}

			$myform = \Drupal::formBuilder()->getForm('Drupal\photo_roster\Form\FacultyIndividualSearchForm');
			// If you want modify the form:
			$myform['field']['#value'] = 'From my controller';
			 \Drupal::service('page_cache_kill_switch')->trigger();
			$build = [
			'#theme' => 'staff-indiv-search',
			'#pageTitle' => 'Search Individual Faculty',
			'#form' => $myform,
			'#breadcrumb' => $breadcrumb_path,
			'#urlVal' => $breadcrumbURL,
			'#value' => 'faculty',
			];

			return $build;
		}

		public function admin() {
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			\Drupal::service('page_cache_kill_switch')->trigger();
			$build = [
				'#theme' => 'admin',
			];

			return $build;
		}
	}

?>
