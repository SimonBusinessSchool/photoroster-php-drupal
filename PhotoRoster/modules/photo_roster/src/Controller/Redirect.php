<?php

	namespace Drupal\photo_roster\Controller;
	use Drupal\Core\Controller\ControllerBase;

	class Redirect extends ControllerBase{
		function __construct(){

		}

		public function content(){
			return array('#type'=>'markup', '#markup'=>t('This is my redirect link'), );
		}

		function init(){
			header("Location: /RosterFiles/login.php");
		}
	}



?>
