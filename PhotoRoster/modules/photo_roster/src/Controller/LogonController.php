<?php

	namespace Drupal\photo_roster\Controller;

	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\HttpFoundation\RedirectResponse;

	class LogonController extends ControllerBase{

		function init(){
			session_start();
			if (isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.homepage'));
			}

			//echo "Correct attempts " + $_SESSION['photo_roster']['attempts']; die();
			$myform = \Drupal::formBuilder()->getForm('Drupal\photo_roster\Form\PhotoRosterForm');
			if($_SESSION['photo_roster']['attempts'] > 0){
				$return_array = array(
					'#theme' => 'logon',
					'#form' => $myform,
					'#error' => 'You have entered invalid login credentials',
				);
			}else{
				$return_array = array(
					'#theme' => 'logon',
					'#form' => $myform,
					'#error' => '',
				);
			}

			return $return_array;
		}



	}
