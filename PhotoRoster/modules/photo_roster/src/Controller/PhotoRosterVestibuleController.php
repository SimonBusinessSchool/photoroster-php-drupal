<?php

	namespace Drupal\photo_roster\Controller;

	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\HttpFoundation\RedirectResponse;
	/* this form allows users to select based on
	* courses
	* This class usurps the course_search.php file
	*/
	class PhotoRosterVestibuleController extends ControllerBase{



		function init(){

			session_start();
			global $base_url;
			header($base_url.\Drupal::request()->getRequestUri());
			if (!isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.login'));
			}
			unset($_SESSION['photo_roster']['bc']['last_page']);
			$_SESSION['photo_roster']['bc']['last_page'] = \Drupal::request()->getRequestUri();
			//========================================================

			$return_array = array(
				'#theme' => 'home',
				'#base_path' => $_SERVER['DOCUMENT_ROOT'],
			);



			return $return_array;

		}

	}


 ?>
