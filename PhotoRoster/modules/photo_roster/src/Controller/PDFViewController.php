<?php


namespace Drupal\photo_roster\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PDFViewController extends ControllerBase{

	function init(){
		session_start();
		\Drupal::service('page_cache_kill_switch')->trigger();
		if (!isset( $_SESSION['photo_roster']['user_id'])) {
			//Redirect them to the login page
			return new RedirectResponse(\Drupal::url('photo_roster.login'));
		}
		
		$token	= $_SESSION['photo_roster']['user_id'];
		$path = $_SERVER['DOCUMENT_ROOT'];
		mkdir($path.'/sites/default/files/files/'.$token.'/');
		$TEMP_LINK_LOCATION = $path.'/sites/default/files/files/'.$token.'/';

	    //need to get all session variable arrays
	    $preferred_name = $_SESSION['photo_roster']['preferred_name'];
	    $first_name = $_SESSION['photo_roster']['first_name'];
	    $last_name = $_SESSION['photo_roster']['last_name'];
	    $country = $_SESSION['photo_roster']['country'];
	    $program = $_SESSION['photo_roster']['program'];
	    $photo = $_SESSION['photo_roster']['photo_image'];
		$sel_prog = $_SESSION['photo_roster']['selprog'];
		$year = $_SESSION['photo_roster']['year'];
		$department = $_SESSION['photo_roster']['dept'];
		


	    require($path.'/sites/default/files/fpdf.php');
		$image = array();
	    $image_height = 29;//30;
	    $image_width = 21;//25;
	    $pdf = new FPDF();
	    $pdf->AddPage();
	    $pdf->SetY(10);
	    $pdf->SetTitle("Download Images");
		//$pdf->SetLeftMargin(10);
		//$pdf->SetRightMargin(10);
	    $pdf->SetMargins(15,-30,10);
		$pdf->SetAutoPageBreak(false,0);
		$this->header($pdf, $sel_prog, $year);
		$pdf->SetFont("Arial", "B", 6);
		
		# Get image from first row and save it again on disk.
		$image = $_SESSION['photo_roster']['photo_image'];

		for($i = 0; $i < sizeof($last_name); $i++){
			if(strpos($first_name[$i], "'") == TRUE){
				$first_name[$i] = str_replace("'", "", $first_name[$i]);
			}
			if(strpos($last_name[$i], "'") == TRUE){
				$last_name[$i] = str_replace("'", "", $last_name[$i]);
			}
		}

		for($i = 0; $i < sizeof($image); $i++){
			if(basename($image[$i]) != null && substr(bin2hex($image[$i]), 0, 2) != 30){
				file_put_contents($TEMP_LINK_LOCATION.'/'.'image_'.$i.".jpeg", $image[$i], FILE_USE_INCLUDE_PATH);
			}else if(basename($image[$i]) != null && substr(bin2hex($image[$i]), 0, 2) == 30){
				$im = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT']."/sites/default/files/rochester.jpeg");
				imagejpeg($im, $TEMP_LINK_LOCATION.'/'.'image_'.$i.".jpeg", 75);
			}
		}

	    //Iterate throught the photos to draw them to PDF
	    $i = 0;
	    for($rows = 0; $rows < sizeof($last_name); $rows++){
	        $start_y = $pdf->GetY();
	        for($cols = 0; $cols < 6; $cols++){
	        $start_x = $pdf->GetX();
	            if($i < sizeof($last_name)){

	                if($photo[$i] != NULL){
	                    //Print image to PDF
						try {
							$pdf->Image($TEMP_LINK_LOCATION."image_".$i.".jpeg", $pdf->GetX(), $pdf->GetY(), $image_width, $image_height, "JPEG");
						} catch (\Exception $e) {
							$pdf->Image($path.'/sites/default/files/rochester.png', $pdf->GetX(), $pdf->GetY(), $image_width, $image_height, "PNG");
						}
	                }else{
	                    //Print default NULL value image to PDF
	                    $pdf->Image($path.'/sites/default/files/rochester.png', $pdf->GetX(), $pdf->GetY(), $image_width, $image_height, "PNG");
	                }
	                $pdf->SetX($start_x);

	                if($preferred_name[$i] != NULL){
						if(strlen($preferred_name[$i]) > 8){
							//$preferred_name[$i] = substr($preferred_name[$i], 0, 8);
						}
	                    $pdf->Write(($image_height*2) + 5, $preferred_name[$i].' '.$last_name[$i]);
	                    $pdf->SetX($start_x);
	                }else{
						if(strlen($first_name[$i]) > 8){
							//$first_name[$i] = substr($first_name[$i], 0, 8);
						}if(strlen($last_name[$i]) > 8){
							//$last_name[$i] = substr($last_name[$i], 0, 8);
						}
	                    $pdf->Write(($image_height*2) + 5, $first_name[$i].' '.$last_name[$i]);
	                    $pdf->SetX($start_x);
	                }
	                if($country[$i] != NULL){
	                    $pdf->Write(($image_height*2) + 10, $country[$i]);
	                    $pdf->SetX($start_x);
					}
					if($program[0] != 'A'){
						$pdf->Write(($image_height*2) + 15, $program[$i]);
					}
					if($department[$i] != NULL){
						$pdf->Write(($image_height*2) + 15, $department[$i]);
					}

	                //Space for side to side between images
	                $pdf->SetX($start_x + $image_width + 10);
	                $i++;

		            //Adds a new page if we have reached the threshold limit for number of images per page
					if($i % 36 == 0){
						$pdf->skipHeader = true;
						$pdf->skipFooter = true;
						$pdf->AddPage();
						$start_x = $pdf->GetX();
						$start_y = $pdf->GetY();
					}
	            }
	        }

	        //Reset the Y component after each row is drawn
	        $pdf->SetY($start_y + $image_height + 15);
	    }
		$folder = $TEMP_LINK_LOCATION.'/'.$token;
		$files = glob($folder . '/*');

		//Loop through the file list.
		foreach($files as $file){
		    //Make sure that this is a file and not a directory.
		    if(is_file($file)){
		        //Use the unlink function to delete the file.
		        unlink($file) or die("Cannot unlnk File");
		    }
		}
		$arr = array();
		ob_end_clean();
	    $pdf->Output();
		return $arr;
	}

	// Page header
	function Header($pdf, $program, $year){
	    // Arial bold 15
	    $pdf->SetFont('Arial','B',15);
	    // Move to the right
	    $pdf->Cell(75);
		// Title


		if($_SESSION['photo_roster']['value'] == 'program'){
			$pdf->Cell(30, 10, $program.' '.$year, 0, 0, 'C');
		}else if ($_SESSION['photo_roster']['value'] == 'course'){
			$pdf->Cell(30, 10, $program.' '.$year.' Section '.$_SESSION['photo_roster']['section'], 0, 0, 'C');
		}else if($_SESSION['photo_roster']['value'] == 'staff'){
			$pdf->Cell(30, 10, 'Simon Staff', 0, 0, 'C');
		}else if($_SESSION['photo_roster']['value'] == 'faculty'){
			$pdf->Cell(30, 10, 'Simon Faculty', 0, 0, 'C');
		}else if($_SESSION['photo_roster']['value'] == 'all'){
			$pdf->Cell(30, 10, 'All Current Simon Students, Faculty, and Staff', 0, 0, 'C');
		}else{
			//no value
		}
		
	    // Line break
	    $pdf->Ln(20);
	}
}
