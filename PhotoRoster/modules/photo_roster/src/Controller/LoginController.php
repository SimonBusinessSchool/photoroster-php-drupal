<?php

	namespace Drupal\photo_roster\Controller;

	use Drupal\Core\Controller\ControllerBase;
	use Symfony\Component\HttpFoundation\RedirectResponse;

	class LoginController extends ControllerBase{

		/**
		 * Initial logiin function sets up the login page
		 * returns a form and view 
		 * @return array $return_array
		 */
		function init(){
			session_start();
			if (isset( $_SESSION['photo_roster']['user_id'])) {
				//Redirect them to the login page
				return new RedirectResponse(\Drupal::url('photo_roster.homepage'));
			}
			\Drupal::service('page_cache_kill_switch')->trigger();
			//echo "Correct attempts " + $_SESSION['photo_roster']['attempts']; die();
			$myform = \Drupal::formBuilder()->getForm('Drupal\photo_roster\Form\PhotoRosterForm');
			if($_SESSION['photo_roster']['attempts'] > 0){
				$return_array = array(
					'#theme' => 'login',
					'#form' => $myform,
					'#error' => 'You have entered invalid login credentials',
				);
			}else{
				$return_array = array(
					'#theme' => 'login',
					'#form' => $myform,
					'#error' => '',
				);
			}

			return $return_array;
		}

		/**
		 * Logout function
		 * clears the session array of the user_id
		 * returns the display for the logout page to prompt user to log back in
		 * @return array $return_array
		 */
		function logout(){
			unset( $_SESSION['photo_roster']['user_id']);
			header("Cache-Control: no-cache, must-revalidate");
			header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header("Content-Type: application/xml; charset=utf-8");
			\Drupal::service('page_cache_kill_switch')->trigger();
			$return_array = array(
				'#theme' => 'logout',
			);

			return $return_array;
		}

	}
