<?php

	namespace Drupal\photo_roster\Form;

	use Drupal\Core\Form\FormBase;
	use Drupal\Core\Form\FormStateInterface;
	use Drupal\Core\Url;
	use Symfony\Component\HttpFoundation\RedirectResponse;
	/* This form allows users to search by individual student_search
	* names or id #'s
	* This class usurps the indiv_conn.php file
	*/

	$path = '';
	class StudentIndividualSearchForm extends FormBase{



		/**
		* {@inheritdoc}
		*/
		public function getFormId(){
			return 'module_photoroster_student_search_enter_form';
		}

		/**
		* {@inheritdoc}
		*/
		public function buildForm(array $form, FormStateInterface $form_state){

			//session_start();
			$token = $_SESSION['user_id'];

			/**
			* Header text
			*/
			$form['title_header'] = array(
				'#attributes' => array('style1'),
				'#markup' => '<h4><center>You can search by name or URID number</center></h4><br />',
			);


			//<table>
			$form['search_area'] = array(
				'#attributes' => array('id' => 'search_area'),
				'#type' => 'table',
			);
			$form['search_area'][0]['name'] = array(
				'item_one' => array(
					'#markup' => $this->t('<div class="style3">First Name:</div>'),
				),
				'item_two' => array(
					'#attributes' => array('id' => 'first_name_field'),
					'#type' => 'textfield',
				),
			);
			$form['search_area'][1]['name'] = array(
				'item_one' => array(
					'#markup' => $this->t('<div class="style3">Last Name:</div>'),
				),
				'item_two' => array(
					'#type' => 'textfield',
				),
			);
			$form['search_area'][0]['uid'] = array(
				'item_one' => array(
					'#markup' => $this->t('<div class="style3">URID Number:</div>'),
				),
				'item_two' => array(
					'#type' => 'textfield',
				),
			);

			$form['#action'] = '';

			$form['search_area_submit'] = array(
				'#type' => 'submit',
				'#value' => $this->t('Submit'),
				'#attributes' => array('style' => 'margin-left: 45%; font-size: 16px; padding: 4px 20px;'),
			);

			$form['blank_space'] = array(
				'#markup' => '<br /><br /><br /><br /><br /><br />',
			);


			return $form;
		}

		/**
		* {@inheritdoc}
		*/
		public function validateForm(array &$form, FormStateInterface $form_state) {
			//This works!!!
			global $path;

			if($form['search_area'][0]['uid']['item_two']['#value'] == NULL){
				if($form['search_area'][0]['name']['item_two']['#value'] != NULL){
					$fname =  $form['search_area'][0]['name']['item_two']['#value'];
					$path .= '&first_name='.$fname;
				}
				if($form['search_area'][1]['name']['item_two']['#value'] != NULL){
					$lname =  $form['search_area'][1]['name']['item_two']['#value'];
					$path .= '&last_name='.$lname;
				}
			}else {
				$path = '&UID='.$form['search_area'][0]['uid']['item_two']['#value'];
			}


			//$path = '&fname='.$fname.'&lname='.$lname;
		}

		/**
		* {@inheritdoc}
		*/
		public function submitForm(array &$form, FormStateInterface $form_state){
			//This works!!!
			//kint($form['search_area'][1]['name']['item_two']['#value']); die();
			global $path;
			$response = new RedirectResponse('indiv_search?usr=student'.$path);
  			$response->send();
			return;
		}

	}


 ?>
