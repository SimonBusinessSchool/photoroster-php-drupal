<?php

	namespace Drupal\photo_roster\Form;
	use Drupal\Core\Form\FormBase;
	use Drupal\Core\Form\FormStateInterface;
	/*
	* This class sets up the initial login
	* {@inheritdoc}
	*/

	$_SESSION['photo_roster']['attempts'] = 0;

	class PhotoRosterForm extends FormBase{
		/**
		* {@inheritdoc}
		*/
		public function getFormId(){
			return "module_photo_roster_login_form";
		}

		/**
		* {@inheritdoc}
		*/
		public function buildForm(array $form, FormStateInterface $form_state){

			
			unset($_SESSION['photo_roster']['bc']['last_page']);
			$form['#attached']['library'][] = 'photo_roster/home-display';
			$form['#attributes']['style'][] = 'font-size: 3rem; font-family: Proxima Nova ExtraBold;';
			//$form['#attributes']['class'][] = 'title';


			$form['user_id_title'] = [
				'#type' => 'text',
				'#markup' => 'UR AD Login',
			];

			$form['user_id'] = [
					//'#attributes' => array('class' => 'title', 'style' => 'font-size: 2rem;'),
					'#type' => 'textfield',
					'#placeholder' => 'firstname.lastname',
			];

			$form['password_title'] = [
				'#type' => 'text',
				'#markup' => 'Password',
			];

			$form['password'] = [
				'#type' => 'password',
			];

			$form['submit'] = [
				'#type' => 'submit',
				'#value' => $this->t('Log In'),
			];

			return $form;
		}

		/**
		* {@inheritdoc}
		*/
		public function submitForm(array &$form, FormStateInterface $form_state){



			session_start();
			
			//unset($_SESSION['photo_roster']['user_id']);
			session_set_cookie_params(60);
			$domain = 'ur.rochester.edu';
			$username = $form_state->getValue('user_id');
			$password = $form_state->getValue('password');
			//$password = 'fswbfkjgsb47873r8';
			$ldapconfig['host'] = 'ldap.ur.rochester.edu';
			$ldapconfig['port'] = 389;
			$ldapconfig['basedn'] = 'DC=UR,DC=Rochester,DC=edu';
			$error_message = "You have entered and incorrect username and password. Please try again!";
			$ds=ldap_connect($ldapconfig['host'], $ldapconfig['port']);
			ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);

			$dn="OU=simon".$ldapconfig['basedn'];
			//echo 'Bind: '.$ldapconfig['host'];
			if(substr($username, 0, 2) == "ur"){
				$username = str_replace(substr($username, 0, 3), '', $username);
			}
			//$isITuser = ldap_search($bind,$dn,'(&(objectClass=user)(sAMAccountName=peter.fosler))');
			try {

			} catch (\Exception $e) {

			}


			if ($bind=ldap_bind($ds, $username.'@'.$domain, $password) && $username != null && $password != null){
				$token = bin2hex($username);
				//echo "Correct"; die();
				$_SESSION['photo_roster']['attempts'] = 0;
				$_SESSION['photo_roster']['user_id'] = $token;
				header("Location: /photoroster/home");
				die();
			} else {
				$message = "Username and/or Password incorrect.\\nTry again.";
  				echo "<script type='text/javascript'>alert('$message');</script>";
				$_SESSION['photo_roster']['attempts']++;
				//echo "Incorrect attempts " + $_SESSION['photo_roster']['attempts']; die();
				$_SESSION['error_message'] = $error_message;
				header("Location: /photoroster/logon");
				die();
			}
		}
	}
