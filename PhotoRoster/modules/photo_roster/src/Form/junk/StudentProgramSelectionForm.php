<?php

	namespace Drupal\photo_roster\Form;

	use Drupal\Core\Form\FormBase;
	use Drupal\Core\Form\FormStateInterface;
	use Drupal\Core\Database\Database;
	use Drupal\Core\Database\Driver\sqlsrv;
	/* This form allows users to search by program_search
	* this class usurps the conn_prog.php file
	*/
	class StudentProgramSelectionForm extends FormBase{

		/**
		* {@inheritdoc}
		*/
		public function getFormId(){
			return 'module_photoroster_student_search_enter_form';
		}

		/**
		* {@inheritdoc}
		*/
		public function buildForm(array $form, FormStateInterface $form_state){


			$prog_stack = array();
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$conn = sqlsrv_connect( $serverName, $connectionInfo);

			if( $conn ) {
			     //echo "Connection established.<br />";

				$query = "SELECT DISTINCT sort_order, program
			 	   FROM photo_roster_student_view
			 	   ORDER BY sort_order";

				$result = sqlsrv_query($conn, $query);
				/*$form['table'] = [
					'#markup'
				]*/
				while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
      				//echo $row['program']."<br />";
					$prog = str_replace(" ", "_", $row['program']);
					$form[$prog] = [
						'#title' => $this->t('<div style="font-size: 2.5em">'.str_replace("_", " ", $row['program']).'</div><br />'),
						'#type' => 'link',
						'#url' => \Drupal\Core\Url::fromUri('internal:/photoroster/prog_search?program='.$row['program']),
					];
				}
			}else{
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}
			return $form;
		}

		/**
		* {@inheritdoc}
		*/
		public function submitForm(array &$form, FormStateInterface $form_state){

		}
	}


 ?>
