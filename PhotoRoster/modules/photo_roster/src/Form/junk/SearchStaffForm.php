<?php

	namespace Drupal\photo_roster\Form;

	use Drupal\Core\Form\FormBase;
	use Drupal\Core\Form\FormStateInterface;
	use Drupal\Core\Database\Database;

	/*
	* This class checks all available years of programs
	* so that the user can select the graduating class
	* of students enrolled in thei program
	* This class usurps the prog_search.php file
	*/
	class SearchStaffForm extends FormBase{

		/*
		* {@inheritdoc}
		*/
		public function getFormId(){
			return 'module_photoroster_search_staff_form';
		}

		public function buildForm(array $form, FormStateInterface $form_state){
			$form['ul'] = array(
				'#markup' => '<ul>',
			);

			$form['all_staff'] = array(
				'#type' => 'link',
				'#title' => $this->t('<li>All Staff</li>'),
				'#url' => \Drupal\Core\Url::fromUri('internal:/photoroster/roster?value=staff'),
			);

			$form['br'] = array(
				'#markup' => '<br>',
			);

			$form['indiv_staff'] = array(
				'#type' => 'link',
				'#title' => $this->t('<li>Individual Staff Search</li>'),
				'#url' => \Drupal\Core\Url::fromUri('internal:/photoroster/staff_indiv_conn'),
			);

			$form['endul'] = array(
				'#markup' => '</ul>',
			);

			$form['#attributes']['class'][] = 'search';

			return $form;


		}

		/**
		* {@inheritdoc}
		*/
		public function submitForm(array &$form, FormStateInterface $form_state){

		}
	}

?>
