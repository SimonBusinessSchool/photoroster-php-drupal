<?php

	namespace Drupal\photo_roster\Form;

	use Drupal\Core\Form\FormBase;
	use Drupal\Core\Form\FormStateInterface;
	use Drupal\Core\Database\Database;

	/*
	* This class checks all available years of programs
	* so that the user can select the graduating class
	* of students enrolled in thei program
	* This class usurps the prog_search.php file
	*/
	class Redirect extends FormBase{

		/*
		* {@inheritdoc}
		*/
		public function getFormId(){
			return 'module_photoroster_redirect_form';
		}

		public function buildForm(array $form, FormStateInterface $form_state){



			$form[strtolower($_SESSION['program'])] = [
				'#title' => 'Photo Roster',
				'#type' => 'link',
				'#url' => \Drupal\Core\Url::fromUri('Internal://RosterResultsForm.php'),
			];



			return $form;


		}

		/**
		* {@inheritdoc}
		*/
		public function submitForm(array &$form, FormStateInterface $form_state){

		}
	}

?>
