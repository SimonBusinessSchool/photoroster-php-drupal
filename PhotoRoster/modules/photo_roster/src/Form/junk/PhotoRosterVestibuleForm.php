<?php

	namespace Drupal\photo_roster\Form;

	use Drupal\Core\Form\FormBase;
	use Drupal\Core\Form\FormStateInterface;
	/* this form allows users to select based on
	* courses
	* This class usurps the course_search.php file
	*/
	class PhotoRosterVestibuleForm extends FormBase{

		/**
		* {@inheritdoc}
		*/
		public function getFormId(){
			return 'module_photoroster_student_search_enter_form';
		}

		/**
		* {@inheritdoc}
		*/
		public function buildForm(array $form, FormStateInterface $form_state){

			//session_start();
			//$token = $_SESSION['user_id'];
			//if (!isset( $_SESSION['user_id'])) {
				// Redirect them to the login page
			//	header("Location: /photoroster/login");
			//}
		    //$_SESSION['bc'] = "/student_search.php"

			$form['search_students'] = array(
				'#item' =>array(
					'uri' => 'public://rochester.png',
				),
				'#title' => $this->t('<br /><br /><h2><div class="style1">Students</div></h2><br /><br />'),
				'#type' => 'link',
				'#url' => \Drupal\Core\Url::fromRoute('photo_roster.student_search'),

			);

			$form['search_staff'] = [
				'#title' => $this->t('<h2><div class="style1">Staff</div></h2><br /><br />'),
				'#type' => 'link',
				'#url' => \Drupal\Core\Url::fromRoute('photo_roster.staff_search'),
			];

			$form['search_faculty'] = [
				'#title' => $this->t('<h2><div class="style1">Faculty</div></h2><br /><br />'),
				'#type' => 'link',
				'#url' => \Drupal\Core\Url::fromRoute('photo_roster.faculty_search'),
			];

			$form['search_all'] = [
			'#title' => $this->t('<h2><div class="style1">Simon All</div></h2><br /><br />'),
			'#type' => 'link',
			'#url' => \Drupal\Core\Url::fromUri('internal:/photoroster/roster?value=all'),

			];

			return $form;
		}

		/**
		* {@inheritdoc}
		*/
		public function submitForm(array &$form, FormStateInterface $form_state){

		}

	}


 ?>
