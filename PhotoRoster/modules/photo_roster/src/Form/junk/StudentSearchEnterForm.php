<?php

	namespace Drupal\photo_roster\Form;

	use Drupal\Core\Form\FormBase;
	use Drupal\Core\Form\FormStateInterface;
	/* This form allows users to select what
	* criteria they want to select by
	* this also usurps the old student_search.php file
	*/
	class StudentSearchEnterForm extends FormBase{

		/**
		* {@inheritdoc}
		*/
		public function getFormId(){
			return 'module_photoroster_student_search_enter_form';
		}

		/**
		* {@inheritdoc}
		*/
		public function buildForm(array $form, FormStateInterface $form_state){
			//phpinfo();
			//session_start();
			//$token = $_SESSION['user_id'];
			//if (!isset( $_SESSION['user_id'])) {
				// Redirect them to the login page
			//	header("Location: /photoroster/login");
			//}
		    //$_SESSION['bc'] = "/student_search.php"

			$form['program_search'] = [
				'#title' => $this->t('<h3><div class="style1">Search By Program</div></h3>'),
				'#type' => 'link',
				'#url' => \Drupal\Core\Url::fromRoute('photo_roster.program_search'),
			];

			$form['course_search'] = [
				'#title' => $this->t('<h3><div class="style1">Search By Course</div></h3>'),
				'#type' => 'link',
				'#url' => \Drupal\Core\Url::fromRoute('photo_roster.course_search'),
			];

			$form['student_indiv_search'] = [
				'#title' => $this->t('<h3><div class="style1">Search By Individual</div></h3>'),
				'#type' => 'link',
				'#url' => \Drupal\Core\Url::fromRoute('photo_roster.student_indiv_connection'),
			];

			return $form;
		}

		/**
		* {@inheritdoc}
		*/
		public function submitForm(array &$form, FormStateInterface $form_state){

		}

	}


 ?>
