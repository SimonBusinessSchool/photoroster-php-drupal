<?php

	namespace Drupal\photo_roster\Form;

	use Drupal\Core\Form\FormBase;
	use Drupal\Core\Form\FormStateInterface;
	use Drupal\Core\Database\Database;

	/*
	* This class checks all available years of programs
	* so that the user can select the graduating class
	* of students enrolled in thei program
	* This class usurps the prog_search.php file
	*/
	class ProgramSearchInputForm extends FormBase{

		/*
		* {@inheritdoc}
		*/
		public function getFormId(){
			return 'module_photoroster_program_search_input_form';
		}

		public function buildForm(array $form, FormStateInterface $form_state){
			$program = $_GET['program'];
			//echo 'The program selected is '.strtolower($program);

			$tempstore = \Drupal::service('user.private_tempstore')->get('photo_roster');
			$current_path = \Drupal::request()->getRequestUri();
			$tempstore->set('uri_path', $current_path);
			//echo 'The program selected is '.strtolower($program);

			$grad_year_stack = array();         //Used for storing the programs
			$serverName = "ITS-SIMNSQL-WP1\\MSSQLSERVER, 1433"; //serverName\instanceName, portNumber (default is 1433)
			$connectionInfo = array( "Database"=>"Photo_Roster", "UID"=>"boomi", "PWD"=>"D3ll_B00m1!#((+");
			$conn = sqlsrv_connect( $serverName, $connectionInfo);

			if( $conn ) {
			     //echo "Connection established.<br />";

				$query = "SELECT DISTINCT grad_year from SIS_student_roster
		            WHERE program = '".$_GET['program']."'
		            ORDER BY grad_year DESC";

				$result = sqlsrv_query($conn, $query);

				while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
      				//echo $row['program']."<br />";
					$prog = str_replace(" ", "_", $row['grad_year']);
					$form[$prog] = [
						'#title' => $this->t('<div style="font-size: 2.8rem; font-family: Proxima Nova ExtraBold;">'.str_replace("_", " ", $row['grad_year']).'</div><br />'),
						'#type' => 'link',
						'#url' => \Drupal\Core\Url::fromUri('internal:/photoroster/roster?program='.$program.'&year='.$row['grad_year']),
					];
				}

			}else{
			     echo "Connection could not be established.<br />";
			     die( print_r( sqlsrv_errors(), true));
			}
			return $form;
		}

		/**
		* {@inheritdoc}
		*/
		public function submitForm(array &$form, FormStateInterface $form_state){

		}
	}

?>
