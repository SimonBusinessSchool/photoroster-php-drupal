<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/simon_sass/templates/system/page.html.twig */
class __TwigTemplate_49004d7ae75ff40ef47f2135f98f42f3c13f489001f7d819d5f2a0d09bc55fff extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'header_wide_width' => [$this, 'block_header_wide_width'],
            'header_two_left' => [$this, 'block_header_two_left'],
            'header_two_right' => [$this, 'block_header_two_right'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'content_block_left' => [$this, 'block_content_block_left'],
            'content_block_right' => [$this, 'block_content_block_right'],
            'structured_content' => [$this, 'block_structured_content'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 54, "if" => 58, "block" => 59];
        $filters = ["clean_class" => 64, "escape" => 67, "t" => 76];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['clean_class', 'escape', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 54
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "fluid_container", [])) ? ("container-fluid") : ("container"));
        // line 55
        echo "

";
        // line 58
        if (($this->getAttribute(($context["page"] ?? null), "navigation", []) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", []))) {
            // line 59
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 96
        echo " 



";
        // line 101
        $this->displayBlock('main', $context, $blocks);
        // line 194
        echo "
";
        // line 195
        if ($this->getAttribute(($context["page"] ?? null), "content_block_left", [])) {
            // line 196
            echo "  ";
            $this->displayBlock('content_block_left', $context, $blocks);
            // line 201
            echo "  ";
        }
        // line 202
        echo "
";
        // line 203
        if ($this->getAttribute(($context["page"] ?? null), "content_block_right", [])) {
            // line 204
            echo "  ";
            $this->displayBlock('content_block_right', $context, $blocks);
            // line 209
            echo "  ";
        }
        // line 210
        echo "
";
        // line 211
        if ($this->getAttribute(($context["page"] ?? null), "structured_content", [])) {
            // line 212
            echo "  ";
            $this->displayBlock('structured_content', $context, $blocks);
            // line 217
            echo "  ";
        }
        // line 218
        echo "
";
        // line 219
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 220
            echo "  ";
            $this->displayBlock('footer', $context, $blocks);
        }
    }

    // line 59
    public function block_navbar($context, array $blocks = [])
    {
        // line 60
        echo "    ";
        // line 61
        $context["navbar_classes"] = [0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 63
($context["theme"] ?? null), "settings", []), "navbar_inverse", [])) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 64
($context["theme"] ?? null), "settings", []), "navbar_position", [])) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "navbar_position", []))))) : (($context["container"] ?? null)))];
        // line 67
        echo "    <header";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["navbar_attributes"] ?? null), "addClass", [0 => ($context["navbar_classes"] ?? null)], "method")), "html", null, true);
        echo " id=\"navbar\" role=\"banner\">
      ";
        // line 68
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method")) {
            // line 69
            echo "        <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo "\">
      ";
        }
        // line 71
        echo "      <div class=\"navbar-header\">
        ";
        // line 72
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
        ";
        // line 74
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 75
            echo "          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
            <span class=\"sr-only\">";
            // line 76
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation"));
            echo "</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        ";
        }
        // line 82
        echo "      </div>

      
      ";
        // line 86
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 87
            echo "        <div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
          ";
            // line 88
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
        </div>
      ";
        }
        // line 91
        echo "      ";
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method")) {
            // line 92
            echo "        </div>
      ";
        }
        // line 94
        echo "    </header>
  ";
    }

    // line 101
    public function block_main($context, array $blocks = [])
    {
        // line 102
        echo "  <div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 106
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 107
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 112
            echo "      ";
        }
        // line 113
        echo "
      ";
        // line 114
        if ($this->getAttribute(($context["page"] ?? null), "header_wide_width", [])) {
            // line 115
            echo "        ";
            $this->displayBlock('header_wide_width', $context, $blocks);
            // line 120
            echo "      ";
        }
        // line 121
        echo "
      ";
        // line 123
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header_two_left", [])) {
            // line 124
            echo "        ";
            $this->displayBlock('header_two_left', $context, $blocks);
            // line 129
            echo "      ";
        }
        echo " 

      ";
        // line 132
        echo "      
      ";
        // line 133
        if ($this->getAttribute(($context["page"] ?? null), "header_two_right", [])) {
            // line 134
            echo "        ";
            $this->displayBlock('header_two_right', $context, $blocks);
            // line 139
            echo "      ";
        }
        // line 140
        echo "      

      ";
        // line 143
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 144
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 149
            echo "      ";
        }
        // line 150
        echo "
      ";
        // line 152
        echo "      ";
        // line 153
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 154
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 155
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 156
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 157
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 160
        echo "      <section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

        ";
        // line 163
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 164
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 167
            echo "        ";
        }
        // line 168
        echo "
        ";
        // line 170
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 171
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 174
            echo "        ";
        }
        // line 175
        echo "
        ";
        // line 177
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 181
        echo "      </section>

      ";
        // line 184
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 185
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 190
            echo "      ";
        }
        // line 191
        echo "    </div>
  </div>
";
    }

    // line 107
    public function block_header($context, array $blocks = [])
    {
        // line 108
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 109
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 115
    public function block_header_wide_width($context, array $blocks = [])
    {
        // line 116
        echo "          <div class=\"headerWideWidth\" role=\"heading\">
            ";
        // line 117
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_wide_width", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 124
    public function block_header_two_left($context, array $blocks = [])
    {
        // line 125
        echo "          <div class=\"col-sm-4 header_two_left\" role=\"heading\">
            ";
        // line 126
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_two_left", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 134
    public function block_header_two_right($context, array $blocks = [])
    {
        // line 135
        echo "          <div class=\"col-sm-8\" role=\"heading\">
            ";
        // line 136
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_two_right", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 144
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 145
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 146
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 164
    public function block_highlighted($context, array $blocks = [])
    {
        // line 165
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 171
    public function block_help($context, array $blocks = [])
    {
        // line 172
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 177
    public function block_content($context, array $blocks = [])
    {
        // line 178
        echo "          <a id=\"main-content\"></a>
          ";
        // line 179
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 185
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 186
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 187
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 196
    public function block_content_block_left($context, array $blocks = [])
    {
        // line 197
        echo "    <div class=\"col-sm-6\" role=\"content_block_left\">
      ";
        // line 198
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_block_left", [])), "html", null, true);
        echo "
      </div> 
    ";
    }

    // line 204
    public function block_content_block_right($context, array $blocks = [])
    {
        // line 205
        echo "    <div class=\"col-sm-6\" role=\"content_block_right\">
      ";
        // line 206
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_block_right", [])), "html", null, true);
        echo "
      </div> 
    ";
    }

    // line 212
    public function block_structured_content($context, array $blocks = [])
    {
        // line 213
        echo "    <div class=\"structured-content container\"> 
      ";
        // line 214
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "structured_content", [])), "html", null, true);
        echo "
      </div> 
    ";
    }

    // line 220
    public function block_footer($context, array $blocks = [])
    {
        // line 221
        echo "    <footer class=\"footer ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" role=\"contentinfo\">
      ";
        // line 222
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
    </footer>
  ";
    }

    public function getTemplateName()
    {
        return "themes/simon_sass/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  496 => 222,  491 => 221,  488 => 220,  481 => 214,  478 => 213,  475 => 212,  468 => 206,  465 => 205,  462 => 204,  455 => 198,  452 => 197,  449 => 196,  442 => 187,  439 => 186,  436 => 185,  430 => 179,  427 => 178,  424 => 177,  417 => 172,  414 => 171,  407 => 165,  404 => 164,  397 => 146,  394 => 145,  391 => 144,  384 => 136,  381 => 135,  378 => 134,  371 => 126,  368 => 125,  365 => 124,  358 => 117,  355 => 116,  352 => 115,  345 => 109,  342 => 108,  339 => 107,  333 => 191,  330 => 190,  327 => 185,  324 => 184,  320 => 181,  317 => 177,  314 => 175,  311 => 174,  308 => 171,  305 => 170,  302 => 168,  299 => 167,  296 => 164,  293 => 163,  287 => 160,  285 => 157,  284 => 156,  283 => 155,  282 => 154,  281 => 153,  279 => 152,  276 => 150,  273 => 149,  270 => 144,  267 => 143,  263 => 140,  260 => 139,  257 => 134,  255 => 133,  252 => 132,  246 => 129,  243 => 124,  240 => 123,  237 => 121,  234 => 120,  231 => 115,  229 => 114,  226 => 113,  223 => 112,  220 => 107,  217 => 106,  210 => 102,  207 => 101,  202 => 94,  198 => 92,  195 => 91,  189 => 88,  186 => 87,  183 => 86,  178 => 82,  169 => 76,  166 => 75,  163 => 74,  159 => 72,  156 => 71,  150 => 69,  148 => 68,  143 => 67,  141 => 64,  140 => 63,  139 => 61,  137 => 60,  134 => 59,  128 => 220,  126 => 219,  123 => 218,  120 => 217,  117 => 212,  115 => 211,  112 => 210,  109 => 209,  106 => 204,  104 => 203,  101 => 202,  98 => 201,  95 => 196,  93 => 195,  90 => 194,  88 => 101,  82 => 96,  78 => 59,  76 => 58,  72 => 55,  70 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/simon_sass/templates/system/page.html.twig", "/opt/local/www/drupal-photoroster/themes/simon_sass/templates/system/page.html.twig");
    }
}
