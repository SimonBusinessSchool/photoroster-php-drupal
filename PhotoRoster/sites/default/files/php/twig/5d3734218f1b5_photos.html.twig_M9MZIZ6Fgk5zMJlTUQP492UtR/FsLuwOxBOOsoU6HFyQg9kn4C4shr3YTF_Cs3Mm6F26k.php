<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/photos.html.twig */
class __TwigTemplate_c1d07b0364bad9afcac0a507d103024dc19fc81bf3e3167ab33120311109a387 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 3, "if" => 5, "for" => 59];
        $filters = ["escape" => 1, "replace" => 9, "render" => 67, "length" => 75];
        $functions = ["attach_library" => 1, "url" => 30, "file_url" => 67];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if', 'for'],
                ['escape', 'replace', 'render', 'length'],
                ['attach_library', 'url', 'file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/large-image-display"), "html", null, true);
        echo "
<section ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
\t";
        // line 3
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/photos.html.twig", 3)->display($context);
        // line 4
        echo "\t<div style=\"width: 80%; margin: 0 auto;\">
\t";
        // line 5
        if (( !($context["selected_program"] ?? null) == null)) {
            // line 6
            echo "\t\t<div class=\"title\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["selected_program"] ?? null)), "html", null, true);
            echo " Class of ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["graduating_year"] ?? null)), "html", null, true);
            echo "</div><br />
\t";
        }
        // line 8
        echo "\t";
        if (( !($context["selected_term"] ?? null) == null)) {
            // line 9
            echo "\t\t<center><h3>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed(($context["selected_term"] ?? null)), "_", " "), "html", null, true);
            echo "<br> Course: ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["selected_course"] ?? null)), "html", null, true);
            echo " Section: ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["selected_section"] ?? null)), "html", null, true);
            echo "</h3></center><br />
\t";
        }
        // line 11
        echo "\t";
        if ((($context["value"] ?? null) == "all")) {
            // line 12
            echo "\t\t<center><div class=\"title\">All Current Simon Students, Faculty, and Staff</div></center>
\t";
        }
        // line 14
        echo "
\t&nbsp;

\t";
        // line 17
        if (($this->getAttribute(($context["breadcrumb"] ?? null), 4, [], "array") != null)) {
            // line 18
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 2, [], "array")), "html", null, true);
            echo "\">
\t\t\t";
            // line 19
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 3, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 3, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 4, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 20
($context["breadcrumb"] ?? null), 3, [], "array") != null)) {
            // line 21
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 2, [], "array")), "html", null, true);
            echo "\">
\t\t\t";
            // line 22
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 3, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 23
($context["breadcrumb"] ?? null), 2, [], "array") != null)) {
            // line 24
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 25
($context["breadcrumb"] ?? null), 1, [], "array") != null)) {
            // line 26
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</div>
\t";
        }
        // line 28
        echo "\t";
        if ((((($context["value"] ?? null) == "staff") || (($context["value"] ?? null) == "faculty")) || (($context["value"] ?? null) == "all"))) {
            // line 29
            echo "
\t\t<a href=\"";
            // line 30
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.homepage"));
            echo "\"><button class=\"btn btn-primary\">Back to search</button></a>
\t";
        } elseif ((        // line 31
($context["value"] ?? null) == "course")) {
            // line 32
            echo "\t\t<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.course_search"));
            echo "\"><button class=\"btn btn-primary\">Back to search</button></a>
\t";
        } elseif ((        // line 33
($context["value"] ?? null) == "program")) {
            // line 34
            echo "\t\t<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.program_search"));
            echo "\"><button class=\"btn btn-primary\">Back to search</button></a>
\t";
        }
        // line 36
        echo "\t
\t\t\t<a href=\"";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.pdf"));
        echo "\"><button class=\"btn btn-primary\">Download as PDF</button></a>
\t";
        // line 39
        echo "

\t<br>
\t<br>
\t<div class=\"photo_bg\">
\t\t<br>
\t\t<!-- The Modal -->
\t\t<div id=\"myModal\" class=\"modal\">
\t\t\t<!-- The Close Button -->
\t\t\t<span class=\"close\">&times;</span>

\t\t\t<!-- Modal Content (The Image) -->
\t\t\t<img class=\"modal-content-local\" id=\"img01\">

\t\t\t<div class=\"modal-content-local\">
\t\t\t\t<button class=\"btn btn-primary\" id=\"download-image-button\">Download</button>&nbsp;<button class=\"btn btn-primary\" id=\"closeButton\">Close</button>
\t\t\t</div>
\t\t</div>
\t\t<table class=\"fixed\" style=\"width: 100%;\">
\t\t\t<tr>
\t\t\t\t";
        // line 59
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["first_name"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["fname"]) {
            // line 60
            echo "\t\t\t\t\t<td>
\t\t\t\t\t\t";
            // line 62
            echo "\t\t\t\t\t\t";
            if (( !$this->getAttribute(($context["photo_image"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == null)) {
                // line 63
                echo "\t\t\t\t\t\t<br>
\t\t\t\t\t\t<center><img alt=\"Broken Photo Link\" onerror=this.src=\"/sites/default/files/rochester.png\"  onClick=\"selectPhoto(";
                // line 64
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["uid"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo ", ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index0", [])), "html", null, true);
                echo ")\" class=\"myImg\" id=\"myImg";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index0", [])), "html", null, true);
                echo "\" data=\"data:image/jpg;base64,";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["large_photo_image"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "\" src=\"data:image/jpg;base64,";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["photo_image"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "\" height=\"200\" ></center>
\t\t\t\t\t\t";
            } else {
                // line 66
                echo "\t\t\t\t\t\t<br>
\t\t\t\t\t\t<center><img border=\"3\" alt=\"\" src=\"";
                // line 67
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>")) . call_user_func_array($this->env->getFunction('file_url')->getCallable(), ["/sites/default/files/rochester.png"])), "html", null, true);
                echo "\" height=\"200\"></center>
\t\t\t\t\t\t";
            }
            // line 69
            echo "\t\t\t\t\t\t<center>
\t\t\t\t\t\t\t<table class=\"fixed\">
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t";
            // line 74
            echo "\t\t\t\t\t\t\t\t\t\t";
            if ((( !$this->getAttribute(($context["preferred_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == null) && ( !$this->getAttribute(($context["preferred_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == " "))) {
                // line 75
                echo "\t\t\t\t\t\t\t\t\t\t\t";
                if ((twig_length_filter($this->env, ($context["preferred_name"] ?? null)) > 10)) {
                    // line 76
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<center>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["preferred_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "&nbsp;";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "</center>
\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 78
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<center>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["preferred_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "&nbsp;";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "</center>
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 80
                echo "\t\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 81
                echo "\t\t\t\t\t\t\t\t\t\t\t";
                if ((twig_length_filter($this->env, ($context["first_name"] ?? null)) > 10)) {
                    // line 82
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<center>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["first_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "&nbsp;";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "</center>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((twig_length_filter($this->env,                 // line 83
($context["first_name"] ?? null)) <= 10)) {
                    // line 84
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<center>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["preferred_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "&nbsp;";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "</center>
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 86
                echo "\t\t\t\t\t\t\t\t\t\t";
            }
            // line 87
            echo "
\t\t\t\t\t\t\t\t\t\t";
            // line 89
            echo "\t\t\t\t\t\t\t\t\t\t";
            if (($this->getAttribute(($context["dept"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == null)) {
                // line 90
                echo "\t\t\t\t\t\t\t\t\t\t\t";
                if (( !$this->getAttribute(($context["country"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == null)) {
                    // line 91
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<center>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["country"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "</center>
\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 93
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<center><br/></center>
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 95
                echo "\t\t\t\t\t\t\t\t\t\t";
            }
            // line 96
            echo "
\t\t\t\t\t\t\t\t\t\t";
            // line 97
            if ((($context["selected_program"] ?? null) == null)) {
                // line 98
                echo "\t\t\t\t\t\t\t\t\t\t";
                // line 99
                echo "\t\t\t\t\t\t\t\t\t\t\t";
                if (( !$this->getAttribute(($context["program"] ?? null), $this->getAttribute($context["loop"], "index", []), [], "array") == null)) {
                    // line 100
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<center>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["program"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                    echo "</center>
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 102
                echo "\t\t\t\t\t\t\t\t\t\t";
            }
            // line 103
            echo "\t\t\t\t\t\t\t\t\t\t";
            if (( !$this->getAttribute(($context["dept"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == null)) {
                // line 104
                echo "\t\t\t\t\t\t\t\t\t\t\t<center>";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["dept"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "<center>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 106
            echo "\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</center>
\t\t\t\t\t</td>
\t\t\t\t\t";
            // line 111
            if (((($this->getAttribute($context["loop"], "index", []) % 5) == 0) &&  !$this->getAttribute($context["loop"], "last", []))) {
                // line 112
                echo "\t\t\t\t</tr><tr style=\"border: 1px;\">
\t\t\t\t";
            }
            // line 114
            echo "\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fname'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "\t\t\t</tr>
\t\t</table>
\t\t<br />
\t</div>
\t<h1> &nbsp; </h1>
\t<h1> &nbsp; </h1>
\t";
        // line 121
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/photos.html.twig", 121)->display($context);
        // line 122
        echo "</div>
\t<script>
\t\t//================Download Large Image as JPEG==============================================
\t\tdocument.getElementById('download-image-button').onclick = function(){downloadLargeImage();};
\t</script>
</section>
";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/photos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  418 => 122,  416 => 121,  408 => 115,  394 => 114,  390 => 112,  388 => 111,  381 => 106,  375 => 104,  372 => 103,  369 => 102,  363 => 100,  360 => 99,  358 => 98,  356 => 97,  353 => 96,  350 => 95,  346 => 93,  340 => 91,  337 => 90,  334 => 89,  331 => 87,  328 => 86,  320 => 84,  318 => 83,  311 => 82,  308 => 81,  305 => 80,  297 => 78,  289 => 76,  286 => 75,  283 => 74,  277 => 69,  272 => 67,  269 => 66,  256 => 64,  253 => 63,  250 => 62,  247 => 60,  230 => 59,  208 => 39,  204 => 37,  201 => 36,  195 => 34,  193 => 33,  188 => 32,  186 => 31,  182 => 30,  179 => 29,  176 => 28,  166 => 26,  164 => 25,  151 => 24,  149 => 23,  143 => 22,  130 => 21,  128 => 20,  118 => 19,  105 => 18,  103 => 17,  98 => 14,  94 => 12,  91 => 11,  81 => 9,  78 => 8,  70 => 6,  68 => 5,  65 => 4,  63 => 3,  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/photos.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/photos.html.twig");
    }
}
