<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/program-search.html.twig */
class __TwigTemplate_73a29b6fee03151d2ea02011e210bf085711cf321161238418c6d036dae7ceba extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 3, "if" => 7];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1, "url" => 17];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if'],
                ['escape'],
                ['attach_library', 'url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/home-display"), "html", null, true);
        echo "
<section ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
\t";
        // line 3
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/program-search.html.twig", 3)->display($context);
        // line 4
        echo "\t<div style=\"width: 80%; margin: 0 auto;\">
\t<div class=\"title\">";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pageTitle"] ?? null)), "html", null, true);
        echo "</div>

\t";
        // line 7
        if (($this->getAttribute(($context["breadcrumb"] ?? null), 3, [], "array") != null)) {
            // line 8
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 2, [], "array")), "html", null, true);
            echo "\">
\t\t\t";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 3, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 10
($context["breadcrumb"] ?? null), 2, [], "array") != null)) {
            // line 11
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 12
($context["breadcrumb"] ?? null), 2, [], "array") == null)) {
            // line 13
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</div>
\t";
        }
        // line 15
        echo "\t<br>
\t&nbsp;
\t<a href=\"";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.homepage"));
        echo "\"><button class=\"btn btn-primary\">Back to Home</button></a><p>Please select program of choice:</p>
\t<center>
\t<div class=\"program_table\" style=\"overflow: scroll;\">
\t\t<center>
\t\t\t<table style=\"overflow: scroll;\">
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 1, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 1, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 0, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 0, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 27
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 2, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 2, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 8, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 8, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 3, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 3, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 7, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 7, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 4, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 4, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 6, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 6, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 39
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 9, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 9, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 10, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 10, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\">&nbsp;</td>
\t\t\t\t\t<td class=\"tdclassnobg\" style=\"min-width: 500px;\"><a href=\"";
        // line 44
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["url"] ?? null), 11, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title"] ?? null), 11, [], "array")), "html", null, true);
        echo "</a></td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</center>
\t</div>
\t</center>
</div>
\t";
        // line 51
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/program-search.html.twig", 51)->display($context);
        // line 52
        echo "</section>
";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/program-search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 52,  217 => 51,  205 => 44,  196 => 40,  190 => 39,  182 => 36,  176 => 35,  168 => 32,  162 => 31,  154 => 28,  148 => 27,  140 => 24,  134 => 23,  125 => 17,  121 => 15,  111 => 13,  109 => 12,  96 => 11,  94 => 10,  88 => 9,  75 => 8,  73 => 7,  68 => 5,  65 => 4,  63 => 3,  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/program-search.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/program-search.html.twig");
    }
}
