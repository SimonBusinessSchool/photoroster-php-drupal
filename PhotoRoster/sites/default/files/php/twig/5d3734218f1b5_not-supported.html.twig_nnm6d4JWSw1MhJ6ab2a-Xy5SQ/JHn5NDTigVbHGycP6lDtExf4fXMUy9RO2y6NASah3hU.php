<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/not-supported.html.twig */
class __TwigTemplate_1f56c6f468f238ea5d1045747f2e9947d21e2e0fc289c65e7c0495071d783e93 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 4];
        $filters = ["escape" => 1];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">


\t";
        // line 4
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/not-supported.html.twig", 4)->display($context);
        // line 5
        echo "<div style=\"height: 60vh\">
    <div style=\"width: 80%; text-align: center; margin: auto; height: 100%\">
        <h3>Sorry but this functionality isn't supported on this platform yet.<br>
            Please use either Firefox or Google Chrome on a non-mobile device to access this feature.</h3>

        <button onclick=\"goBack()\">Back</button>
    </div>
    <div id=\"footer\" style=\"bottom: 0; position: relative;\">
\t";
        // line 13
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/not-supported.html.twig", 13)->display($context);
        // line 14
        echo "    </div>
</div>
    <script>
        function goBack(){
            window.history.back();
        }
        
    </script>
    
</section>
";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/not-supported.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 14,  74 => 13,  64 => 5,  62 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/not-supported.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/not-supported.html.twig");
    }
}
