<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/course-selection.html.twig */
class __TwigTemplate_ef78bd295c757108df42ac5d3b4e010ae6faf38a672870ebf39e3c305e3bfa4a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 16, "if" => 19, "for" => 49];
        $filters = ["escape" => 2, "replace" => 50, "length" => 103];
        $functions = ["attach_library" => 2, "url" => 26];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if', 'for'],
                ['escape', 'replace', 'length'],
                ['attach_library', 'url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/searchbar-functionality-all-browsers"), "html", null, true);
        echo "
";
        // line 4
        echo "<script>
\twindow.addEventListener( \"pageshow\", function ( event ) {
\t\tvar historyTraversal = event.persisted || ( typeof window.performance != \"undefined\" && window.performance.navigation.type === 2 );
\t\tif ( historyTraversal ) {
\t\t\t// Handle page restore.
\t\t\tdocument.getElementById(\"semesterForm\").reset();
\t\t\tdocument.getElementById(\"courseForm\").reset();
\t\t\tdocument.getElementById(\"sectionForm\").reset();
\t\t}
\t});
</script>
<section ";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
\t";
        // line 16
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/course-selection.html.twig", 16)->display($context);
        // line 17
        echo "\t<div style=\"width: 80%; margin: 0 auto;\">
\t<div class=\"title\">Search by Course and Section</div>
\t";
        // line 19
        if (($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array") != null)) {
            // line 20
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;>>&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 21
($context["breadcrumb"] ?? null), 2, [], "array") == null)) {
            // line 22
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;>>&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</div>
\t";
        }
        // line 24
        echo "\t<br>
\t&nbsp;
\t<a href=\"";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.homepage"));
        echo "\"><button class=\"btn btn-primary\">Back to Home</button></a>
\t<br>
\t<br>
\t<div style=\"width: auto\">
\t\t<br>
\t\t<table style=\"width: 100%\" style=\"border-spacing: 125px; border-collapse: seperate;\">
\t\t\t<tr>
\t\t\t\t<th>
\t\t\t\t\t<a href=\"";
        // line 34
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.course_search"));
        echo "\"><button class=\"btn btn-primary\">Refresh Page</button></a>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t</th>
\t\t\t</tr>
\t\t\t<tr>
\t\t\t\t<form id=\"semesterForm\" method=\"post\">
\t\t\t\t\t<div class=\"autocomplete\">
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t<br />
\t\t\t\t\t\t\t<p>Start by selecting the semester of your course:</p>
\t\t\t\t\t\t\t<br />
\t\t\t\t\t\t\t<input id=\"hidden_output\" type=\"hidden\" />
\t\t\t\t\t\t\t<input id=\"input_term\" class=\"dropdown\" style=\"background-image: url('";
        // line 47
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "' ~ '/modules/photo_roster/include/rochester2.png')\" type=\"text\" name=\"term\" list=\"term_list\" placeholder=\"Term\" autofocus=\"true\"/>
\t\t\t\t\t\t\t<datalist id=\"term_list\">
\t\t\t\t\t\t\t\t";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["semester"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["sem"]) {
            // line 50
            echo "\t\t\t\t\t\t\t\t\t<option id=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["semester"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
            echo "\" name=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["semester"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), [" " => "%20"]), "html", null, true);
            echo "\" class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["semester"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
            echo "\" value=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["semester"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
            echo "\"></option>
\t\t\t\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "\t\t\t\t\t\t\t</datalist>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<p>Then select Your course number:</p>
\t\t\t\t\t\t\t<br />
\t\t\t\t\t\t</th>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</tr>
\t\t\t<tr>
\t\t\t\t<form id=\"courseForm\" method=\"post\">
\t\t\t\t\t<div class=\"autocomplete\">
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t<!--<input type=\"hidden\" id=\"input_term\" type=\"text\" name=\"term\"  list=\"course_list\" value=\"<?php echo \$sel_term ?>\" />-->
\t\t\t\t\t\t\t<input id=\"input_course\" class=\"dropdown\" class=\"dropdown\" type=\"text\" name=\"course\" list=\"course_list\" placeholder=\"Course Number\" disabled=\"true\"/>
\t\t\t\t\t\t\t<datalist id=\"course_list\">
\t\t\t\t\t\t\t</datalist>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<p>And then the section number:</p>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</th>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</tr>
\t\t\t<br />
\t\t\t<tr>
\t\t\t\t<form id=\"sectionForm\" method=\"post\">
\t\t\t\t\t<div class=\"autocomplete\">
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t<input id=\"input_section\" class=\"dropdown\" class=\"dropdown\" type=\"text\" name=\"section\" list=\"section_list\" placeholder=\"Section Number\" disabled=\"true\"/>
\t\t\t\t\t\t\t<datalist id=\"section_list\">
\t\t\t\t\t\t\t</datalist>
\t\t\t\t\t\t</th>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</tr>
\t\t</table>
\t\t<br>
\t\t<br>
\t\t<br>
\t\t<br>
\t\t<br>
\t\t<br>
\t</div>
\t<h1> &nbsp; </h1>>
\t";
        // line 100
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/course-selection.html.twig", 100)->display($context);
        // line 101
        echo "</div>
\t<script type=\"text/javascript\">
\t\tdocument.getElementById(\"input_term\").onselect = function() {refreshPage(\"input_term\", \"";
        // line 103
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_length_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["semester"] ?? null))), "html", null, true);
        echo "\", document.getElementById(\"input_term\").value)};
\t\tdocument.getElementById(\"input_course\").onselect = function() {refreshPage(\"input_course\")};
\t\tdocument.getElementById(\"input_section\").onselect = function() {refreshPage(\"input_section\")};
\t</script>
</section>
";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/course-selection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 103,  237 => 101,  235 => 100,  185 => 52,  162 => 50,  145 => 49,  140 => 47,  124 => 34,  113 => 26,  109 => 24,  99 => 22,  97 => 21,  84 => 20,  82 => 19,  78 => 17,  76 => 16,  72 => 15,  59 => 4,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/course-selection.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/course-selection.html.twig");
    }
}
