<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/course-selection-all-browsers.html.twig */
class __TwigTemplate_13598805076cd5bdc6a13155e2b9d42f5ae52e26c36574cd41468f6cccf30fb5 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 15, "if" => 18, "for" => 49];
        $filters = ["escape" => 1, "replace" => 50, "length" => 103];
        $functions = ["attach_library" => 1, "url" => 25];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if', 'for'],
                ['escape', 'replace', 'length'],
                ['attach_library', 'url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/searchbar-functionality-all-browsers"), "html", null, true);
        echo "
";
        // line 3
        echo "<script>
\twindow.addEventListener( \"pageshow\", function ( event ) {
\t\tvar historyTraversal = event.persisted || ( typeof window.performance != \"undefined\" && window.performance.navigation.type === 2 );
\t\tif ( historyTraversal ) {
\t\t\t// Handle page restore.
\t\t\tdocument.getElementById(\"semesterForm\").reset();
\t\t\tdocument.getElementById(\"courseForm\").reset();
\t\t\tdocument.getElementById(\"sectionForm\").reset();
\t\t}
\t});
</script>
<section ";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
\t";
        // line 15
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/course-selection-all-browsers.html.twig", 15)->display($context);
        // line 16
        echo "\t<div style=\"width: 80%; margin: 0 auto;\">
\t<div class=\"title\">Search by Course and Section</div>
\t";
        // line 18
        if (($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array") != null)) {
            // line 19
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;>>&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 20
($context["breadcrumb"] ?? null), 2, [], "array") == null)) {
            // line 21
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;>>&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</div>
\t";
        }
        // line 23
        echo "\t<br>
\t&nbsp;
\t<a href=\"";
        // line 25
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.homepage"));
        echo "\"><button class=\"btn btn-primary\">Back to Home</button></a>
\t<br>
\t<br>
\t<div style=\"width: auto\">
\t\t<br>
\t\t<table style=\"width: 100%\" style=\"border-spacing: 125px; border-collapse: seperate;\">
\t\t\t<tr>
\t\t\t\t<th>
\t\t\t\t\t<a href=\"";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.course_search"));
        echo "\"><button class=\"btn btn-primary\">Refresh Page</button></a>
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t</th>
\t\t\t</tr>
\t\t\t<tr>
\t\t\t\t<form id=\"semesterForm\" method=\"post\">
\t\t\t\t\t<div class=\"autocomplete\">
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t<br />
\t\t\t\t\t\t\t<p class=\"course_selection_statements\">Start by selecting the semester of your course:</p>
\t\t\t\t\t\t\t<br />

\t\t\t\t\t\t\t<input id=\"hidden_output\" type=\"hidden\" />
\t\t\t\t\t\t\t<input id=\"input_term\" class=\"dropdown\" type=\"text\" name=\"term\" list=\"term_list\" placeholder=\"Term\" autofocus=\"true\">
\t\t\t\t\t\t\t<datalist id=\"term_list\">
\t\t\t\t\t\t\t\t";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["semester"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["sem"]) {
            // line 50
            echo "\t\t\t\t\t\t\t\t\t<option id=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["semester"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
            echo "\" name=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["semester"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), [" " => "%20"]), "html", null, true);
            echo "\" class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["semester"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
            echo "\" value=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["semester"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
            echo "\"></option>
\t\t\t\t\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "\t\t\t\t\t\t\t</datalist>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<p class=\"course_selection_statements\">Then select Your course number:</p>
\t\t\t\t\t\t\t<br />
\t\t\t\t\t\t</th>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</tr>
\t\t\t<tr>
\t\t\t\t<form id=\"courseForm\" method=\"post\">
\t\t\t\t\t<div class=\"autocomplete\">
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t<!--<input type=\"hidden\" id=\"input_term\" type=\"text\" name=\"term\"  list=\"course_list\" value=\"<?php echo \$sel_term ?>\" />-->
\t\t\t\t\t\t\t<input id=\"input_course\" class=\"dropdown\" type=\"text\" name=\"course\" list=\"course_list\" placeholder=\"Course Number\" disabled=\"true\"/>
\t\t\t\t\t\t\t<datalist id=\"course_list\">
\t\t\t\t\t\t\t</datalist>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<p class=\"course_selection_statements\">And then the section number:</p>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</th>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</tr>
\t\t\t<br />
\t\t\t<tr>
\t\t\t\t<form id=\"sectionForm\" method=\"post\">
\t\t\t\t\t<div class=\"autocomplete\">
\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t<input id=\"input_section\" class=\"dropdown\" type=\"text\" name=\"section\" list=\"section_list\" placeholder=\"Section Number\" disabled=\"true\"/>
\t\t\t\t\t\t\t<datalist id=\"section_list\">
\t\t\t\t\t\t\t</datalist>
\t\t\t\t\t\t</th>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</tr>
\t\t</table>
\t\t<br>
\t\t<br>
\t\t<br>
\t\t<br>
\t\t<br>
\t\t<br>
\t</div>
\t<h1> &nbsp; </h1>
\t";
        // line 100
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/course-selection-all-browsers.html.twig", 100)->display($context);
        // line 101
        echo "</div>
\t<script type=\"text/javascript\">
\t\tdocument.getElementById(\"input_term\").onchange = function() {refreshPage(\"input_term\", \"";
        // line 103
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_length_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["semester"] ?? null))), "html", null, true);
        echo "\", document.getElementById(\"input_term\").value)};
\t\tdocument.getElementById(\"input_course\").onchange = function() {refreshPage(\"input_course\")};
\t\tdocument.getElementById(\"input_section\").onchange = function() {refreshPage(\"input_section\")};
\t</script>
</section>
";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/course-selection-all-browsers.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 103,  235 => 101,  233 => 100,  183 => 52,  160 => 50,  143 => 49,  124 => 33,  113 => 25,  109 => 23,  99 => 21,  97 => 20,  84 => 19,  82 => 18,  78 => 16,  76 => 15,  72 => 14,  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/course-selection-all-browsers.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/course-selection-all-browsers.html.twig");
    }
}
