<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/student-indiv-search.html.twig */
class __TwigTemplate_d8fbcfc8a90b0c3406d65cc4c937f2dd17dfc6e37255c27ef13fffbe47020056 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 3, "if" => 6];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1, "url" => 16];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if'],
                ['escape'],
                ['attach_library', 'url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/form-display"), "html", null, true);
        echo "
<section ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
    ";
        // line 3
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/student-indiv-search.html.twig", 3)->display($context);
        // line 4
        echo "\t<div style=\"width: 80%; margin: 0 auto;\">
\t<div class=\"title\">";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pageTitle"] ?? null)), "html", null, true);
        echo "</div>
\t";
        // line 6
        if (($this->getAttribute(($context["breadcrumb"] ?? null), 3, [], "array") != null)) {
            // line 7
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 2, [], "array")), "html", null, true);
            echo "\">
\t\t\t";
            // line 8
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 3, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 9
($context["breadcrumb"] ?? null), 2, [], "array") != null)) {
            // line 10
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 11
($context["breadcrumb"] ?? null), 2, [], "array") == null)) {
            // line 12
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</div>
\t";
        }
        // line 14
        echo "\t<br>
\t&nbsp;
    <a href=\"";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.homepage"));
        echo "\"><button class=\"btn btn-primary\">Back to Home</button></a><img src=\"/sites/default/files/icons/full/Cap.png\" height=\"100px\" style=\"margin-top: 10px; margin-left: 35%;\"></img>
        <img src=\"/sites/default/files/icons/full/Individual.png\" height=\"100px\" style=\"margin-top: 10px;\"></img>
\t<br>
    <div style=\"width: 80%; margin: auto;\">
        ";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["form"] ?? null)), "html", null, true);
        echo "
\t\t<h1> &nbsp; </h1>
    </div>
\t
\t";
        // line 24
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/student-indiv-search.html.twig", 24)->display($context);
        // line 25
        echo "\t</div>
</section>";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/student-indiv-search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 25,  138 => 24,  131 => 20,  124 => 16,  120 => 14,  110 => 12,  108 => 11,  95 => 10,  93 => 9,  87 => 8,  74 => 7,  72 => 6,  68 => 5,  65 => 4,  63 => 3,  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/student-indiv-search.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/student-indiv-search.html.twig");
    }
}
