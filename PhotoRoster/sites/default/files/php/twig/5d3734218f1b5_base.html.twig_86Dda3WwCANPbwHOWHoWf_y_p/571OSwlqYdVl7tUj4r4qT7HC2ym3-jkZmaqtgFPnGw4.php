<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @simon_sass/base.html.twig */
class __TwigTemplate_b5dfd01df63e9a91a49e87aed5f0fa2ffd8366ab1eeefad4a593f4effd8417b9 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/footer"), "html", null, true);
        echo "
<section ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">
    <br>
    <div class=\"footer\">
        <div class=\"main_inset_left\">
            <table class=\"fixed\">
                <tr>
                    <td>
                    <div class=\"inset\">
                    <br>
                        <ul id=\"footer_values\">
                            <li><a href=\"http://simon.rochester.edu/contact-us/index.aspx\">Contact Us </a></li>
                            <li><a href=\"http://simon.rochester.edu/news-and-media/news/index.aspx\">News and Media</a></li>
                            <li><a href=\"http://simon.rochester.edu/cmc/career-management-center/index.aspx\">Career Management Center</a></li>
                            <li><a href=\"http://simon.rochester.edu/faculty-and-research/faculty-directory/index.aspx\">Directory</a></li>
                            <li><a href=\"http://simon.rochester.edu/about-simon/index.aspx\">About Simon</a></li>
                            <li><a href=\"http://simon.rochester.edu/academic-programs/index.aspx\">Programs</a></li></ul><ul id=\"mobile_footer_values\">
                        </ul>
                        <br>
                        </div>
                    </td>
                    <td>
                        <h4 style=\" margin-bottom: 0; padding-bottom: 0;\"><a href=\"http://simon.rochester.edu/index.aspx\" style=\"font-size:20px;\" target=\"\" title=\"Simon Business School\">Simon Business School</a></h4>
                        <ul class=\"footer_values\">
                            <li><a href=\"http://simon.rochester.edu/contact-simon/index.aspx\" target=\"\" title=\"Contact Simon Staff\">Contact</a></li>
                            <li><a href=\"http://simon.rochester.edu/apply-now/index.aspx\" target=\"\" title=\"Apply Now\">Apply</a></li>
                            <li><a href=\"https://rochmba.askadmissions.net/vip/\" target=\"_blank\">Portal Login</a></li>
                            <li><a href=\"http://simon.rochester.edu/alumni/simon-school-advancement-301/giving-opportunities/index.aspx\" target=\"\" title=\"Giving to Simon\">Give to Simon</a></li>
                            <li><a href=\"http://simon.rochester.edu/sitemap/index.aspx\" target=\"\" title=\"Sitemap\">Sitemap</a></li>
                            <li><a href=\"http://simon.rochester.edu/news-and-media/news/privacy-policy/index.aspx\" target=\"\" title=\"Privacy Policy\">Privacy Policy</a></li>
                            <li><a href=\"http://simon.rochester.edu/legal/index.aspx\" target=\"\" title=\"Legal\">Legal</a></li>
                        </ul>
                    </td>
                    <td>
                        <h4 style=\" margin-bottom: 0; padding-bottom: 0;\"><a href=\"http://www.rochester.edu\" style=\"font-size:20px;\" target=\"_blank\">University of Rochester</a></h4>
                        <ul class=\"footer_values\">
                            <li><a href=\"http://www.rochester.edu\" target=\"_blank\">University of Rochester</a></li>
                            <li><a href=\"http://www.rochester.edu/maps/\" target=\"_blank\">Maps</a></li>
                            <li><a href=\"http://www.rochester.edu/advancement/\" target=\"_blank\">Giving</a></li>
                            <li><a href=\"https://info.rochester.edu/FacultyStaffDirectory/\" target=\"_blank\">Online Directory</a></li>
                            <br>
                        </ul>
                        <br><br>
                    </td>
                </tr>
            </table>
        </div>
        <div class=\"main_inset_right\">
            <div class=\"footer-left\">
                <br>
                <br>
                <p style=\"text-align: center;\">University of Rochester <br />
                Simon Business School <br />
                254 Gleason Hall <br />
                Rochester, NY 14627 <br />
                United States</p>
                <br>
            </div>
        </div>
    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "@simon_sass/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@simon_sass/base.html.twig", "/opt/local/www/drupal-photoroster/themes/simon_sass/templates/base.html.twig");
    }
}
