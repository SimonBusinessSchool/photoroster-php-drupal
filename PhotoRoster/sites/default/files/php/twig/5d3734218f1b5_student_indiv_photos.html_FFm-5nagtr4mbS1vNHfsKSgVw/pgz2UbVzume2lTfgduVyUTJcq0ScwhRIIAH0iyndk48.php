<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/student_indiv_photos.html.twig */
class __TwigTemplate_e6cab64378385011336e2f060a217372e6e657729c31037e6611535180a05e7d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 3, "if" => 6, "for" => 40];
        $filters = ["escape" => 1, "render" => 46];
        $functions = ["attach_library" => 1, "url" => 14, "file_url" => 46];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if', 'for'],
                ['escape', 'render'],
                ['attach_library', 'url', 'file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/large-image-display"), "html", null, true);
        echo "
<section ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
\t";
        // line 3
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/student_indiv_photos.html.twig", 3)->display($context);
        // line 4
        echo "\t<div style=\"width: 80%; margin: 0 auto;\">
\t<div class=\"title\">Individual Search Results</div>
\t";
        // line 6
        if (($this->getAttribute(($context["breadcrumb"] ?? null), 3, [], "array") != null)) {
            // line 7
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;
\t\t\t<a href=\"";
            // line 8
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 2, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 3, [], "array")), "html", null, true);
            echo "</div>
\t";
        } elseif (($this->getAttribute(        // line 9
($context["breadcrumb"] ?? null), 3, [], "array") == null)) {
            // line 10
            echo "\t\t<div class=\"breadcrumb\"><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 1, [], "array")), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 1, [], "array")), "html", null, true);
            echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp;";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 2, [], "array")), "html", null, true);
            echo "</div>
\t";
        }
        // line 12
        echo "\t<br>
\t&nbsp;
\t<a href=\"";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.homepage"));
        echo "\"><button class=\"btn btn-primary\">Back to Home</button></a>
\t";
        // line 15
        if ((($context["value"] ?? null) == "student")) {
            // line 16
            echo "\t\t<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.student_indiv_connection"));
            echo "\"><button class=\"btn btn-primary\">Back to search</button></a>
\t";
        } elseif ((        // line 17
($context["value"] ?? null) == "staff")) {
            // line 18
            echo "\t\t<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.staff_indiv_search_form"));
            echo "\"><button class=\"btn btn-primary\">Back to search</button></a>
\t";
        } elseif ((        // line 19
($context["value"] ?? null) == "faculty")) {
            // line 20
            echo "\t\t<a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.faculty_indiv_search_form"));
            echo "\"><button class=\"btn btn-primary\">Back to search</button></a>
\t";
        }
        // line 22
        echo "\t<br>
\t<br>
\t<div class=\"photo_bg\">
\t\t<br>
\t\t<!-- The Modal -->
\t\t<div id=\"myModal\" class=\"modal\">
\t\t\t<!-- The Close Button -->
\t\t\t<span class=\"close\">&times;</span>

\t\t\t<!-- Modal Content (The Image) -->
\t\t\t<img class=\"modal-content-local\" id=\"img01\">

\t\t\t<div class=\"modal-content-local\">
\t\t\t\t<button class=\"btn btn-primary\" id=\"download-image-button\">Download</button>&nbsp;<button class=\"btn btn-primary\" id=\"closeButton\">Close</button>
\t\t\t</div>
\t\t</div>
\t\t<table style=\"width: 100%;\">
\t\t\t<tr>
\t\t\t\t";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["first_name"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["fname"]) {
            // line 41
            echo "\t\t\t\t\t<td>
\t\t\t\t\t\t";
            // line 43
            echo "\t\t\t\t\t\t";
            if (( !$this->getAttribute(($context["photo_image"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == null)) {
                // line 44
                echo "\t\t\t\t\t\t\t<center><img alt=\"Broken Photo Link\" onerror=this.src=\"/sites/default/files/rochester.png\" onClick=\"selectPhoto(";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["uid"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo ", ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index0", [])), "html", null, true);
                echo ")\" class=\"myImg\" id=\"myImg";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index0", [])), "html", null, true);
                echo "\" data=\"data:image/jpg;base64,";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["large_photo_image"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "\" src=\"data:image/jpg;base64,";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["photo_image"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "\" height=\"200\" ></center>
\t\t\t\t\t\t";
            } else {
                // line 46
                echo "\t\t\t\t\t\t\t<center><a href=\"\" data-student=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["uid"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "\" data-popup=\"#popup\"><img border=\"3\" alt=\"\" src=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>")) . call_user_func_array($this->env->getFunction('file_url')->getCallable(), ["public://rochester.png"])), "html", null, true);
                echo "\" height=\"200\"></a></center>
\t\t\t\t\t\t";
            }
            // line 48
            echo "
\t\t\t\t\t\t";
            // line 50
            echo "\t\t\t\t\t\t";
            if (( !$this->getAttribute(($context["preferred_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == null)) {
                // line 51
                echo "\t\t\t\t\t\t\t<center><div  class=\"mobile_name_center\"><p>";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["preferred_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "&nbsp;";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "</p></div></center>
\t\t\t\t\t\t";
            } else {
                // line 53
                echo "\t\t\t\t\t\t\t<center><div  class=\"mobile_name_center\"><p>";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["first_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "&nbsp;";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last_name"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "</p></div></center>
\t\t\t\t\t\t";
            }
            // line 55
            echo "\t\t\t\t\t\t";
            if (( !$this->getAttribute(($context["dept"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array") == null)) {
                // line 56
                echo "\t\t\t\t\t\t\t<center><div  class=\"mobile_name_center\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["dept"] ?? null), $this->getAttribute($context["loop"], "index0", []), [], "array")), "html", null, true);
                echo "</div><center>
\t\t\t\t\t\t";
            }
            // line 58
            echo "\t\t\t\t\t\t<br>
\t\t\t\t\t</td>
\t\t\t\t\t";
            // line 60
            if (((($this->getAttribute($context["loop"], "index", []) % 5) == 0) &&  !$this->getAttribute($context["loop"], "last", []))) {
                // line 61
                echo "\t\t\t\t\t\t</tr><tr>
\t\t\t\t\t";
            }
            // line 63
            echo "\t\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fname'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "\t\t\t</tr>
\t\t</table>
\t\t<br>
\t\t<br>
\t\t<br>
\t</div>
\t<h1> &nbsp; </h1>
\t";
        // line 71
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/student_indiv_photos.html.twig", 71)->display($context);
        // line 72
        echo "</div>
\t<script>
\t\t//================Download Large Image as JPEG==============================================
\t\tdocument.getElementById('download-image-button').onclick = function(){downloadLargeImage();};
\t</script>
</section>
";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/student_indiv_photos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  267 => 72,  265 => 71,  256 => 64,  242 => 63,  238 => 61,  236 => 60,  232 => 58,  226 => 56,  223 => 55,  215 => 53,  207 => 51,  204 => 50,  201 => 48,  193 => 46,  179 => 44,  176 => 43,  173 => 41,  156 => 40,  136 => 22,  130 => 20,  128 => 19,  123 => 18,  121 => 17,  116 => 16,  114 => 15,  110 => 14,  106 => 12,  92 => 10,  90 => 9,  82 => 8,  71 => 7,  69 => 6,  65 => 4,  63 => 3,  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/student_indiv_photos.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/student_indiv_photos.html.twig");
    }
}
