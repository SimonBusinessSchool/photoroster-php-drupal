<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @simon_sass/header.html.twig */
class __TwigTemplate_f243688ab3dac4a8df1f7fc542fac194713877dbefb1376e27bc564ac2701d9e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 12];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1, "path" => 12];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                ['attach_library', 'path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/home-display"), "html", null, true);
        echo "
<section ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo ">
\t<div>
\t\t<br>
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t<a href=\"https://simon.rochester.edu/index.aspx\"><img src=\"/sites/default/files/simon_logo.png\"></a>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t";
        // line 12
        if (($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("<current>") == "/photoroster/logout")) {
            // line 13
            echo "\t\t\t
\t\t\t";
        } elseif (($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("<current>") == "/photoroster/login")) {
            // line 15
            echo "
\t\t\t";
        } elseif (($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("<current>") == "/photoroster/logon")) {
            // line 17
            echo "
\t\t\t";
        } else {
            // line 19
            echo "\t\t\t\t<button class=\"btn btn-primary\" onmouseup=\"logout()\" style=\"margin-left: 20px;\">Logout</button>
\t\t\t";
        }
        // line 21
        echo "\t\t\t <hr style=\"border: 2px DarkGrey; border-style: inset; width: 100%\">
\t\t</div>
\t</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "@simon_sass/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 21,  86 => 19,  82 => 17,  78 => 15,  74 => 13,  72 => 12,  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@simon_sass/header.html.twig", "/opt/local/www/drupal-photoroster/themes/simon_sass/templates/header.html.twig");
    }
}
