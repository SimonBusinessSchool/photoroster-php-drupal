<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/login-template.html.twig */
class __TwigTemplate_4ad3ccc1e64ddd90b4ce693e5f66234139dc897c1592823740b1371819b1e673 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 3];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/home-display"), "html", null, true);
        echo "
<section ";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
\t";
        // line 3
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/login-template.html.twig", 3)->display($context);
        // line 4
        echo "\t<div>
\t\t<div class=\"title\">Welcome to Photo Roster</div>
\t\t<div style=\"width: auto;\">
\t\t\t<h4>";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["error"] ?? null)), "html", null, true);
        echo "</h4>
\t\t\t<div class=\"login_div\">
\t\t\t\t<table style=\"width: 100%\">
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"home1\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div style=\"width: 85%; text-align: left; margin-left: 20px;\">";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["form"] ?? null)), "html", null, true);
        echo "</div>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t<div class=\"style4\" style=\"text-align: center;\">Simon Photo Roster Login</div>
\t\t\t\t\t\t\t<img class=\"login_img\" src=\"/sites/default/files/icons/full/OpenBook.png\" height=\"175px\"></img>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t</table>

\t\t\t</div>
\t\t\t<br>
\t\t\t<br>
\t\t</div>
\t\t<br>
\t\t<div style=\"bottom: 0;\">
\t";
        // line 30
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/login-template.html.twig", 30)->display($context);
        // line 31
        echo "\t</div>
\t</div>
</section>
\t
";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/login-template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 31,  99 => 30,  79 => 13,  70 => 7,  65 => 4,  63 => 3,  59 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/login-template.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/login-template.html.twig");
    }
}
