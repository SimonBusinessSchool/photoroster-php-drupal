<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/home.html.twig */
class __TwigTemplate_222242d4af2f393a1a7975340baeb90f8061e1259b402a6cae1c33fa18ca0bce extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 23];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1, "path" => 35];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                ['attach_library', 'path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/home-display"), "html", null, true);
        echo "
";
        // line 3
        echo "<script>
\twindow.addEventListener( \"pageshow\", function ( event ) {
\t\tvar historyTraversal = event.persisted || ( typeof window.performance != \"undefined\" && window.performance.navigation.type === 2 );
\t\tif ( historyTraversal ) {
\t\t\t// Handle page restore.
\t\t\twindow.location.reload();
\t\t}
\t});
\tWindow.onbeforeunload = function() {
\t    (document.getElementById(\"loader\")).style.display = \"none\";
\t}

\t\$(window).load(function() {
\t\t\t// Animate loader off screen
\t\t\t\$(\".loader\").fadeOut(\"slow\");;
\t\t});

\twindow.addEventListener(\"beforeunload\", function(event) { console.log(\"Hello\"); });
</script>
<section ";
        // line 22
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
\t";
        // line 23
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/home.html.twig", 23)->display($context);
        // line 24
        echo "\t<div style=\"width: 80%; margin: 0 auto;\">
        <div class=\"title\">Welcome to Photo Roster Home Page</div>
        <div class=\"breadcrumb\">Home</div>
        <br><br>
        <div style=\"width: 90%;\">
            <table class=\"fixed\" style=\"margin-left: 4%;\">
                <tr>
                    <td class=\"home3\" style=\"border: 2px solid #DCDCDC;\">
                        <br><br>
                        <div style=\"width: 50%; float: left;\">
                        <br>
                            <h4 style=\"text-align: left; margin: 0 0 0 25px; padding: 0\"><a href=\"";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.student_search"));
        echo "\"><u>Students</u></a></h4>
                            <ul style=\"text-align: left; padding-left: 35%\">
                                <li><a href=\"";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.program_search"));
        echo "\">Program</a></li>
                                <li><a href=\"";
        // line 38
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.course_search"));
        echo "\">Course</a></li>
                                <li><a href=\"";
        // line 39
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.student_indiv_connection"));
        echo "\">Individual Student</a></li>
                            </ul>
                        </div>
                        <div style=\"width: 40%; float: right;\">
                            <br>
                                <img src=\"/sites/default/files/icons/full/Cap.png\" height=\"100rem\"></img>
                            <br>
                            <br>
                        </div>
                        <br>
                        <br>
                    </td>
                    <td class=\"home3\" style=\"border: 2px solid #DCDCDC;\">
                        <br><br>
                        <div style=\"width: 50%; float: left;\">
                            <br>
                            <h4 style=\"text-align: left; margin: 0 0 0 25px; padding: 0\"><a href=\"";
        // line 55
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.staff_search"));
        echo "\" ><u>Staff</u></a></h4>
                            <ul style=\"text-align: left; margin-left: 35%;\">
                                <li><a href=\"";
        // line 57
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.staff_all_search"));
        echo "\">All Staff</a></li>
                                <li><a href=\"";
        // line 58
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.staff_indiv_search_form"));
        echo "\">Individual Staff</a></li>
                            </ul>
                        </div>
                        <div style=\"width: 40%; float: right;\">
                            <br>
                                <img src=\"/sites/default/files/icons/full/Staff.png\" height=\"100rem\"></img>
                            <br>
                            <br>
                        </div>
                        <br>
                        <br>

                    </td>
                    <td class=\"home3\" style=\"border: 2px solid #DCDCDC;\">
                        <br><br>
                        <div style=\"width: 50%; float: left;\">
                            <br>
                            <h4 style=\"text-align: left; margin: 0 0 0 25px; padding: 0\"><a href=\"";
        // line 75
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.faculty_search"));
        echo "\"><u>Faculty</u></a></h4>
                            <ul style=\"text-align: left; margin-left: 35%;\">
                                <li><a href=\"";
        // line 77
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.faculty_all_search"));
        echo "\">All Faculty</a></li>
                                <li><a href=\"";
        // line 78
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.faculty_indiv_search_form"));
        echo "\">Individual Faculty</a></li>
                            </ul>
                        </div>
                        <div style=\"width: 40%; float: right;\">
                            <br>
                                <img src=\"/sites/default/files/icons/full/Apple.png\" height=\"100rem\"></img>
                            <br>
                            <br>
                        </div>
                        <br>
                        <br>
                    </td>
                </tr>
                <tr>
                    <div id=\"table__full\">
                        <td  class=\"home3\" style=\"border: 2px solid #DCDCDC;\" colspan=\"3\">
                        <br>
                            <center>
                                <a href=\"";
        // line 96
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.simon_all_search"));
        echo "\"  onmouseup=\"loading()\"><img src=\"/sites/default/files/icons/full/Building.png\" id=\"noloader\" height=\"100rem\" onmouseup=\"loading()\" visible></a></img>
                                <div class=\"loader\" id=\"loader\" style=\"width: 10%; border: 5px solid #DCDCDC; background-color: #808080;\">
                                <center>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__ball\" style=\"margin-left: 25px\"></div>
                                    <div>Loading ... </div>
                                    </center>
                                </div>
                                <h4 onmouseup=\"loading()\"><a href=\"";
        // line 108
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.simon_all_search"));
        echo "\" >Search All of Simon</h4>
                            </center>";
        // line 110
        echo "                        </td>
                    </div>
                    <div id=\"table__mobile\">
                        <td  class=\"home3\" style=\"width 100rem; border: 2px solid #DCDCDC; display: none\" colspan=\"1\">
                        <br>
                            <center>
                                <a href=\"";
        // line 116
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.simon_all_search"));
        echo "\"  onmouseup=\"loading()\"><img src=\"/sites/default/files/icons/full/Building.png\" id=\"noloader\" height=\"100rem\" onmouseup=\"loading()\" visible></a></img>
                                <div class=\"loaderr\" id=\"loaderr\" style=\"width: 100rem; border: 5px solid #DCDCDC; background-color: #808080;\">
                                <center>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__bar\" style=\"margin-left: 25px\"></div>
                                    <div class=\"loader__ball\" style=\"margin-left: 25px\"></div>
                                    <div>Loading ... </div>
                                    </center>
                                </div>
                                <center><h4 onmouseup=\"loading()\"><a href=\"";
        // line 128
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("photo_roster.simon_all_search"));
        echo "\" >Search All of Simon</h4></center>
                            </center>";
        // line 130
        echo "                        </td>
                    </div>
                </tr>
            </table>
        </div>
    </div>
    <section>
\t";
        // line 137
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/home.html.twig", 137)->display($context);
        // line 138
        echo "    </section>
</section>";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 138,  245 => 137,  236 => 130,  232 => 128,  217 => 116,  209 => 110,  205 => 108,  190 => 96,  169 => 78,  165 => 77,  160 => 75,  140 => 58,  136 => 57,  131 => 55,  112 => 39,  108 => 38,  104 => 37,  99 => 35,  86 => 24,  84 => 23,  80 => 22,  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/home.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/home.html.twig");
    }
}
