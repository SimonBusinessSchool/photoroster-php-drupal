<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/photo_roster/templates/student-search.html.twig */
class __TwigTemplate_c15e716e6f0b061f484192d68aec4d70a88ad023c71974ae3f77a4972e3cb745 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 4];
        $filters = ["escape" => 1];
        $functions = ["attach_library" => 1, "url" => 9];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                ['attach_library', 'url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("photo_roster/home-display"), "html", null, true);
        echo "
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
<section ";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
        echo " class=\"main\">
\t";
        // line 4
        $this->loadTemplate("@simon_sass/header.html.twig", "modules/photo_roster/templates/student-search.html.twig", 4)->display($context);
        // line 5
        echo "\t<div style=\"width: 80%; margin: 0 auto;\">
\t<div class=\"title\">";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pageTitle"] ?? null)), "html", null, true);
        echo "</div>
\t<br>
\t<div class=\"breadcrumb\"><a href=\"";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["urlVal"] ?? null), 0, [], "array")), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["breadcrumb"] ?? null), 0, [], "array")), "html", null, true);
        echo "</a>&nbsp;&nbsp;>>&nbsp;&nbsp; Student Search</div>
\t<a href=\"";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.homepage"));
        echo "\"><button class=\"btn btn-primary\">Back to Home</button></a>
\t<div style=\"width: 100%\">
\t\t<div class=\"second_div__style\" style=\"width: 80%\">
\t\t\t<center>
\t\t\t\t<br>
\t\t\t\t<table class=\"fixed\">
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"home3\" style=\"width: 25rem; border: 2px solid #DCDCDC;\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<a href=\"";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.program_search"));
        echo "\">
\t\t\t\t\t\t\t<img src=\"/sites/default/files/icons/full/Program.png\" height=\"100rem\" max-width=\"100%\"></img>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div class=\"title\">Program Search</div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td class=\"home3\" style=\"width: 25rem; border: 2px solid #DCDCDC;\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<a href =\"";
        // line 27
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.course_search"));
        echo "\">
\t\t\t\t\t\t\t<img src=\"/sites/default/files/icons/full/Course.png\" height=\"100rem\"></img>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div class=\"title\">Course Search</div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t</td>
\t\t\t\t\t\t<td class=\"home3\" style=\"width: 25rem; border: 2px solid #DCDCDC;\">
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<a href =\"";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("photo_roster.student_indiv_connection"));
        echo "\">
\t\t\t\t\t\t\t<img src=\"/sites/default/files/icons/full/Individual.png\" height=\"100rem\"></img>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t<div class=\"title\">Individual Search</div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<br><br>
\t\t\t\t\t\t</td>
\t\t\t\t\t</tr>
\t\t\t\t</table>
\t\t\t</center>
\t\t\t<br>
\t\t\t<br>
\t\t</div>
\t</div>
</div>
\t";
        // line 51
        $this->loadTemplate("@simon_sass/base.html.twig", "modules/photo_roster/templates/student-search.html.twig", 51)->display($context);
        // line 52
        echo "</section>
";
    }

    public function getTemplateName()
    {
        return "modules/photo_roster/templates/student-search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 52,  134 => 51,  116 => 36,  104 => 27,  92 => 18,  80 => 9,  74 => 8,  69 => 6,  66 => 5,  64 => 4,  60 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "modules/photo_roster/templates/student-search.html.twig", "/opt/local/www/drupal-photoroster/modules/photo_roster/templates/student-search.html.twig");
    }
}
