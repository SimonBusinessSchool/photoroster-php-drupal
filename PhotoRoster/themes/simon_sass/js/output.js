"use strict";

var accordionList = document.querySelectorAll("[id^=\"accordion-\"]");

accordionList.forEach(function (accordion) {
	var itemList = Array.from(accordion.children[0].children[0].children);
	var headerList = [];
	var collapseList = [];
	var index = 0;
	itemList.forEach(function (item) {
		if (index % 2 == 0) {
			headerList.push(item);
			index++;
		} else {
			collapseList.push(item);
			index = 0;
		}
	});
	collapseList.forEach(function (item) {
		if (item.innerText == "") {
			var num = item.id.substr(19);
			headerList.forEach(function (header) {
				if (header.id.substr(18) == num) {
					headerList.splice(headerList.indexOf(header), 1);
					header.children[0].children[0].className = 'hide-collapse';
				}
			});
		}
	});
});
'use strict';

// helper function to recursively loop through given elements
// children, finding first instance of class specified

// @param {Object} el, {String} classToFind
// returns element with class or undefined if no match

// e.g. elementToUse = document.getElementById('myid');

// findClassInChildren(elementToUse, 'class-to-find')

function findClassInChildren(el, classToFind) {

	if (el.className == classToFind) {
		return el;
	}
	for (var i = 0; i < el.children.length; i++) {
		var result = findClassInChildren(el.children[i], classToFind);
		if (result != undefined) {
			return findClassInChildren(el.children[i], classToFind);
		}
	}
}

/* -------------------------------------------------------- */

var events = document.getElementsByClassName('node--view-mode-event-directory-view');

Array.from(events).forEach(function (el) {
	var photoClass = 'field field--name-field-image-2 field--type-image field--label-hidden field--item';
	var photoSpot = findClassInChildren(el, photoClass);
	if (photoSpot === undefined) {
		var div = document.createElement('div');
		div.className = photoClass;
		var img = document.createElement('div');
		img.className = 'placeholder img-responsive';
		var placeholderText = findClassInChildren(el, 'field field--name-field-event-d field--type-daterange field--label-hidden field--item').innerHTML;

		var month = /^([^\s]+)/g.exec(placeholderText)[0];
		var day = /(\d{2})/g.exec(placeholderText)[0];
		var photo = findClassInChildren(el, 'wrap-left-hug');
		photo.appendChild(div);
		div.appendChild(img);
		img.innerHTML = month + ' ' + day;
		console.log('this should be working');
		console.log(img.innerHTML);
	}
});
"use strict";
let ranking = document.getElementsByClassName('field--name-field-citation-text');

let rankingList = [];

Array.from(ranking).forEach(function (el) {
  if (el.innerHTML.length > 3) {
    el.classList.add('ranking--top');
  }
  if (el.innerHTML.indexOf('top') !== -1) {
  	console.log('hello');

  }
  el.innerHTML = el.innerHTML.replace("\"", "&ldquo;");
  el.innerHTML = el.innerHTML.replace("\"", "&rdquo;");
  el.innerHTML = el.innerHTML.replace(/#/g, "<span class=\"pound-sign\">#</span>");
  el.innerHTML = el.innerHTML.replace(/(Top)/g, "<span class=\"ranking--small\">$1</span>");



});

let subheadline = document.getElementsByClassName('field--name-field-subheadline-text');


Array.from(subheadline).forEach((sub) => {
  let num = sub.innerHTML.replace(/(\d+)/g, "<span class=\"no-italics\">$1</span>");
  sub.innerHTML = num;
});

//Capitalize MBA when it is in breadcrumbs
let breadcrumbsub = document.getElementsByClassName('breadcrumb');


Array.from(breadcrumbsub).forEach((bread) => {
    let newbread = bread.innerHTML.replace(/(Mba)/gi, "MBA");

    bread.innerHTML = newbread;
});

//ADD Hyphen to Full Time MBA
let breadcrumbhyphen = document.getElementsByClassName('breadcrumb');


Array.from(breadcrumbhyphen).forEach((breadhyphen) => {
    let newhyphen = breadhyphen.innerHTML.replace(/(Full Time)/gi, "Full-Time");
    breadhyphen.innerHTML = newhyphen;
});

/*
let rankingTop = document.getElementsByClassName('field--name-field-citation-text');


Array.from(rankingTop).forEach((rank) => {
    let numTop = rank.innerHTML.replace(/(\w+)\s/g, "<span class=\"ranking--small\">$1</span>");
    rank.innerHTML = numTop;
});

*/
/*
var ranking = document.getElementsByClassName('field--name-field-citation-text');

var rankingList = [];

Array.from(ranking).forEach(function (el) {
	for (var i = 0; i < el.innerHTML.length; i++) {
		var alpha = [];
		var num = [];
		console.log(el.innerHTML[i]);
	}
	el.innerHTML = el.innerHTML.replace("\"", "&ldquo;");
	el.innerHTML = el.innerHTML.replace("\"", "&rdquo;");
	el.innerHTML = el.innerHTML.replace(/#/g, "<span class=\"pound-sign\">#</span>");
});

var subheadline = document.getElementsByClassName('field--name-field-subheadline-text');

Array.from(subheadline).forEach(function (sub) {
	var num = sub.innerHTML.replace(/(\d+)/g, "<span class=\"no-italics\">$1</span>");
	sub.innerHTML = num;
});
*/
