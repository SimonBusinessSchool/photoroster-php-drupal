let ranking = document.getElementsByClassName('field--name-field-citation-text');

let rankingList = [];

Array.from(ranking).forEach(function (el) {
  if (el.innerHTML.length > 5) {
    el.classList.add('ranking--small');
  }
  el.innerHTML = el.innerHTML.replace("\"", "&ldquo;");
	el.innerHTML = el.innerHTML.replace("\"", "&rdquo;");
	el.innerHTML = el.innerHTML.replace(/#/g, "<span class=\"pound-sign\">#</span>");
});

let subheadline = document.getElementsByClassName('field--name-field-subheadline-text');


Array.from(subheadline).forEach((sub) => {
    let num = sub.innerHTML.replace(/(\d+)/g, "<span class=\"no-italics\">$1</span>");
    sub.innerHTML = num;
});

/*for (let i = 0; i < el.innerHTML.length; i++) {
  let alpha = [];
  let num = [];
  //console.log(el.innerHTML[i])
}
*/
