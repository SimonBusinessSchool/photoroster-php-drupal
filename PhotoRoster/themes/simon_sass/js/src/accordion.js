let accordionList = document.querySelectorAll(`[id^="accordion-"]`);

accordionList.forEach((accordion) => {
	let itemList = Array.from(accordion.children[0].children[0].children);
	let headerList = [];
	let collapseList = [];
	let index = 0;
	itemList.forEach((item) => {
		if ((index % 2) == 0) {
			headerList.push(item);
			index++;
		}
		else {
			collapseList.push(item);
			index = 0;
		}
	});
	collapseList.forEach((item) => {
		if (item.innerText == "") {
			let num = item.id.substr(19);
			headerList.forEach((header) => {
				if (header.id.substr(18) == num) {
					headerList.splice(headerList.indexOf(header), 1);
					header.children[0].children[0].className = 'hide-collapse';
				}
			});
		}
	});
});
