// helper function to recursively loop through given elements
// children, finding first instance of class specified

// @param {Object} el, {String} classToFind
// returns element with class or undefined if no match

// e.g. elementToUse = document.getElementById('myid');

// findClassInChildren(elementToUse, 'class-to-find')

function findClassInChildren(el, classToFind) {

	if (el.className == classToFind) {
		return el;
	}
	for (let i = 0; i < el.children.length; i++) {
		let result = findClassInChildren(el.children[i], classToFind);
		if (result != undefined) {
			return findClassInChildren(el.children[i], classToFind);
		}
	}
}

/* -------------------------------------------------------- */

let events = document.getElementsByClassName('node--view-mode-event-directory-view');


Array.from(events).forEach((el) => {
	let photoClass = `field field--name-field-image-2 field--type-image field--label-hidden field--item`
	let photoSpot = findClassInChildren(el, photoClass);
	if (photoSpot === undefined) {
		let div = document.createElement('div');
		div.className = photoClass;
		let img = document.createElement('div');
		img.className = 'placeholder img-responsive';
		let placeholderText = findClassInChildren(el, 'field field--name-field-event-d field--type-daterange field--label-hidden field--item').innerHTML;

		let month = /^([^\s]+)/g.exec(placeholderText)[0];
		let day = /(\d{2})/g.exec(placeholderText)[0];
		let photo = findClassInChildren(el, 'wrap-left-hug');
		photo.appendChild(div);
		div.appendChild(img);
		img.innerHTML = `${month} ${day}`;
		console.log('this should be working');
		console.log(img.innerHTML);
	}
});