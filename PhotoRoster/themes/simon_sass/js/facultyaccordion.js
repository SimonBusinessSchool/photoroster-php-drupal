var $ = jQuery;
//add panel class to faculty accordion on page load

$( "a.panel-title").addClass( "collapsed");

//Faculty Directory change default value of selects
$(document).ready(function() {
    $('select[name=field_last_name_value] option[value="All"]').text('A-Z');
    $('select[name=field_expertise_value] option[value="All"]').text('Expertise');
    $('select[name=field_program_faculty_value] option[value="All"]').text('Program');
});