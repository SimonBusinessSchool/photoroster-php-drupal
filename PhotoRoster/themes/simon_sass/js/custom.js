jQuery(document).ready(function ($) {

    $('.accordion').find('.accordion-toggle').click(function () {
        $(this).next().slideToggle('600');
        $(".accordion-content").not($(this).next()).slideUp('600');
    });
    $('.accordion-toggle').on('click', function () {
        $(this).toggleClass('active').siblings().removeClass('active');
    });

//    3 Reasons why carousel starts
    if($('div.resonswhy-container').length >0){
        var count = $('div.resonswhy-container').length;
        var text=count+' '+$('.secondary-title').html();
        $('.secondary-title').html(text);
    }
//    3 Reasons why carousel Ends

//Lightslider for Story Carousel starts
    var lihtml = '';
    jQuery('.lightslider-content').each(function (i, obj) {

        lihtml = lihtml + $(this).html();

        //test
    });
    lihtml=lihtml+'<li data-thumb="" style="height:100%;background:#356BAC;" > <div class="slider-body"><div class="slider-formoreoption"><a class="formoreoption" href="stories-directory" >More Stories</a></div></div></li>';

    $('#lightSlider').html(lihtml);

    window.prettyPrint && prettyPrint();
    slider = jQuery('#lightSlider').lightSlider({
        item: 4,
        loop: false,
        auto: false,
        slideMove: 1,
        adaptiveHeight: true,

        slideMargin:0,
        //                    autoWidth:true,
        currentPagerPosition: 'middle',
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 500,
        responsive : [
        {
                breakpoint:1920,
                settings: {
                    item:4,
                    slideMove:1,
                    slideMargin:0,
                  }
            },
            {
                breakpoint:1024,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:0,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ],
        onSliderLoad: function () {
            $('#lightSlider').removeClass('cS-hidden');
            var sliderprev = $('.lslide.active').children('.slider-video').html();
            var slidertxt = $('.lslide.active').children('.slider-txt').html();
            $('.imgtxt').fadeIn(1000).html(slidertxt);
            $('.imgpreview').fadeIn(1000).html(sliderprev);
        },
        goToNextSlide: function (el) {
        },
        onBeforeNextSlide: function ($el, scene) {
        },
        onBeforeSlide: function ($el, scene) {
            $children = $el.children();
            var sliderprev = $children.eq(scene).children('.slider-video').html();
            var slidertxt = $children.eq(scene).children('.slider-txt').html();
            $('.imgtxt').fadeIn(1000).html(slidertxt);
            $('.imgpreview').fadeIn(1000).html(sliderprev);
        }
    });

    //Lightslider for Story Carousel Ends

    //START #block-simon-sass-mainnavigation-12 Sub Menu Code
    $("#block-simon-sass-mainnavigation-12 ul.menu--main li.expanded.dropdown a.dropdown-toggle").removeAttr("data-toggle");
    $("#block-simon-sass-mainnavigation-12 ul.menu--main li.expanded.dropdown a.dropdown-toggle").removeAttr("href");
    $("#block-simon-sass-mainnavigation-12 ul li.expanded ul.dropdown-menu li.expanded ul.dropdown-menu").css("display", "none");

    $("#block-simon-sass-mainnavigation-12 ul.menu--main li.expanded.dropdown a.dropdown-toggle").on("click", function() {
         $(this).parent().toggleClass('open');
    });

    $("#block-simon-sass-mainnavigation-12 ul li.expanded ul.dropdown-menu li.expanded a").after("<span class='childMenuSpan'></span>");

    $("#block-simon-sass-mainnavigation-12 ul li.expanded ul.dropdown-menu li.expanded span.childMenuSpan").on('click', function() {
        if(!$(this).parent().hasClass("menuOpened")) {
            $(this).parent().addClass("menuOpened");
            $(this).next().css("display", "block");
            $(this).css({'transform': 'rotateX(180deg)', 'transition': '.2s'  });
            $(this).parent().css("flex-wrap", "wrap");
        } else {
            $(this).parent().removeClass("menuOpened");
            $(this).next().css("display", "none");
            $(this).css({'transform': 'unset', 'transition': '.2s' });
            $(this).parent().css("flex-wrap", "nowrap");
        }
    });
    //END #block-simon-sass-mainnavigation-12 Sub Menu Code

    //START #block-simon-sass-mainnavigation-12 Sub Menu Code
    $("#menu-c259").on("click", function() {
         // retrieve current state, initially undefined
        var state = $(this).data('state');

        // toggle the state - first click will make this "true"
        state = !state;

        // do your stuff
        if (state) {
            // do this (1st click, 3rd click, etc)
            $("#block-googlecustomsearchblock").addClass("searchSlideDown");
        } else {
            // do that
            $("#block-googlecustomsearchblock").removeClass("searchSlideDown");
        }

        // put the state back
        $(this).data('state', state);
    });
    //END #block-simon-sass-mainnavigation-12 Sub Menu Code

    $("#block-googlecustomsearchblock").on("click", "div.gsc-clear-button",function (){
        $("#block-googlecustomsearchblock").removeClass("searchSlideDown");
        $("#menu-c259").data('state', "");
    });
});